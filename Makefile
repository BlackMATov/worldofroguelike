all: update get_deps compile_all
dev: compile_self check_self tags
dev_start: dev start
STARTCMD = erl\
	-pa apps/wor/ebin\
	-pa deps/cowboy/ebin\
	-pa deps/cowlib/ebin\
	-pa deps/ecoro/ebin\
	-pa deps/eep/ebin\
	-pa deps/goldrush/ebin\
	-pa deps/jiffy/ebin\
	-pa deps/lager/ebin\
	-pa deps/ranch/ebin\
	-pa deps/erlmonads/ebin\
	-s wor

update:
	hg pull -u

clean:
	rebar clean

get_deps:
	rebar get-deps

compile_all:
	rebar compile

compile_self:
	rebar compile skip_deps=true

check_self:
	dialyzer -r -nn apps/wor/ebin

build_plt:
	dialyzer --build_plt --apps\
		erts\
		kernel\
		stdlib\
		deps/cowboy\
		deps/cowlib\
		deps/ecoro\
		deps/eep\
		deps/goldrush\
		deps/jiffy\
		deps/lager\
		deps/ranch\
		deps/erlmonads

tags:
	ctags -R\
		apps/wor/src\
		deps/cowboy/src\
		deps/cowlib/src\
		deps/ecoro/src\
		deps/eep/src\
		deps/goldrush/src\
		deps/jiffy/src\
		deps/lager/src\
		deps/ranch/src\
		deps/erlmonads/src

start:
	$(STARTCMD)

start_sasl:
	$(STARTCMD) -boot start_sasl

start_silent:
	$(STARTCMD) -detached
