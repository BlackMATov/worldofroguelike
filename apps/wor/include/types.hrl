%% ----------------------------------------------------------------------------
%%
%% DEFINES
%%
%% ----------------------------------------------------------------------------

-define(WOR_MAX_ZONE_WIDTH,  1000).
-define(WOR_MAX_ZONE_HEIGHT, 1000).

%% ----------------------------------------------------------------------------
%%
%% TYPES
%%
%% ----------------------------------------------------------------------------

-type wor_json()   :: list() | tuple() | number() | binary().
-type wor_vec2()   :: wor_vec2:vec2().
-type wor_entity() :: wor_es:entity().

-type wor_comp_type() ::
	zone        |
	dead        |
	world       |
	btree       |
	brain       |
	door        |
	food        |
	window      |
	usable      |
	spirit      |
	usabler     |
	pusher      |
	pushable    |
	stepable    |
	portal      |
	mobfarm     |
	farmable    |
	nameable    |
	equipment   |
	equip_slots |
	inv_item    |
	add_stat    |
	inventory   |
	health      |
	spawner     |
	respawn     |
	stats       |
	fighter     |
	relation    |
	experience  |
	userable    |
	placeable   |
	trash       |
	trasher     |
	requester.

-type wor_comp_state() ::
	wor_zone_comp()        |
	wor_dead_comp()        |
	wor_world_comp()       |
	wor_btree_comp()       |
	wor_brain_comp()       |
	wor_door_comp()        |
	wor_food_comp()        |
	wor_window_comp()      |
	wor_usable_comp()      |
	wor_spirit_comp()      |
	wor_usabler_comp()     |
	wor_pusher_comp()      |
	wor_pushable_comp()    |
	wor_stepable_comp()    |
	wor_portal_comp()      |
	wor_mobfarm_comp()     |
	wor_farmable_comp()    |
	wor_nameable_comp()    |
	wor_equipment_comp()   |
	wor_equip_slots_comp() |
	wor_inv_item_comp()    |
	wor_add_stat_comp()    |
	wor_inventory_comp()   |
	wor_health_comp()      |
	wor_spawner_comp()     |
	wor_respawn_comp()     |
	wor_stats_comp()       |
	wor_fighter_comp()     |
	wor_relation_comp()    |
	wor_experience_comp()  |
	wor_userable_comp()    |
	wor_placeable_comp()   |
	wor_trash_comp()       |
	wor_trasher_comp()     |
	wor_requester_comp().

-type wor_view_type() ::
	none        |
	ground      |
	wall        |
	tree        |
	rock        |
	water       |
	ver_window  |
	hor_window  |
	opened_door |
	closed_door |
	orc         |
	rat         |
	human       |
	hobbit      |
	zombie      |
	player      |
	spirit      |
	respawn     |
	portal      |
	sword       |
	helmet      |
	potion.

%% ----------------------------------------------------------------------------
%%
%% COMPONENTS
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% wor_zone_comp
%% ------------------------------------

-record(wor_zone_comp, {
	name    = <<"">> :: binary(),
	started = false  :: boolean(),
	size    = {0,0}  :: wor_vec2(),
	loaded  = false  :: boolean()
}).
-type wor_zone_comp() :: #wor_zone_comp{}.

%% ------------------------------------
%% wor_dead_comp
%% ------------------------------------

-record(wor_dead_comp, {
}).
-type wor_dead_comp() :: #wor_dead_comp{}.

%% ------------------------------------
%% wor_world_comp
%% ------------------------------------

-record(wor_world_comp, {
	ticks = 0 :: integer()
}).
-type wor_world_comp() :: #wor_world_comp{}.

%% ------------------------------------
%% wor_btree_comp
%% ------------------------------------

-record(wor_btree_comp, {
	coro       = undefined  :: ecoro:ecoro() | undefined,
	root       = undefined  :: wor_btree:bt_node() | undefined,
	blackboard = dict:new() :: wor_btree:bt_blackboard()
}).
-type wor_btree_comp() :: #wor_btree_comp{}.

%% ------------------------------------
%% wor_brain_comp
%% ------------------------------------

-record(wor_brain_comp, {
	ticks  = 0         :: integer(),
	action = undefined :: wor_action:state() | undefined
}).
-type wor_brain_comp() :: #wor_brain_comp{}.

%% ------------------------------------
%% wor_door_comp
%% ------------------------------------

-record(wor_door_comp, {
	opened = false :: boolean()
}).
-type wor_door_comp() :: #wor_door_comp{}.

%% ------------------------------------
%% wor_food_comp
%% ------------------------------------

-record(wor_food_comp, {
	hp = 0 :: integer()
}).
-type wor_food_comp() :: #wor_food_comp{}.

%% ------------------------------------
%% wor_window_comp
%% ------------------------------------

-record(wor_window_comp, {
}).
-type wor_window_comp() :: #wor_window_comp{}.

%% ------------------------------------
%% wor_usable_comp
%% ------------------------------------

-record(wor_usable_comp, {
	comp = undefined :: wor_comp_type() | undefined
}).
-type wor_usable_comp() :: #wor_usable_comp{}.

%% ------------------------------------
%% wor_spirit_comp
%% ------------------------------------

-record(wor_spirit_comp, {
}).
-type wor_spirit_comp() :: #wor_spirit_comp{}.

%% ------------------------------------
%% wor_usabler_comp
%% ------------------------------------

-record(wor_usabler_comp, {
}).
-type wor_usabler_comp() :: #wor_usabler_comp{}.

%% ------------------------------------
%% wor_pusher_comp
%% ------------------------------------

-record(wor_pusher_comp, {
}).
-type wor_pusher_comp() :: #wor_pusher_comp{}.

%% ------------------------------------
%% wor_pushable_comp
%% ------------------------------------

-record(wor_pushable_comp, {
	comp = undefined :: wor_comp_type() | undefined
}).
-type wor_pushable_comp() :: #wor_pushable_comp{}.

%% ------------------------------------
%% wor_stepable_comp
%% ------------------------------------

-record(wor_stepable_comp, {
	comp = undefined :: wor_comp_type() | undefined
}).
-type wor_stepable_comp() :: #wor_stepable_comp{}.

%% ------------------------------------
%% wor_portal_comp
%% ------------------------------------

-record(wor_portal_comp, {
	name     = <<"">> :: binary(),
	tozone   = <<"">> :: binary(),
	totarget = <<"">> :: binary()
}).
-type wor_portal_comp() :: #wor_portal_comp{}.

%% ------------------------------------
%% wor_mobfarm_comp
%% ------------------------------------

-record(wor_mobfarm_comp, {
	type   = <<"">> :: binary(),
	max    = 0      :: integer(),
	period = 0      :: integer(),
	ticks  = 0      :: integer()
}).
-type wor_mobfarm_comp() :: #wor_mobfarm_comp{}.

%% ------------------------------------
%% wor_farmable_comp
%% ------------------------------------

-record(wor_farmable_comp, {
	farm = undefined :: wor_entity() | undefined
}).
-type wor_farmable_comp() :: #wor_farmable_comp{}.

%% ------------------------------------
%% wor_nameable_comp
%% ------------------------------------

-record(wor_nameable_comp, {
	name = <<"">> :: binary()
}).
-type wor_nameable_comp() :: #wor_nameable_comp{}.

%% ------------------------------------
%% wor_equipment_comp
%% ------------------------------------

-record(wor_equipment_comp, {
	slot = undefined :: wor_equip_slots:slot_type() | undefined
}).
-type wor_equipment_comp() :: #wor_equipment_comp{}.

%% ------------------------------------
%% wor_equip_slots_comp
%% ------------------------------------

-record(wor_equip_slots_comp, {
	slots = dict:new() :: dict:dict(wor_equip_slots:slot_type(), wor_entity())
}).
-type wor_equip_slots_comp() :: #wor_equip_slots_comp{}.

%% ------------------------------------
%% wor_inv_item_comp
%% ------------------------------------

-record(wor_inv_item_comp, {
}).
-type wor_inv_item_comp() :: #wor_inv_item_comp{}.

%% ------------------------------------
%% wor_add_stat_comp
%% ------------------------------------

-record(wor_add_stat_comp, {
	stats = [] :: list({wor_stats:stat_type(),integer()})
}).
-type wor_add_stat_comp() :: #wor_add_stat_comp{}.

%% ------------------------------------
%% wor_inventory_comp
%% ------------------------------------

-record(wor_inventory_comp, {
	items = sets:new() :: sets:set()
}).
-type wor_inventory_comp() :: #wor_inventory_comp{}.

%% ------------------------------------
%% wor_health_comp
%% ------------------------------------

-record(wor_health_comp, {
	hp  = 0 :: integer(),
	max = 0 :: integer()
}).
-type wor_health_comp() :: #wor_health_comp{}.

%% ------------------------------------
%% wor_spawner_comp
%% ------------------------------------

-record(wor_spawner_comp, {
	attach = queue:new() :: queue:queue(),
	detach = queue:new() :: queue:queue()
}).
-type wor_spawner_comp() :: #wor_spawner_comp{}.

%% ------------------------------------
%% wor_respawn_comp
%% ------------------------------------

-record(wor_respawn_comp, {
}).
-type wor_respawn_comp() :: #wor_respawn_comp{}.

%% ------------------------------------
%% wor_stats_comp
%% ------------------------------------

-record(wor_stats_comp, {
	str = 1 :: integer(),
	dex = 1 :: integer(),
	int = 1 :: integer()
}).
-type wor_stats_comp() :: #wor_stats_comp{}.

%% ------------------------------------
%% wor_fighter_comp
%% ------------------------------------

-record(wor_fighter_comp, {
	def     = 0 :: integer(),
	min_dmg = 0 :: integer(),
	max_dmg = 0 :: integer()
}).
-type wor_fighter_comp() :: #wor_fighter_comp{}.

%% ------------------------------------
%% wor_relation_comp
%% ------------------------------------

-record(wor_relation_comp, {
	group     = undefined  :: wor_relation:group_type() | undefined,
	relations = dict:new() :: dict:dict(wor_relation:group_type(),wor_relation:relation_type())
}).
-type wor_relation_comp() :: #wor_relation_comp{}.

%% ------------------------------------
%% wor_experience_comp
%% ------------------------------------

-record(wor_experience_comp, {
	level      = 0 :: integer(),
	experience = 0 :: integer()
}).
-type wor_experience_comp() :: #wor_experience_comp{}.

%% ------------------------------------
%% wor_userable_comp
%% ------------------------------------

-record(wor_userable_comp, {
	user      = undefined :: pid() | undefined,
	newaction = undefined :: wor_action:state() | undefined
}).
-type wor_userable_comp() :: #wor_userable_comp{}.

%% ------------------------------------
%% wor_placeable_comp
%% ------------------------------------

-record(wor_placeable_comp, {
	pos         = {0,0}     :: wor_vec2(),
	zone        = undefined :: wor_entity() | undefined,
	view        = ground    :: wor_view_type(),
	visible     = true      :: boolean(),
	passable    = true      :: boolean(),
	transparent = true      :: boolean()
}).
-type wor_placeable_comp() :: #wor_placeable_comp{}.

%% ------------------------------------
%% wor_trash_comp
%% ------------------------------------

-record(wor_trash_comp, {
	ticks = 0 :: integer()
}).
-type wor_trash_comp() :: #wor_trash_comp{}.

%% ------------------------------------
%% wor_trasher_comp
%% ------------------------------------

-record(wor_trasher_comp, {
	trash = sets:new() :: sets:set()
}).
-type wor_trasher_comp() :: #wor_trasher_comp{}.

%% ------------------------------------
%% wor_requester_comp
%% ------------------------------------

-record(wor_requester_comp, {
	requests = queue:new() :: queue:queue()
}).
-type wor_requester_comp() :: #wor_requester_comp{}.
