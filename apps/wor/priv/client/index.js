// ----------------------------------------------------------------------------
// 
// Utils
// 
// ----------------------------------------------------------------------------

// http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for ( var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x ) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame  = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}
	if ( !window.requestAnimationFrame ) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function(){
				callback(currTime + timeToCall);
			}, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	if ( !window.cancelAnimationFrame ) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());

// https://gist.github.com/tbranyen/1049426
String.prototype.format = function(i, safe, arg) {
	function format() {
		var str = this, len = arguments.length + 1;
		for ( i = 0; i < len; arg = arguments[i++] ) {
			safe = typeof arg === 'object' ? JSON.stringify(arg) : arg;
			str = str.replace(RegExp('\\{'+(i-1)+'\\}', 'g'), safe);
		}
		return str;
	}
	format.native = String.prototype.format;
	return format;
}();

// ----------------------------------------------------------------------------
// 
// Tables
// 
// ----------------------------------------------------------------------------

var woc_layers = {
	floor       : 0,
	items       : 1,
	decorations : 2,
	spirits     : 3,
	monsters    : 4,
	players     : 5,
	me_ent      : 6
};

var woc_views = {
	me_ent      : { ch : "@",  fg : "#4FF", bg : "#000", layer : woc_layers.me_ent},
	me_spirit   : { ch : "@",  fg : "#CCC", bg : "#000", layer : woc_layers.me_ent},
	none        : { ch : " ",  fg : "#FFF", bg : "#000", layer : woc_layers.floor},
	ground      : { ch : ".",  fg : "#CCC", bg : "#000", layer : woc_layers.floor},
	wall        : { ch : "#",  fg : "#FFF", bg : "#000", layer : woc_layers.decorations},
	tree        : { ch : "T",  fg : "#0F0", bg : "#000", layer : woc_layers.decorations},
	rock        : { ch : "^",  fg : "#FFF", bg : "#000", layer : woc_layers.decorations},
	water       : { ch : "~",  fg : "#00F", bg : "#000", layer : woc_layers.decorations},
	ver_window  : { ch : "|",  fg : "#00F", bg : "#000", layer : woc_layers.decorations},
	hor_window  : { ch : "-",  fg : "#00F", bg : "#000", layer : woc_layers.decorations},
	opened_door : { ch : "'",  fg : "#FF0", bg : "#000", layer : woc_layers.decorations},
	closed_door : { ch : "+",  fg : "#FF0", bg : "#000", layer : woc_layers.decorations},
	orc         : { ch : "o",  fg : "#0A0", bg : "#000", layer : woc_layers.monsters},
	rat         : { ch : "r",  fg : "#FFF", bg : "#000", layer : woc_layers.monsters},
	human       : { ch : "h",  fg : "#FFF", bg : "#000", layer : woc_layers.monsters},
	hobbit      : { ch : "h",  fg : "#0A0", bg : "#000", layer : woc_layers.monsters},
	zombie      : { ch : "z",  fg : "#F00", bg : "#000", layer : woc_layers.monsters},
	player      : { ch : "@",  fg : "#FF0", bg : "#000", layer : woc_layers.players},
	spirit      : { ch : "@",  fg : "#CCC", bg : "#000", layer : woc_layers.spirits},
	respawn     : { ch : "*",  fg : "#FFF", bg : "#999", layer : woc_layers.floor},
	portal      : { ch : ".",  fg : "#FFF", bg : "#999", layer : woc_layers.floor},
	sword       : { ch : "/",  fg : "#4FF", bg : "#000", layer : woc_layers.items},
	helmet      : { ch : "\"", fg : "#4FF", bg : "#000", layer : woc_layers.items},
	potion      : { ch : "!",  fg : "#F00", bg : "#000", layer : woc_layers.items}
};

var woc_begin_menu = {
	type     : "",
	items    : [],
	cur_item : 0
};

var woc_gen_direction_desc =
	"Press %c{yellow}qweasdzxc%c{} or " +
	"%c{yellow}hjklyubn%c{} or " +
	"%c{yellow}Arrows%c{} or " +
	"%c{yellow}Numpads%c{} for %c{green}Move%c{}";

var woc_gen_direction_binds = function(callback) {
	return [
		{
			keys : [ROT.VK_Q, ROT.VK_NUMPAD7, ROT.VK_Y],
			func : function() {
				callback(-1,-1);
			}
		},
		{
			keys : [ROT.VK_W, ROT.VK_NUMPAD8, ROT.VK_K, ROT.VK_UP],
			func : function() {
				callback(0,-1);
			}
		},
		{
			keys : [ROT.VK_E, ROT.VK_NUMPAD9, ROT.VK_U],
			func : function() {
				callback(1,-1);
			}
		},
		{
			keys : [ROT.VK_A, ROT.VK_NUMPAD4, ROT.VK_H, ROT.VK_LEFT],
			func : function() {
				callback(-1,0);
			}
		},
		{
			keys : [ROT.VK_S, ROT.VK_X, ROT.VK_NUMPAD5, ROT.VK_NUMPAD2, ROT.VK_J, ROT.VK_DOWN],
			func : function() {
				callback(0,1);
			}
		},
		{
			keys : [ROT.VK_D, ROT.VK_NUMPAD6, ROT.VK_L, ROT.VK_RIGHT],
			func : function() {
				callback(1,0);
			}
		},
		{
			keys : [ROT.VK_Z, ROT.VK_NUMPAD1, ROT.VK_B],
			func : function() {
				callback(-1,1);
			}
		},
		{
			keys : [ROT.VK_C, ROT.VK_NUMPAD3, ROT.VK_N],
			func : function() {
				callback(1,1);
			}
		}
	]
};

var woc_begin_binds = {
	keys : _.union(
		[
			{
				keys : [ROT.VK_O],
				func : function() {
					woc_begin_use_action();
				}
			},
			{
				keys : [ROT.VK_G],
				func : function() {
					woc_begin_grab_request();
				}
			},
			{
				keys : [ROT.VK_I],
				func : function() {
					woc_begin_inventory_request();
				}
			},
			{
				keys : [ROT.VK_ENTER, ROT.VK_RETURN],
				func : function() {
					woc_begin_chat_action();
				}
			}
		], woc_gen_direction_binds(function(dx, dy) {
			woc_send_action_step(dx, dy)
		})),
	desc :
		woc_gen_direction_desc +
		"\n\nPress %c{yellow}o%c{} for %c{green}Use%c{} map object" +
		"\n\nPress %c{yellow}g%c{} for %c{green}Grab%c{} items" +
		"\n\nPress %c{yellow}i%c{} for open %c{green}Inventory%c{}" +
		"\n\nPress %c{yellow}Enter%c{} for %c{green}Chat%c{}"
};

// ----------------------------------------------------------------------------
// 
// Data holder
// 
// ----------------------------------------------------------------------------

var woc = function() {
	start_time = Date.now();
	return {
		client            : new Client(),
		display           : new ROT.Display({width:80, height:50}),
		ping              : 0,
		users             : 0,
		size              : {x:80,y:50},
		me_ent            : -1,
		me_dead           : false,
		menu              : woc_begin_menu,
		binds             : woc_begin_binds,
		stats             : null,
		logs              : [],
		entities          : {},
		redraw_ui         : false,
		redraw_map        : false,
		menu_bar_width    : 27,
		debug_bar_height  : 2,
		status_bar_height : 8,
		ping_timeout      : null,
		get_game_time     : function() {
			return Date.now() - start_time;
		}
	}
}();

// ----------------------------------------------------------------------------
// 
// Start
// 
// ----------------------------------------------------------------------------

$(function(){
	$("body").keydown(function(e){
		var succeed = woc_input_process(e.keyCode);
		return !succeed;
	});
	woc.client.start(woc_game_start, woc_game_finish);
	woc.client.add_handler("log_info",       woc_log_info_handler);
	woc.client.add_handler("self_info",      woc_self_info_handler);
	woc.client.add_handler("zone_info",      woc_zone_info_handler);
	woc.client.add_handler("stats_info",     woc_stats_info_handler);
	woc.client.add_handler("grab_info",      woc_grab_info_handler);
	woc.client.add_handler("inventory_info", woc_inventory_info_handler);
});

var woc_game_start = function() {
	console.log("online");
	document.body.appendChild(woc.display.getContainer());
	woc_start_ping_loop();
	woc_begin_attach_request();
	woc_game_step();
};

var woc_game_finish = function() {
	woc_stop_ping_loop();
	document.body.removeChild(woc.display.getContainer());
	console.log("offline");
};

var woc_game_step = function() {
	if ( woc.client.is_online() ) {
		if ( woc.redraw_ui ) {
			woc_draw_menu_bar();
			woc_draw_status_bar();
			woc_draw_debug_bar();
			woc.redraw_ui = false;
		}
		if ( woc.redraw_map ) {
			woc_draw_tiles();
			woc_draw_entities();
			woc.redraw_map = false;
		}
		requestAnimationFrame(woc_game_step);
	}
};

// ----------------------------------------------------------------------------
// 
// Handlers
// 
// ----------------------------------------------------------------------------

var woc_log_info_handler = function(compact_value) {
	var value = woc_extract_log_info_value(compact_value);
	woc.logs.push(value);
	woc.redraw_ui = true;
};

var woc_self_info_handler = function(compact_value) {
	var value = woc_extract_self_info_value(compact_value);
	woc.me_ent  = value.entity;
	woc.me_dead = value.dead;
};

var woc_zone_info_handler = function(compact_value) {
	var value = woc_extract_zone_info_value(compact_value);
	if ( value.users ) {
		woc.users = value.users;
	}
	if ( value.size ) {
		woc.size = value.size;
		woc_check_display_size();
	}
	if ( value.sync ) {
		woc.entities = {};
	}
	if ( value.kills ) {
		for ( var i = 0; i < value.kills.length; ++i ) {
			var id = value.kills[i];
			delete woc.entities[id];
		}
	}
	if ( value.entities ) {
		for ( var i = 0; i < value.entities.length; ++i ) {
			var entity = woc_extract_entity(value.entities[i]);
			woc.entities[entity.id] = entity;
		}
	}
	woc.redraw_map = true;
};

var woc_stats_info_handler = function(compact_value) {
	var value = woc_extract_stats_info_value(compact_value);
	woc.stats = value;
	woc.redraw_ui = true;
};

var woc_grab_info_handler = function(compact_value) {
	var value = woc_extract_grab_info_value(compact_value);
	if ( value.items.length <= 0 ) {
		if ( woc.menu.type == "grab" ) {
			woc.menu  = woc_begin_menu;
			woc.binds = woc_begin_binds;
		}
		return;
	}
	var cur_item = 0;
	if ( woc.menu.type == "grab" ) {
		cur_item = woc.menu.cur_item;
	}
	woc.menu = {
		type     : "grab",
		items    : value.items,
		cur_item : 0
	};
	woc_clamp_menu_items();
	woc.binds = {
		keys : _.union(
			[
				{
					keys : [ROT.VK_ESCAPE],
					func : function() {
						woc.menu  = woc_begin_menu;
						woc.binds = woc_begin_binds;
					}
				},
				{
					keys : [ROT.VK_ENTER, ROT.VK_RETURN],
					func : function() {
						var item  = value.items[woc.menu.cur_item];
						woc_send_action_grab_item(item.id);
					}
				}
			], woc_gen_direction_binds(function(dx,dy) {
				woc.menu.cur_item += dy;
				if ( woc.menu.cur_item < 0 ) {
					woc.menu.cur_item = woc.menu.items.length - 1;
				} else if ( woc.menu.cur_item > woc.menu.items.length - 1 ) {
					woc.menu.cur_item = 0;
				}
			})),
		desc :
			"Choose item and press %c{yellow}Enter%c{} for %c{green}Grab%c{} or " +
			"press %c{yellow}ESC%c{} for cancel"
	};
	woc.redraw_ui = true;
};

var woc_inventory_info_handler = function(compact_value) {
	var value = woc_extract_inventory_info_value(compact_value);
	if ( value.items.length <= 0 ) {
		if ( woc.menu.type == "inventory" ) {
			woc.menu  = woc_begin_menu;
			woc.binds = woc_begin_binds;
		}
		return;
	}
	var cur_item = 0;
	if ( woc.menu.type == "inventory" ) {
		cur_item = woc.menu.cur_item;
	}
	woc.menu = {
		type     : "inventory",
		items    : value.items,
		cur_item : cur_item
	};
	woc_clamp_menu_items();
	woc.binds = {
		keys : _.union(
			[
				{
					keys : [ROT.VK_ESCAPE],
					func : function() {
						woc.menu  = woc_begin_menu;
						woc.binds = woc_begin_binds;
					}
				},
				{
					keys : [ROT.VK_BACK_SPACE, ROT.VK_DELETE],
					func : function() {
						var item  = value.items[woc.menu.cur_item];
						woc_send_action_drop_item(item.id);
					}
				},
				{
					keys : [ROT.VK_ENTER, ROT.VK_RETURN],
					func : function() {
						var item  = value.items[woc.menu.cur_item];
						woc_send_action_use_item(item.id);
					}
				}
			], woc_gen_direction_binds(function(dx,dy) {
				woc.menu.cur_item += dy;
				if ( woc.menu.cur_item < 0 ) {
					woc.menu.cur_item = woc.menu.items.length - 1;
				} else if ( woc.menu.cur_item > woc.menu.items.length - 1 ) {
					woc.menu.cur_item = 0;
				}
			})),
		desc :
			"Press %c{yellow}Backspace%c{} for %c{green}Drop%c{} or " +
			"press %c{yellow}Enter%c{} for %c{green}Use%c{} or " +
			"press %c{yellow}ESC%c{} for cancel"
	};
	woc.redraw_ui = true;
};

// ----------------------------------------------------------------------------
// 
// Actions
// 
// ----------------------------------------------------------------------------

var woc_begin_grab_request = function() {
	woc.client.send("request_grab");
};

var woc_begin_inventory_request = function() {
	woc.client.send("request_inventory");
};

var woc_send_action_use = function(dx, dy) {
	woc.client.send("action_use", [dx, dy]);
};

var woc_send_action_chat = function(text) {
	woc.client.send("action_chat", [text]);
};

var woc_send_action_step = function(dx, dy) {
	woc.client.send("action_step", [dx, dy]);
};

var woc_send_action_grab_item = function(id) {
	woc.client.send("action_grab_item", [id]);
};

var woc_send_action_drop_item = function(id) {
	woc.client.send("action_drop_item", [id]);
};

var woc_send_action_use_item = function(id) {
	woc.client.send("action_use_item", [id]);
};

var woc_begin_use_action = function() {
	woc.binds = {
		keys : _.union(
			[
				{
					keys : [ROT.VK_ESCAPE],
					func : function() {
						woc.binds = woc_begin_binds;
					}
				}
			], woc_gen_direction_binds(function(dx,dy) {
				woc.binds = woc_begin_binds;
				woc_send_action_use(dx, dy);
			})),
		desc :
			"Choose direction for %c{green}Use%c{} or " +
			"press %c{yellow}ESC%c{} for cancel"
	};
};

var woc_begin_chat_action = function() {
	var message = window.prompt("Enter chat message","");
	if ( message != null && message.length > 0 ) {
		woc_send_action_chat(message);
	}
};

var woc_begin_attach_request = function() {
	var name = window.prompt("Enter your name","");
	if ( name != null && name.length > 0 ) {
		woc.client.send("action_attach", [name]);
	}
};

// ----------------------------------------------------------------------------
// 
// Helpers
// 
// ----------------------------------------------------------------------------

var woc_start_ping_loop = function() {
	woc.client.request("ping", [woc.get_game_time()], function(value) {
		var time = value[0];
		woc.ping = woc.get_game_time() - time;
		woc.redraw_ui = true;
	});
	woc.ping_timeout = setTimeout(woc_start_ping_loop, 1000);
};

var woc_stop_ping_loop = function() {
	if ( woc.ping_timeout ) {
		clearTimeout(woc.ping_timeout);
		woc.ping_timeout = null;
	}
};

var woc_clamp_menu_items = function() {
	woc.menu.cur_item = Math.min(woc.menu.cur_item, woc.menu.items.length - 1);
	woc.menu.cur_item = Math.max(woc.menu.cur_item, 0);
};

var woc_input_process = function(key) {
	for ( var i = 0; i < woc.binds.keys.length; ++i ) {
		var bind = woc.binds.keys[i];
		for ( var j = 0; j < bind.keys.length; ++j ) {
			if ( key == bind.keys[j] ) {
				bind.func();
				woc.redraw_ui = true;
				return true;
			}
		}
	}
	return false;
};

var woc_check_display_size = function() {
	var w = woc.size.x + woc.menu_bar_width;
	var h = woc.size.y + woc.status_bar_height + woc.debug_bar_height;
	var opts = woc.display.getOptions();
	if ( w != opts.width || h != opts.height ) {
		woc.display.setOptions({
			width  : w,
			height : h
		});
		woc.redraw_ui = true;
		woc.redraw_map = true;
	}
};

var woc_draw_tiles = function() {
	woc_draw_region(0, 0, woc.size.x, woc.size.y, ".");
};

var woc_draw_entities = function() {
	var map = {};
	for ( var entity_id in woc.entities ) {
		if ( woc.entities.hasOwnProperty(entity_id) ) {
			var entity = woc.entities[entity_id];
			if ( entity && entity.visible ) {
				var pos_id = entity.pos.x + entity.pos.y * woc.size.x;
				if ( entity_id == woc.me_ent ) {
					entity.view = woc.me_dead ?
						woc_views.me_spirit : woc_views.me_ent;
				}
				if ( !map[pos_id] ) {
					map[pos_id] = [entity];
				} else {
					map[pos_id].push(entity);
				}
			}
		}
	}
	for ( var tile_id in map ) {
		if ( map.hasOwnProperty(tile_id) ) {
			var entities = map[tile_id];
			entities.sort(function(a, b) {
				var la = a.view.layer;
				var lb = b.view.layer;
				return (la < lb) ? -1 : (la > lb) ? 1 : 0;
			});
			for ( var i = 0; i < entities.length; ++i ) {
				var entity = entities[i];
				woc_draw_view(entity.pos.x, entity.pos.y, entity.view);
			}
		}
	}
};

var woc_draw_menu_bar = function() {
	var mx = woc.size.x;
	var my = 0;
	var mi = 0;
	woc_draw_region(
		mx,
		my,
		woc.menu_bar_width,
		woc.size.y + woc.status_bar_height,
		' ');
	woc_draw_ver_line(
		mx,
		my,
		woc.size.y + woc.status_bar_height);
	woc_draw_ver_line(
		mx + woc.menu_bar_width - 1,
		my,
		woc.size.y + woc.status_bar_height);
	var width = woc.menu_bar_width - 2;
	if ( woc.binds.desc.length > 0 ) {
		var header = "-Input-";
		mi += woc.display.drawText(
			mx + 1 + width/2 - header.length/2,
			mi,
			"%c{yellow}" + header,
			width);
		mi += 1;
		mi += woc.display.drawText(
			mx + 1,
			mi,
			woc.binds.desc,
			width);
		mi += 1;
	}
	if ( woc.menu.items.length > 0 ) {
		var header = "-Items-";
		mi += woc.display.drawText(
			mx + 1 + width/2 - header.length/2,
			mi,
			"%c{yellow}" + header,
			width);
		var items = woc.menu.items;
		var cur_item = woc.menu.cur_item;
		for ( var i = 0; i < items.length; ++i ) {
			var item = items[i];
			var title = item.title;
			if ( item.equipped ) {
				title = title + "(e)";
			}
			mi += woc.display.drawText(
				mx + 1,
				mi,
				(i == cur_item ? "%c{red}" : "") + title,
				width);
		}
	}
};

var woc_log_text_colorize = function(level, text) {
	switch ( level ) {
		case 0  : return text;
		case 1  : return "%c{yellow}" + text;
		case 2  : return "%c{green}"  + text;
		default : return text;
	}
};

var woc_draw_status_bar = function() {
	var bx = 0;
	var by = woc.size.y;
	var bw = woc.size.x;
	var bh = woc.status_bar_height;
	woc_draw_region(bx, by, bw, bh, ' ');
	woc_draw_hor_line(bx, by, bw);
	woc_draw_hor_line(bx, by + bh - 1, bw);
	if ( woc.stats ) {
		var hp_text = "(HP:{0}/{1})-(EXP:{2}/{3},LEV:{4})".format(
			woc.stats.Hp, woc.stats.MaxHp,
			woc.stats.Exp, woc.stats.NextExp, woc.stats.Lev);
		woc.display.drawText(bx, by, hp_text);
		var stats_text = "(DMG:{0}-{1},DEF:{2})-(STR:{3},DEX:{4},INT:{5})".format(
			woc.stats.MinDmg, woc.stats.MaxDmg, woc.stats.Def,
			woc.stats.Str, woc.stats.Dex, woc.stats.Int);
		woc.display.drawText(bx + bw - stats_text.length, by, stats_text);
	}
	if ( woc.logs ) {
		var logs_count = -woc.status_bar_height + 1;
		_.reduce(woc.logs.slice(logs_count + 1), function(acc, msg) {
			var text = "- " + msg.text.slice(0, bw - 4);
			return acc + woc.display.drawText(
				bx + 1, by + 1 + acc,
				woc_log_text_colorize(msg.level, text), bw - 2);
		}, 0);
	}
};

var woc_draw_debug_bar = function() {
	var dx = 0;
	var dy = woc.size.y + woc.status_bar_height;
	woc_draw_region(dx, dy, woc.size.x, woc.debug_bar_height, ' ');
	var stat_text = "Users: {0} | Ping: {1}".format(
		woc.users,
		woc.ping);
	var network_text = "send: {0}/{1} | receive: {2}/{3}".format(
		woc.client.get_sps(), woc.client.get_sbps(),
		woc.client.get_rps(), woc.client.get_rbps());
	woc.display.drawText(dx, dy + 0, stat_text);
	woc.display.drawText(dx, dy + 1, network_text);
};

var woc_draw_view = function(x, y, view) {
	woc.display.draw(
		x, y, view.ch, view.fg, view.bg);
};

var woc_draw_hor_line = function(x, y, w) {
	for ( var i = x; i < x + w; ++i ) {
		woc.display.draw(i, y, "-");
	}
};

var woc_draw_ver_line = function(x, y, h) {
	for ( var i = y; i < y + h; ++i ) {
		woc.display.draw(x, i, "|");
	}
};

var woc_draw_region = function(x, y, w, h, c) {
	for ( var i = y; i < y + h; ++i ) {
		for ( var j = x; j < x + w; ++j ) {
			woc.display.draw(j, i, c);
		}
	}
};

// ----------------------------------------------------------------------------
// 
// Extracts
// 
// ----------------------------------------------------------------------------

var woc_extract_stats_info_value = function(value) {
	var stats = {
		MinDmg  : 0,
		MaxDmg  : 0,
		Def     : 0,
		Hp      : 0,
		MaxHp   : 0,
		Str     : 0,
		Dex     : 0,
		Int     : 0,
		Lev     : 0,
		Exp     : 0,
		NextExp : 0
	};
	if ( value ) {
		stats.MinDmg  = value[0];
		stats.MaxDmg  = value[1];
		stats.Def     = value[2];
		stats.Hp      = value[3];
		stats.MaxHp   = value[4];
		stats.Str     = value[5];
		stats.Dex     = value[6];
		stats.Int     = value[7];
		stats.Lev     = value[8];
		stats.Exp     = value[9];
		stats.NextExp = value[10];
	}
	return stats;
};

var woc_extract_grab_info_value = function(value) {
	var info = {
		items : []
	};
	var compact_items = value ? value[0] : [];
	for ( var i = 0; i < compact_items.length; ++i ) {
		var compact_item = compact_items[i];
		var item = {
			id    : compact_item[0],
			title : compact_item[1]
		};
		info.items.push(item);
	}
	return info;
};

var woc_extract_inventory_info_value = function(value) {
	var info = {
		items : []
	};
	var compact_items = value ? value[0] : [];
	for ( var i = 0; i < compact_items.length; ++i ) {
		var compact_item = compact_items[i];
		var item = {
			id       : compact_item[0],
			title    : compact_item[1],
			equipped : woc_extract_bool(compact_item[2])
		};
		info.items.push(item);
	}
	return info;
};

var woc_extract_self_info_value = function(value) {
	return value ? {
		entity : value[0],
		dead   : woc_extract_bool(value[1])
	} : value;
};

var woc_extract_log_info_value = function(value) {
	var msg = {
		level : 0,
		text  : ""
	};
	var type = value ? value[0] : -1;
	switch ( type ) {
		case 0:
			msg.level = 0;
			msg.text  = "exp +{0}".format(value[1]);
			break;
		case 1:
			msg.level = 2;
			msg.text  = "News: {0}".format(value[1]);
			break;
		case 2:
			msg.level = 1;
			msg.text  = "'{0}': {1}".format(value[1], value[2]);
			break;
		case 3:
			msg.level = 1;
			msg.text  = "'{0}' is dead!".format(value[1]);
			break;
		case 4:
			msg.level = 1;
			msg.text  = "'{0}' is alive!".format(value[1]);
			break;
		case 5:
			msg.level = 2;
			msg.text  = "'{0}' grab '{1}'!".format(value[1], value[2]);
			break;
		case 6:
			msg.level = 2;
			msg.text  = "'{0}' drop '{1}'!".format(value[1], value[2]);
			break;
		case 7:
			msg.level = 1;
			msg.text  = "'{0}' has joined the game!".format(value[1]);
			break;
		case 8:
			msg.level = 1;
			msg.text  = "'{0}' has left the game!".format(value[1]);
			break;
		case 9:
			msg.level = 0;
			msg.text  = "'{0}' -{1}".format(value[1], value[2]);
			break;
		case 10:
			msg.level = 0;
			msg.text  = "Private: {0}".format(value[1]);
			break;
		case 11:
			msg.level = 2;
			msg.text  = "'{0}' level up to '{1}'".format(
				value[1], value[2]);
			break;
		case 12:
			msg.level = 1;
			msg.text  = "User '{0}' teleport to '{1}'".format(
				value[1], value[2]);
			break;
		case 13:
			msg.level = 1;
			msg.text  = "User '{0}' teleport from '{1}'".format(
				value[1], value[2]);
			break;
		default:
			msg.level = 0;
			msg.text  = "";
	}
	return msg;
};

var woc_extract_zone_info_value = function(value) {
	return value ? {
		users    : value[0],
		size     : woc_extract_vec2(value[1]),
		sync     : woc_extract_bool(value[2]),
		entities : value[3],
		kills    : value[4]
	} : value;
};

var woc_extract_entity = function(entity) {
	return entity ? {
		id      : entity[0],
		pos     : woc_extract_vec2(entity[1]),
		view    : woc_extract_view(entity[2]),
		visible : woc_extract_bool(entity[3])
	} : entity;
};

var woc_extract_bool = function(bool) {
	return bool == 1 ? true : false;
};

var woc_extract_vec2 = function(vec2) {
	return vec2 ? {
		x : vec2[0],
		y : vec2[1]
	} : vec2;
};

var woc_extract_view = function(v) {
	switch ( v ) {
		case 0  : return woc_views.none;
		case 1  : return woc_views.ground;
		case 2  : return woc_views.wall;
		case 3  : return woc_views.tree;
		case 4  : return woc_views.rock;
		case 5  : return woc_views.water;
		case 6  : return woc_views.ver_window;
		case 7  : return woc_views.hor_window;
		case 8  : return woc_views.opened_door;
		case 9  : return woc_views.closed_door;
		case 10 : return woc_views.orc;
		case 11 : return woc_views.rat;
		case 12 : return woc_views.human;
		case 13 : return woc_views.hobbit;
		case 14 : return woc_views.zombie;
		case 15 : return woc_views.player;
		case 16 : return woc_views.spirit;
		case 17 : return woc_views.respawn;
		case 18 : return woc_views.portal;
		case 19 : return woc_views.sword;
		case 20 : return woc_views.helmet;
		case 21 : return woc_views.potion;
		default : return woc_views.none;
	}
};
