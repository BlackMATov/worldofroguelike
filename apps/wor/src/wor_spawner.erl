-module(wor_spawner).

%% public
-export([tick/1]).
-export([attach_user/2]).
-export([detach_user/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	ok = attach_process(World, WorldTicks),
	ok = detach_process(World).

%% ------------------------------------
%% attach_user
%% ------------------------------------

-spec attach_user(
	pid(), binary()
) -> ok.

attach_user(User, Name) ->
	wor_entmgr:update_component(fun(Spawner) ->
		Attach = Spawner#wor_spawner_comp.attach,
		NewAttach = queue:in({User, Name}, Attach),
		Spawner#wor_spawner_comp{attach = NewAttach}
	end, spawner).

%% ------------------------------------
%% detach_user
%% ------------------------------------

-spec detach_user(
	pid()
) -> ok.

detach_user(User) ->
	wor_entmgr:update_component(fun(Spawner) ->
		Detach = Spawner#wor_spawner_comp.detach,
		NewDetach = queue:in(User, Detach),
		Spawner#wor_spawner_comp{detach = NewDetach}
	end, spawner).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% is_attached
%% ------------------------------------

-spec is_attached(
	pid()
) -> boolean().

is_attached(User) ->
	dict:size(wor_entmgr:filter_components(fun(_Ent, Userable) ->
		#wor_userable_comp{user = OtherUser} = Userable,
		User == OtherUser
	end, userable)) > 0.

%% ------------------------------------
%% attach_process
%% ------------------------------------

-spec attach_process(
	wor_entity(), integer()
) -> ok.

attach_process(SpawnerEnt, WorldTicks) ->
	{User, Name} = wor_entmgr:rupdate_component(SpawnerEnt, fun(Spawner) ->
		#wor_spawner_comp{attach = Attach} = Spawner,
		case queue:out(Attach) of
			{{value, {AttachUser, AttachName}}, NewAttach} ->
				{
					Spawner#wor_spawner_comp{attach = NewAttach},
					{AttachUser, AttachName}
				};
			_ ->
				{
					Spawner,
					{undefined, undefined}
				}
		end
	end, spawner),
	case {User, Name} of
		{undefined, _} -> ok;
		{_, undefined} -> ok;
		{User, Name} ->
			case is_attached(User) of
				true  -> ok;
				false ->
					case try_attach_user(User, Name, WorldTicks) of
						true  -> ok;
						false -> attach_user(User, Name)
					end
			end
	end.

%% ------------------------------------
%% detach_process
%% ------------------------------------

-spec detach_process(
	wor_entity()
) -> ok.

detach_process(SpawnerEnt) ->
	User = wor_entmgr:rupdate_component(SpawnerEnt, fun(Spawner) ->
		#wor_spawner_comp{detach = Detach} = Spawner,
		case queue:out(Detach) of
			{{value, DetachUser}, NewDetach} ->
				{
					Spawner#wor_spawner_comp{detach = NewDetach},
					DetachUser
				};
			_ ->
				{
					Spawner,
					undefined
				}
		end
	end, spawner),
	case User of
		undefined -> ok;
		User ->
			case try_detach_user(User) of
				true  -> ok;
				false -> detach_user(User)
			end
	end.

%% ------------------------------------
%% try_attach_user
%% ------------------------------------

-spec try_attach_user(
	pid(), binary(), integer()
) -> boolean().

try_attach_user(User, Name, Ticks) ->
	Respawns = dict:fold(fun(ZoneEnt, _Zone, AccIn) ->
		wor_entmgr:fold_components(fun(RespawnEnt, _, InternalAccIn) ->
			[RespawnEnt | InternalAccIn]
		end, AccIn, [placeable, respawn],
		{zone_index, wor_utils:zone_index(ZoneEnt)})
	end, [], wor_entmgr:filter_components(fun(_ZoneEnt, Zone) ->
		Zone#wor_zone_comp.started
	end, zone)),
	lists:foldl(fun(RespawnEnt, AccIn) ->
		case AccIn of
			true  -> AccIn;
			false -> wor_respawn:spawn_user(RespawnEnt, User, Name, Ticks)
		end
	end, false, Respawns).

%% ------------------------------------
%% try_detach_user
%% ------------------------------------

-spec try_detach_user(
	pid()
) -> boolean().

try_detach_user(User) ->
	dict:fold(fun(UserableEnt, _Userable, _AccIn) ->
		ok = wor_logger:do_log_detach(UserableEnt),
		ok = wor_inventory:do_drop_all(UserableEnt),
		ok = wor_entmgr:remove_entity(UserableEnt),
		true
	end, false, wor_entmgr:filter_components(fun(_UserableEnt, Userable) ->
		#wor_userable_comp{user = OtherUser} = Userable,
		User == OtherUser
	end, userable)).
