-module(wor_health).

%% public
-export([tick/1]).
-export([do_add_hp/2]).
-export([do_damage/2]).
-export([do_restore/1]).
-export([do_recalculate/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_add_hp
%% ------------------------------------

-spec do_add_hp(
	wor_entity(), integer()
) -> ok.

do_add_hp(Entity, AddHp) ->
	case wor_entmgr:has_component(Entity, health) of
		true ->
			ok = wor_entmgr:update_component(Entity, fun(Health) ->
				#wor_health_comp{hp = Hp, max = Max} = Health,
				NewHp = min(Hp + AddHp, Max),
				Health#wor_health_comp{hp = NewHp}
			end, health),
			ok = wor_requester:send_stats_info(Entity);
		false -> ok
	end.

%% ------------------------------------
%% do_damage
%% ------------------------------------

-spec do_damage(
	wor_entity(), integer()
) -> ok.

do_damage(Entity, Damage) ->
	case wor_entmgr:has_component(Entity, health) of
		true ->
			CurHp = wor_entmgr:rupdate_component(Entity, fun(Health) ->
				#wor_health_comp{hp = Hp} = Health,
				NewHp = max(Hp - Damage, 0),
				NewHealth = Health#wor_health_comp{hp = NewHp},
				{NewHealth, NewHp}
			end, health),
			ok = case CurHp =< 0 of
				true  -> wor_dead:do_dead(Entity);
				false -> ok
			end,
			ok = wor_requester:send_stats_info(Entity);
		false ->
			ok
	end.

%% ------------------------------------
%% do_restore
%% ------------------------------------

-spec do_restore(
	wor_entity()
) -> ok.

do_restore(Entity) ->
	case wor_entmgr:has_component(Entity, health) of
		true ->
			#wor_health_comp{hp = Hp, max = Max} =
				wor_entmgr:get_component(Entity, health),
			do_add_hp(Entity, Max - Hp);
		false -> ok
	end.

%% ------------------------------------
%% do_recalculate
%% ------------------------------------

-spec do_recalculate(
	wor_entity()
) -> ok.

do_recalculate(Entity) ->
	case can_do_recalculate(Entity) of
		true ->
			#wor_stats_comp{str = Str} =
				wor_entmgr:get_component(Entity, stats),
			wor_entmgr:update_component(Entity, fun(Health) ->
				#wor_health_comp{hp = Hp} = Health,
				NewMax = Str * 10,
				NewHp  = min(Hp, NewMax),
				Health#wor_health_comp{hp = NewHp, max = NewMax}
			end, health),
			wor_requester:send_stats_info(Entity);
		false -> ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_recalculate
%% ------------------------------------

-spec can_do_recalculate(
	wor_entity()
) -> boolean().

can_do_recalculate(Entity) ->
	wor_entmgr:has_component(Entity, stats),
	wor_entmgr:has_component(Entity, health).
