-module(wor_trasher).

%% public
-export([tick/1]).
-export([do_trash/1]).
-export([do_untrash/1]).

-include("types.hrl").
-define(TRASH_TIMER, 60000).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	ok = trashing_process(World, WorldTicks),
	ok = cleaning_process(WorldTicks).

%% ------------------------------------
%% do_trash
%% ------------------------------------

-spec do_trash(
	wor_entity()
) -> ok.

do_trash(Entity) ->
	wor_entmgr:update_component(fun(Trasher) ->
		#wor_trasher_comp{trash = Trash} = Trasher,
		NewTrash = sets:add_element(Entity, Trash),
		Trasher#wor_trasher_comp{trash = NewTrash}
	end, trasher).

%% ------------------------------------
%% do_untrash
%% ------------------------------------

-spec do_untrash(
	wor_entity()
) -> ok.

do_untrash(Entity) ->
	ok = case wor_entmgr:has_component(Entity, trash) of
		true  -> wor_entmgr:remove_component(Entity, trash);
		false -> ok
	end,
	ok = wor_entmgr:update_component(fun(Trasher) ->
		#wor_trasher_comp{trash = Trash} = Trasher,
		NewTrash = sets:del_element(Entity, Trash),
		Trasher#wor_trasher_comp{trash = NewTrash}
	end, trasher).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% trashing_process
%% ------------------------------------

-spec trashing_process(
	wor_entity(), integer()
) -> ok.

trashing_process(TrasherEnt, WorldTicks) ->
	#wor_trasher_comp{trash = Trash} =
		wor_entmgr:get_component(TrasherEnt, trasher),
	ok = sets:fold(fun(TrashEnt, AccIn) ->
		AccIn = case wor_entmgr:has_component(TrashEnt, trash) of
			true  -> ok;
			false -> wor_entmgr:create_component(TrashEnt, trash,
				wor_comps:new_trash(WorldTicks + ?TRASH_TIMER))
		end
	end, ok, Trash),
	ok = wor_entmgr:update_component(TrasherEnt, fun(OtherTrasher) ->
		OtherTrasher#wor_trasher_comp{trash = sets:new()}
	end, trasher).

%% ------------------------------------
%% cleaning_process
%% ------------------------------------

-spec cleaning_process(
	integer()
) -> ok.

cleaning_process(WorldTicks) ->
	wor_entmgr:fold_components(fun(TrashEnt, Trash, AccIn) ->
		#wor_trash_comp{ticks = Ticks} = Trash,
		AccIn = case Ticks =< WorldTicks of
			true ->
				ok = do_untrash(TrashEnt),
				ok = wor_dead:do_dead(TrashEnt);
			false ->
				ok
		end
	end, ok, trash).
