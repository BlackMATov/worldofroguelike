-module(wor_inventory).

%% public
-export([tick/1]).
-export([do_grab/2]).
-export([do_grab/3]).
-export([do_drop/2]).
-export([do_drop/3]).
-export([do_drop_all/1]).
-export([do_drop_all/2]).
-export([has_item/2]).
-export([has_items/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_grab
%% ------------------------------------

-spec do_grab(
	wor_entity(), wor_entity() | list(wor_entity())
) -> ok.

do_grab(Entity, ItemOtItems) ->
	do_grab(Entity, ItemOtItems, false).

-spec do_grab(
	wor_entity(), wor_entity() | list(wor_entity()), boolean()
) -> ok.

do_grab(Entity, ItemEnt, Silent) when is_integer(ItemEnt) ->
	case can_do_grab(Entity, ItemEnt) of
		true ->
			ok = wor_entmgr:update_component(ItemEnt, fun(Placeable) ->
				Placeable#wor_placeable_comp{pos = {0,0}, zone = undefined}
			end, placeable),
			ok = wor_entmgr:update_component(Entity, fun(Inventory) ->
				#wor_inventory_comp{items = Items} = Inventory,
				NewItems = sets:add_element(ItemEnt, Items),
				Inventory#wor_inventory_comp{items = NewItems}
			end, inventory),
			ok = wor_trasher:do_untrash(ItemEnt),
			ok = case Silent of
				true  -> ok;
				false -> wor_logger:do_log_grab(Entity, ItemEnt)
			end,
			ok = wor_requester:send_grab_info(Entity),
			ok = wor_equip_slots:do_try_equip(Entity, ItemEnt);
		false ->
			ok
	end;
do_grab(_Entity, Items = [], _Silent) when is_list(Items) ->
	ok;
do_grab(Entity, Items = [ItemEnt|Tail], Silent) when is_list(Items) ->
	ok = do_grab(Entity, ItemEnt, Silent),
	do_grab(Entity, Tail, Silent).

%% ------------------------------------
%% do_drop
%% ------------------------------------

-spec do_drop(
	wor_entity(), wor_entity()
) -> ok.

do_drop(Entity, ItemEnt) ->
	do_drop(Entity, ItemEnt, false).

-spec do_drop(
	wor_entity(), wor_entity(), boolean()
) -> ok.

do_drop(Entity, ItemEnt, Silent) ->
	case can_do_drop(Entity, ItemEnt) of
		true ->
			ok = wor_equip_slots:do_unequip(Entity, ItemEnt),
			#wor_placeable_comp{pos = Pos, zone = ZoneEnt} =
				wor_entmgr:get_component(Entity, placeable),
			ok = wor_entmgr:update_component(ItemEnt, fun(Placeable) ->
				Placeable#wor_placeable_comp{pos = Pos, zone = ZoneEnt}
			end, placeable),
			ok = wor_entmgr:update_component(Entity, fun(Inventory) ->
				#wor_inventory_comp{items = Items} = Inventory,
				NewItems = sets:del_element(ItemEnt, Items),
				Inventory#wor_inventory_comp{items = NewItems}
			end, inventory),
			ok = wor_trasher:do_trash(ItemEnt),
			ok = case Silent of
				true  -> ok;
				false -> wor_logger:do_log_drop(Entity, ItemEnt)
			end,
			ok = wor_requester:send_inventory_info(Entity);
		false ->
			ok
	end.

%% ------------------------------------
%% do_drop_all
%% ------------------------------------

-spec do_drop_all(
	wor_entity()
) -> ok.

do_drop_all(Entity) ->
	do_drop_all(Entity, false).

-spec do_drop_all(
	wor_entity(), boolean()
) -> ok.

do_drop_all(Entity, Silent) ->
	case can_do_drop_all(Entity) of
		true ->
			#wor_inventory_comp{items = Items} =
				wor_entmgr:get_component(Entity, inventory),
			sets:fold(fun(ItemEnt, AccIn) ->
				AccIn = do_drop(Entity, ItemEnt, Silent)
			end, ok, Items);
		false ->
			ok
	end.

%% ------------------------------------
%% has_item
%% ------------------------------------

-spec has_item(
	wor_entity(), wor_entity()
) -> boolean().

has_item(Entity, ItemEnt) ->
	#wor_inventory_comp{items = Items} =
		wor_entmgr:get_component(Entity, inventory),
	sets:is_element(ItemEnt, Items).

%% ------------------------------------
%% has_items
%% ------------------------------------

-spec has_items(
	wor_entity(), list(wor_entity())
) -> boolean().

has_items(_Entity, []) ->
	true;
has_items(Entity, [ItemEnt|Tail]) ->
	case has_item(Entity, ItemEnt) of
		true  -> has_items(Entity, Tail);
		false -> false
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_grab
%% ------------------------------------

-spec can_do_grab(
	wor_entity(), wor_entity()
) -> boolean().

can_do_grab(Entity, ItemEnt) ->
	YesNo =
		wor_entmgr:has_component(Entity, inventory) andalso
		wor_entmgr:has_component(ItemEnt, inv_item),
	case YesNo of
		true ->
			#wor_placeable_comp{pos = Pos, zone = ZoneEnt} =
				wor_entmgr:get_component(Entity, placeable),
			#wor_placeable_comp{pos = ItemPos, zone = ItemZoneEnt} =
				wor_entmgr:get_component(ItemEnt, placeable),
			Pos == ItemPos andalso ZoneEnt == ItemZoneEnt;
		false ->
			false
	end.

%% ------------------------------------
%% can_do_drop
%% ------------------------------------

-spec can_do_drop(
	wor_entity(), wor_entity()
) -> boolean().

can_do_drop(Entity, ItemEnt) ->
	wor_entmgr:has_component(Entity, inventory) andalso
	wor_entmgr:has_component(ItemEnt, inv_item) andalso
	has_item(Entity, ItemEnt).

%% ------------------------------------
%% can_do_drop_all
%% ------------------------------------

-spec can_do_drop_all(
	wor_entity()
) -> boolean().

can_do_drop_all(Entity) ->
	wor_entmgr:has_component(Entity, inventory).
