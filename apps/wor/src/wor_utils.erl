-module(wor_utils).

%% public
-export([bool_to_json/1]).
-export([vec2_to_json/1]).
-export([view_to_json/1]).
-export([log_type_to_json/1]).

%% public
-export([zone_filename/1]).
-export([world_filename/0]).

%% public
-export([tile_index/1]).
-export([zone_index/1]).
-export([farm_index/1]).
-export([sign/1]).
-export([zero_sign/1]).
-export([clamp/3]).
-export([generate_line/2]).
-export([random_integer/1]).
-export([random_integer/2]).
-export([random_direction/0]).
-export([random_zero_direction/0]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% XXX_to_json
%% ------------------------------------

-spec bool_to_json(boolean()) -> wor_json().
bool_to_json(true)  -> 1;
bool_to_json(false) -> 0.

-spec vec2_to_json(wor_vec2()) -> wor_json().
vec2_to_json({X, Y}) -> [X,Y].

-spec view_to_json(wor_view_type()) -> integer().
view_to_json(none)        -> 0;
view_to_json(ground)      -> 1;
view_to_json(wall)        -> 2;
view_to_json(tree)        -> 3;
view_to_json(rock)        -> 4;
view_to_json(water)       -> 5;
view_to_json(ver_window)  -> 6;
view_to_json(hor_window)  -> 7;
view_to_json(opened_door) -> 8;
view_to_json(closed_door) -> 9;
view_to_json(orc)         -> 10;
view_to_json(rat)         -> 11;
view_to_json(human)       -> 12;
view_to_json(hobbit)      -> 13;
view_to_json(zombie)      -> 14;
view_to_json(player)      -> 15;
view_to_json(spirit)      -> 16;
view_to_json(respawn)     -> 17;
view_to_json(portal)      -> 18;
view_to_json(sword)       -> 19;
view_to_json(helmet)      -> 20;
view_to_json(potion)      -> 21.

-spec log_type_to_json(wor_logger:log_type()) -> integer().
log_type_to_json(exp)           -> 0;
log_type_to_json(news)          -> 1;
log_type_to_json(chat)          -> 2;
log_type_to_json(dead)          -> 3;
log_type_to_json(alive)         -> 4;
log_type_to_json(grab)          -> 5;
log_type_to_json(drop)          -> 6;
log_type_to_json(attach)        -> 7;
log_type_to_json(detach)        -> 8;
log_type_to_json(attack)        -> 9;
log_type_to_json(private)       -> 10;
log_type_to_json(level_up)      -> 11;
log_type_to_json(teleport_to)   -> 12;
log_type_to_json(teleport_from) -> 13.

%% ------------------------------------
%% XXX_filename
%% ------------------------------------

-spec zone_filename(binary()) -> string().
zone_filename(ZoneName) ->
	code:priv_dir(wor) ++ "/world/" ++ binary_to_list(ZoneName) ++ ".json".

-spec world_filename() -> string().
world_filename() ->
	code:priv_dir(wor) ++ "/world/world.json".

%% ------------------------------------
%% XXX_index
%% ------------------------------------

-spec tile_index(wor_vec2()) -> integer().
tile_index({X,Y}) ->
	Y * ?WOR_MAX_ZONE_WIDTH + X.

-spec zone_index(wor_entity()) -> integer().
zone_index(ZoneEnt) ->
	ZoneEnt.

-spec farm_index(wor_entity()) -> integer().
farm_index(FarmEnt) ->
	FarmEnt.

%% ------------------------------------
%% sign
%% ------------------------------------

-spec sign(integer()) -> integer().
sign(Value) ->
	case Value < 0 of
		true  -> -1;
		false ->  1
	end.

-spec zero_sign(integer()) -> integer().
zero_sign(Value) ->
	case Value == 0 of
		true  -> 0;
		false -> sign(Value)
	end.

%% ------------------------------------
%% clamp
%% ------------------------------------

-spec clamp(number(), number(), number()) -> number().
clamp(Value, Min, Max) when Min =< Max ->
	min(max(Value, Min), Max).

%% ------------------------------------
%% generate_line
%% ------------------------------------

-spec generate_line(
	wor_vec2(), wor_vec2()
) -> list(wor_vec2()).

generate_line({X0,Y0}, {X1,Y1}) ->
	Dx = abs(X1 - X0),
	Dy = abs(Y1 - Y0),
	Sx = sign(X1 - X0),
	Sy = sign(Y1 - Y0),
	Er = Dx - Dy,
	generate_line_step({X0,Y0}, {X1,Y1}, {Sx,Sy}, {Dx,Dy}, Er, []).

-spec generate_line_step(
	wor_vec2(), wor_vec2(), wor_vec2(),
	wor_vec2(), integer(), list(wor_vec2())
) -> list(wor_vec2()).

generate_line_step(P = {X,Y}, P, _S, _D, _Er, Acc) ->
	lists:reverse([{X,Y}|Acc]);
generate_line_step({X,Y}, {X1,Y1}, {Sx,Sy}, {Dx,Dy}, Er0, Acc) ->
	De = 2 * Er0,
	{X0, Er1} = case De > -Dy of
		true  -> {X + Sx, Er0 - Dy};
		false -> {X, Er0}
	end,
	{Y0, Er2} = case De < Dx of
		true  -> {Y + Sy, Er1 + Dx};
		false -> {Y, Er1}
	end,
	generate_line_step({X0,Y0}, {X1,Y1}, {Sx,Sy}, {Dx,Dy}, Er2, [{X,Y}|Acc]).

%% ------------------------------------
%% random_integer
%% ------------------------------------

-spec random_integer(integer()) -> integer().
random_integer(Max) ->
	random_integer(0, Max).

-spec random_integer(integer(), integer()) -> integer().
random_integer(Min, Max) when Min >= 0, Min =< Max ->
	random:uniform(Max - Min + 1) + Min - 1.

%% ------------------------------------
%% random_direction
%% ------------------------------------

-spec random_direction() -> wor_vec2().
random_direction() ->
	case wor_utils:random_integer(7) of
		0 -> { 1, 1};
		1 -> {-1, 1};
		2 -> { 0, 1};
		3 -> { 1,-1};
		4 -> {-1,-1};
		5 -> { 0,-1};
		6 -> { 1, 0};
		7 -> {-1, 0}
	end.

-spec random_zero_direction() -> wor_vec2().
random_zero_direction() ->
	case wor_utils:random_integer(8) of
		0 -> { 1, 1};
		1 -> {-1, 1};
		2 -> { 0, 1};
		3 -> { 1,-1};
		4 -> {-1,-1};
		5 -> { 0,-1};
		6 -> { 1, 0};
		7 -> {-1, 0};
		8 -> { 0, 0}
	end.
