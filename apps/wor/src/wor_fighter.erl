-module(wor_fighter).

%% public
-export([tick/1]).
-export([do_attack/2]).
-export([do_recalculate/1]).

-include("types.hrl").
-define(ATTACK_COOLDOWN, 200).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_attack
%% ------------------------------------

-spec do_attack(
	wor_entity(), wor_entity()
) -> ok.

do_attack(Who, Whom) ->
	case can_do_attack(Who, Whom) of
		true ->
			[
				#wor_fighter_comp{def = Def},
				#wor_nameable_comp{name = WhomName}
			] = wor_entmgr:get_entity_components(Whom, [fighter, nameable]),
			#wor_fighter_comp{min_dmg = MinDmg, max_dmg = MaxDmg} =
				wor_entmgr:get_component(Who, fighter),
			RndDmg  = wor_utils:random_integer(MinDmg, MaxDmg),
			FullDmg = max(RndDmg - Def, 0),
			ok = wor_health:do_damage(Whom, FullDmg),
			ok = wor_brain:do_cooldown(Who, ?ATTACK_COOLDOWN),
			ok = wor_logger:do_log_attack(Who,  WhomName, FullDmg),
			ok = wor_logger:do_log_attack(Whom, WhomName, FullDmg),
			case wor_dead:is_dead(Whom) of
				true  -> wor_experience:do_add_kill_experience(Who, Whom);
				false -> ok
			end;
		false ->
			ok
	end.

%% ------------------------------------
%% do_recalculate
%% ------------------------------------

-spec do_recalculate(
	wor_entity()
) -> ok.

do_recalculate(Entity) ->
	case can_do_recalculate(Entity) of
		true ->
			#wor_stats_comp{str = Str, dex = Dex} =
				wor_entmgr:get_component(Entity, stats),
			Def    = recalculate_stat(Entity, def,     Dex),
			Dmg    = recalculate_stat(Entity, dmg,     Str),
			MinDmg = recalculate_stat(Entity, min_dmg, Dmg),
			MaxDmg = recalculate_stat(Entity, max_dmg, Dmg),
			wor_entmgr:update_component(Entity, fun(Fighter) ->
				Fighter#wor_fighter_comp{
					def     = Def,
					min_dmg = MinDmg,
					max_dmg = MaxDmg
				}
			end, fighter),
			wor_requester:send_stats_info(Entity);
		false -> ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_attack
%% ------------------------------------

-spec can_do_attack(
	wor_entity(), wor_entity()
) -> boolean().

can_do_attack(Who, Whom) ->
	wor_entmgr:has_components(Who,  [fighter, relation]) andalso
	wor_entmgr:has_components(Whom, [fighter, relation]) andalso
	wor_relation:is_enemies(Who, Whom).

%% ------------------------------------
%% can_do_recalculate
%% ------------------------------------

-spec can_do_recalculate(
	wor_entity()
) -> boolean().

can_do_recalculate(Entity) ->
	wor_entmgr:has_component(Entity, stats),
	wor_entmgr:has_component(Entity, fighter).

%% ------------------------------------
%% recalculate_stat
%% ------------------------------------

-spec recalculate_stat(
	wor_entity(), wor_stats:stat_type(), integer()
) -> integer().

recalculate_stat(Entity, StatName, Base) ->
	case wor_entmgr:find_component(Entity, equip_slots) of
		{ok, EquipSlots} ->
			#wor_equip_slots_comp{slots = Slots} = EquipSlots,
			dict:fold(fun(_Slot, ItemEnt, AccIn) ->
				case ItemEnt of
					undefined -> AccIn;
					ItemEnt   -> add_item_stat(ItemEnt, StatName, AccIn)
				end
			end, Base, Slots);
		_ ->
			Base
	end.

%% ------------------------------------
%% add_item_stat
%% ------------------------------------

-spec add_item_stat(
	wor_entity(), wor_stats:stat_type(), integer()
) -> integer().

add_item_stat(ItemEnt, StatName, Base) ->
	case wor_entmgr:find_component(ItemEnt, add_stat) of
		{ok, AddStat} ->
			#wor_add_stat_comp{stats = Stats} = AddStat,
			proplists:get_value(StatName, Stats, 0) + Base;
		_ ->
			Base
	end.
