-module(wor_btree_nodes).

%% public
-export([wait/1]).
-export([wait/2]).
-export([random_step/0]).
-export([if_has_target/0]).
-export([if_target_los/1]).
-export([find_target/1]).
-export([step_to_target/0]).
-export([attack_target/0]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% wait
%% ------------------------------------

-spec wait(integer()) -> wor_btree:bt_node().
wait(Ticks) ->
	wait(Ticks, Ticks).

-spec wait(integer(), integer()) -> wor_btree:bt_node().
wait(Min, Max) ->
	wor_btree:bt_act(fun(_Entity, Blackboard) ->
		Cooldown = wor_utils:random_integer(Min, Max),
		{
			success,
			wor_action:new(wait, [{cooldown, Cooldown}]),
			Blackboard
		}
	end).

%% ------------------------------------
%% random_step
%% ------------------------------------

-spec random_step() -> wor_btree:bt_node().
random_step() ->
	wor_btree:bt_act(fun(_Entity, Blackboard) ->
		{
			success,
			wor_action:new(random_step),
			Blackboard
		}
	end).

%% ------------------------------------
%% if_has_target
%% ------------------------------------

-spec if_has_target() -> wor_btree:bt_node().
if_has_target() ->
	wor_btree:bt_cond(fun(_Entity, Blackboard) ->
		case dict:find(target, Blackboard) of
			{ok, Target} -> wor_entmgr:has_entity(Target);
			_ -> false
		end
	end).

%% ------------------------------------
%% if_target_los
%% ------------------------------------

-spec if_target_los(integer()) -> wor_btree:bt_node().
if_target_los(Dist) ->
	wor_btree:bt_cond(fun(Entity, Blackboard) ->
		Target = dict:fetch(target, Blackboard),
		#wor_placeable_comp{
			pos  = SPos,
			zone = SZoneEnt
		} = wor_entmgr:get_component(Entity, placeable),
		#wor_placeable_comp{
			pos  = TPos,
			zone = TZoneEnt
		} = wor_entmgr:get_component(Target, placeable),
		Line = wor_utils:generate_line(SPos, TPos),
		Length = length(Line),
		case (SZoneEnt == TZoneEnt) andalso (Length >= 2) andalso (Length =< Dist) of
			true ->
				[_|Path] = lists:droplast(Line),
				wor_zone:is_line_transparent(SZoneEnt, Path);
			false ->
				false
		end
	end).

%% ------------------------------------
%% find_target
%% ------------------------------------

-spec find_target(integer()) -> wor_btree:bt_node().
find_target(Radius) ->
	wor_btree:bt_act(fun(Entity, Blackboard) ->
		[
			#wor_relation_comp{group = Group, relations = Relations},
			#wor_placeable_comp{pos = SPos = {Sx,Sy}, zone = ZoneEnt}
		] = wor_entmgr:get_entity_components(Entity, [relation, placeable]),
		Targets = wor_entmgr:select_components(
			fun(Target, [TPlaceable, TRelation]) ->
				#wor_relation_comp{
					group     = TGroup,
					relations = TRelations
				} = TRelation,
				#wor_placeable_comp{
					pos = TPos = {Tx,Ty}
				} = TPlaceable,
				Target /= Entity andalso
				abs(Sx - Tx) =< Radius andalso
				abs(Sy - Ty) =< Radius andalso
				case {dict:find(TGroup, Relations), dict:find(Group, TRelations)} of
					{{ok, enemy},_} -> {value, TPos};
					{_,{ok, enemy}} -> {value, TPos};
					_ -> false
				end
			end, [placeable, relation], {zone_index, wor_utils:zone_index(ZoneEnt)}),
		LosTargets = dict:filter(fun(_Target, TPos) ->
			Line = wor_utils:generate_line(SPos, TPos),
			Length = length(Line),
			case (Length >= 2) andalso (Length =< Radius) of
				true ->
					[_|Path] = lists:droplast(Line),
					wor_zone:is_line_transparent(ZoneEnt, Path);
				false ->
					false
			end
		end, Targets),
		case dict:to_list(LosTargets) of
			[] ->
				{failure, Blackboard};
			[{Target,_}|_] ->
				NewBlackboard = dict:store(target, Target, Blackboard),
				{success, NewBlackboard}
		end
	end).

%% ------------------------------------
%% step_to_target
%% ------------------------------------

-spec step_to_target() -> wor_btree:bt_node().
step_to_target() ->
	wor_btree:bt_act(fun(Entity, Blackboard) ->
		Target = dict:fetch(target, Blackboard),
		#wor_placeable_comp{
			pos = SPos
		} = wor_entmgr:get_component(Entity, placeable),
		#wor_placeable_comp{
			pos = TPos
		} = wor_entmgr:get_component(Target, placeable),
		Line = wor_utils:generate_line(SPos, TPos),
		case Line of
			[SPos|[NPos|[_|_]]] ->  % 3 or more
				{
					success,
					wor_action:new(step, [
						{delta, wor_vec2:sub(NPos,SPos)}
					]),
					Blackboard
				};
			[SPos,TPos] ->
				{failure, Blackboard}
		end
	end).

%% ------------------------------------
%% attack_target
%% ------------------------------------

-spec attack_target() -> wor_btree:bt_node().
attack_target() ->
	wor_btree:bt_act(fun(Entity, Blackboard) ->
		Target = dict:fetch(target, Blackboard),
		#wor_placeable_comp{
			pos = SPos
		} = wor_entmgr:get_component(Entity, placeable),
		#wor_placeable_comp{
			pos = TPos
		} = wor_entmgr:get_component(Target, placeable),
		case wor_utils:generate_line(SPos, TPos) of
			[SPos,TPos] ->
				{
					success,
					wor_action:new(step, [
						{delta, wor_vec2:sub(TPos,SPos)}
					]),
					Blackboard
				}
		end
	end).
