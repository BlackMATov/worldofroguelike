-module(wor_usable).

%% public
-export([tick/1]).
-export([do_use/2]).
-export([do_use_item/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_use
%% ------------------------------------

-spec do_use(
	wor_entity(), wor_vec2()
) -> ok.

do_use(Entity, Delta) ->
	#wor_placeable_comp{pos = Pos, zone = ZoneEnt} =
		wor_entmgr:get_component(Entity, placeable),
	NewPos = wor_vec2:add(Pos, Delta),
	Usables = wor_entmgr:select_components(
		fun(_UsableEnt, [
			#wor_placeable_comp{zone = UsableZoneEnt},
			#wor_usable_comp{comp = UsableComp}
		]) ->
			case UsableZoneEnt of
				ZoneEnt -> {value, UsableComp};
				_ -> false
			end
		end, [placeable, usable], {tile_index, wor_utils:tile_index(NewPos)}),
	dict:fold(fun(UsableEnt, UsableComp, AccIn) ->
		case can_do_use(Entity, UsableEnt) of
			true ->
				AccIn = do_concrete_use(UsableComp, Entity, UsableEnt);
			false ->
				AccIn
		end
	end, ok, Usables).

%% ------------------------------------
%% do_use_item
%% ------------------------------------

-spec do_use_item(
	wor_entity(), wor_entity()
) -> ok.

do_use_item(Entity, ItemEnt) ->
	case can_do_use_item(Entity, ItemEnt) of
		true ->
			#wor_usable_comp{comp = UsableComp} =
				wor_entmgr:get_component(ItemEnt, usable),
			do_concrete_use(UsableComp, Entity, ItemEnt),
			wor_requester:send_inventory_info(Entity);
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_use
%% ------------------------------------

-spec can_do_use(
	wor_entity(), wor_entity()
) -> boolean().

can_do_use(Who, Whom) ->
	wor_entmgr:has_component(Who,  usabler) andalso
	wor_entmgr:has_component(Whom, usable).

%% ------------------------------------
%% can_do_use_item
%% ------------------------------------

-spec can_do_use_item(
	wor_entity(), wor_entity()
) -> boolean().

can_do_use_item(Who, Whom) ->
	wor_entmgr:has_component(Who,  usabler) andalso
	wor_entmgr:has_component(Whom, usable).

%% ------------------------------------
%% do_concrete_use
%% ------------------------------------

-spec do_concrete_use(
	wor_comp_type(), wor_entity(), wor_entity()
) -> ok.

do_concrete_use(door, _Who, Whom) ->
	#wor_door_comp{opened = Opened} =
		wor_entmgr:get_component(Whom, door),
	case Opened of
		true  -> wor_door:do_close(Whom);
		false -> wor_door:do_open(Whom)
	end;

do_concrete_use(food, Who, Whom) ->
	wor_food:do_eat(Who, Whom);

do_concrete_use(equipment, Who, Whom) ->
	case wor_equip_slots:is_equipped(Who, Whom) of
		true  -> wor_equip_slots:do_unequip(Who, Whom);
		false -> wor_equip_slots:do_equip(Who, Whom)
	end.
