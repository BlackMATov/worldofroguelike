-module(wor_stepable).

%% public
-export([tick/1]).
-export([do_step/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_step
%% ------------------------------------

-spec do_step(
	wor_entity()
) -> ok.

do_step(Entity) ->
	#wor_placeable_comp{
		pos  = Pos,
		zone = ZoneEnt
	} = wor_entmgr:get_component(Entity, placeable),
	Stepables = wor_entmgr:select_components(
		fun(_StepableEnt, [
			#wor_placeable_comp{zone = StepableZoneEnt},
			#wor_stepable_comp{comp = StepableComp}
		]) ->
			case StepableZoneEnt of
				ZoneEnt -> {value, StepableComp};
				_ -> false
			end
		end, [placeable, stepable], {tile_index, wor_utils:tile_index(Pos)}),
	dict:fold(fun(StepableEnt, StepableComp, AccIn) ->
		AccIn = do_concrete_step(StepableComp, StepableEnt, Entity)
	end, ok, Stepables).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

-spec do_concrete_step(
	wor_comp_type(), wor_entity(), wor_entity()
) -> ok.

do_concrete_step(portal, Entity, Stepper) ->
	wor_portal:do_teleport(Entity, Stepper);

do_concrete_step(respawn, Entity, Stepper) ->
	wor_respawn:do_resurrect(Entity, Stepper).
