-module(wor_es).

%% public
-export([new/0]).
-export([dump/1]).

%% public
-export([create_entity/1]).
-export([create_entity/2]).
-export([remove_entity/2]).

%% public
-export([create_index/4]).
-export([has_any_index/2]).

%% public
-export([create_component/4]).
-export([create_components/3]).
-export([remove_component/3]).
-export([remove_components/2]).
-export([remove_components/3]).

%% public
-export([update_component/3]).
-export([update_component/4]).
-export([supdate_component/4]).
-export([rupdate_component/4]).
-export([update_components/3]).
-export([select_components/3]).
-export([select_components/4]).

%% public
-export([has_component/3]).
-export([has_components/3]).
-export([get_component/2]).
-export([get_component/3]).
-export([find_component/3]).

%% public
-export([get_versions/2]).
-export([get_components/2]).
-export([get_components/3]).

%% public
-export([has_entity/2]).
-export([has_entities/2]).
-export([get_entities/1]).
-export([get_entity_version/2]).
-export([get_entity_components/2]).
-export([get_entity_components/3]).

-type entity() ::
	integer().

-type version() ::
	integer().

-type comp_type() ::
	atom().

-type comp_state() ::
	term().

-type index_name() ::
	atom().

-type index_func() ::
	fun((comp_state()) -> integer()).

-type index() ::
	{index_name(), integer()}.

-type update_func() ::
	fun((comp_state()) -> comp_state()).

-type rupdate_func() ::
	fun((comp_state()) -> {comp_state(),term()}).

-type select_func() ::
	fun((entity(), list(comp_state())) -> boolean() | {value, term()}).

-type indexes() ::
	dict:dict(
		comp_type(),
		dict:dict(
			index_name(), {
				index_func(),
				dict:dict(
					integer(),
					dict:dict(entity(), comp_state()))
			}
		)
	).

-type entities() ::
	dict:dict(
		entity(),
		version()).

-type versions() ::
	dict:dict(
		comp_type(),
		dict:dict(
			entity(),
			version()
		)
	).

-type components() ::
	dict:dict(
		comp_type(),
		dict:dict(
			entity(),
			comp_state()
		)
	).

-record(state, {
	last_id    = 0          :: entity(),
	indexes    = dict:new() :: indexes(),
	entities   = dict:new() :: entities(),
	versions   = dict:new() :: versions(),
	components = dict:new() :: components()
}).
-type state() :: #state{}.

-export_type([state/0]).
-export_type([entity/0, version/0]).
-export_type([comp_type/0, comp_state/0]).
-export_type([index_name/0, index_func/0, index/0]).
-export_type([update_func/0, rupdate_func/0, select_func/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% new
%% ------------------------------------

-spec new(
) -> state().

new() ->
	#state{}.

%% ------------------------------------
%% dump
%% ------------------------------------

-spec dump(
	state()
) -> ok.

dump(
	#state{
		last_id    = LastId,
		entities   = Entities,
		indexes    = Indexes,
		versions   = Versions,
		components = Components
	}
) ->
	io:format("-= =============== =-~n"),
	io:format("-=== LAST_ID ===-~n"),
	io:format("-= =============== =-~n"),
	io:format("~p~n", [LastId]),
	io:format("-= =============== =-~n"),
	io:format("-=== ENTITIES ===-~n"),
	io:format("-= =============== =-~n"),
	dict:map(fun(Entity, Version) ->
		io:format("id:~p | ver:~p~n", [Entity, Version])
	end, Entities),
	io:format("-= =============== =-~n"),
	io:format("-=== COMPONENTS ===-~n"),
	io:format("-= =============== =-~n"),
	dict:map(fun(CompType, Comps) ->
		io:format("--- ~p ---~n", [CompType]),
		CompVersions = dict:fetch(CompType, Versions),
		dict:map(fun(Entity, Comp) ->
			io:format("  ent:~p | data:~p | ver:~p~n",
				[Entity, Comp, dict:fetch(Entity, CompVersions)])
		end, Comps)
	end, Components),
	io:format("-= =============== =-~n"),
	io:format("-=== INDEXES ===-~n"),
	io:format("-= =============== =-~n"),
	dict:map(fun(CompType, CompIndexes) ->
		io:format("--- ~p ---~n", [CompType]),
		dict:map(fun(IndexName, {_Func,InternalIndexes}) ->
			io:format("  ~p:~n", [IndexName]),
			dict:map(fun(IndexValue, IndexEntities) ->
				io:format("    ~p: ~p~n",
					[IndexValue, dict:to_list(IndexEntities)])
			end, InternalIndexes)
		end, CompIndexes)
	end, Indexes),
	ok.

%% ------------------------------------
%% create_entity
%% ------------------------------------

-spec create_entity(
	state()
) -> {entity(), state()}.

create_entity(
	State = #state{
		last_id  = LastId,
		entities = Entities
	}
) ->
	Entity = LastId + 1,
	{Entity, State#state{
		last_id  = Entity,
		entities = dict:store(Entity, 1, Entities)
	}}.

-spec create_entity(
	list({comp_type(), comp_state()}), state()
) -> {entity(), state()}.

create_entity(Comps, State) ->
	{Entity, NewState} = create_entity(State),
	{Entity, create_components(Entity, Comps, NewState)}.

%% ------------------------------------
%% remove_entity
%% ------------------------------------

-spec remove_entity(
	entity(), state()
) -> state().

remove_entity(Entity, State) ->
	NewState = remove_components(Entity, State),
	remove_entity_id(Entity, NewState).

%% ------------------------------------
%% create_index
%% ------------------------------------

-spec create_index(
	comp_type(), index_name(), index_func(), state()
) -> state().

create_index(
	CompType, IndexName, IndexFunc,
	State = #state{
		indexes  = Indexes,
		entities = Entities
	}
) ->
	true = dict:is_empty(Entities),
	CompIndexes = case dict:find(CompType, Indexes) of
		{ok, FindCompIndexes} -> FindCompIndexes;
		_ -> dict:new()
	end,
	NewCompIndex   = {IndexFunc, dict:new()},
	NewCompIndexes = dict:store(IndexName, NewCompIndex, CompIndexes),
	NewIndexes     = dict:store(CompType, NewCompIndexes, Indexes),
	State#state{indexes = NewIndexes}.

%% ------------------------------------
%% has_any_index
%% ------------------------------------

-spec has_any_index(
	comp_type(), state()
) -> boolean().

has_any_index(
	CompType,
	#state{indexes = Indexes}
) ->
	case dict:find(CompType, Indexes) of
		{ok, CompIndexes} -> not dict:is_empty(CompIndexes);
		_ -> false
	end.

%% ------------------------------------
%% create_component
%% ------------------------------------

-spec create_component(
	entity(), comp_type(), comp_state(), state()
) -> state().

create_component(
	Entity, CompType, Comp,
	State = #state{
		entities   = Entities,
		versions   = Versions,
		components = Components
	}
) ->
	InternalVers = case dict:find(CompType, Versions) of
		{ok, FindVersions} -> FindVersions;
		_ -> dict:new()
	end,
	InternalComps = case dict:find(CompType, Components) of
		{ok, FindComps} -> FindComps;
		_ -> dict:new()
	end,
	NewInternalVers  = dict:store(Entity, 1, InternalVers),
	NewInternalComps = dict:store(Entity, Comp, InternalComps),
	safe_create_component_index(Entity, CompType, Comp, State#state{
		entities   = dict:update_counter(Entity, 1, Entities),
		versions   = dict:store(CompType, NewInternalVers, Versions),
		components = dict:store(CompType, NewInternalComps, Components)
	}).

%% ------------------------------------
%% create_components
%% ------------------------------------

-spec create_components(
	entity(), list({comp_type(), comp_state()}), state()
) -> state().

create_components(Entity, Comps, State) ->
	lists:foldl(fun({CompType, Comp}, AccIn) ->
		create_component(Entity, CompType, Comp, AccIn)
	end, State, Comps).

%% ------------------------------------
%% remove_component
%% ------------------------------------

-spec remove_component(
	entity(), comp_type(), state()
) -> state().

remove_component(
	Entity, CompType,
	State = #state{
		entities   = Entities,
		versions   = Versions,
		components = Components
	}
) ->
	InternalVers     = dict:fetch(CompType, Versions),
	NewInternalVers  = dict:erase(Entity, InternalVers),
	InternalComps    = dict:fetch(CompType, Components),
	NewInternalComps = dict:erase(Entity, InternalComps),
	safe_remove_component_index(Entity, CompType, State, State#state{
		entities   = dict:update_counter(Entity, 1, Entities),
		versions   = dict:store(CompType, NewInternalVers, Versions),
		components = dict:store(CompType, NewInternalComps, Components)
	}).

%% ------------------------------------
%% remove_components
%% ------------------------------------

-spec remove_components(
	entity(), state()
) -> state().

remove_components(Entity, State) ->
	dict:fold(fun(CompType, _Comp, AccIn) ->
		remove_component(Entity, CompType, AccIn)
	end, State, get_entity_components(Entity, State)).

%% ------------------------------------
%% remove_components
%% ------------------------------------

-spec remove_components(
	entity(), list(comp_type()), state()
) -> state().

remove_components(Entity, Comps, State) ->
	lists:foldl(fun(CompType, AccIn) ->
		remove_component(Entity, CompType, AccIn)
	end, State, Comps).

%% ------------------------------------
%% update_component
%% ------------------------------------

-spec update_component(
	update_func() | comp_state(), comp_type(), state()
) -> state().

update_component(FuncOrComp, CompType, State) ->
	{Entity, _Comp} = get_component(CompType, State),
	update_component(Entity, FuncOrComp, CompType, State).

-spec update_component(
	entity(), update_func() | comp_state(), comp_type(), state()
) -> state().

update_component(
	Entity, Func, CompType, State
) when is_function(Func) ->
	NewComp = Func(get_component(Entity, CompType, State)),
	update_component(Entity, NewComp, CompType, State);

update_component(
	Entity, NewComp, CompType,
	State = #state{
		entities   = Entities,
		versions   = Versions,
		components = Components
	}
) ->
	InternalVers     = dict:fetch(CompType, Versions),
	NewInternalVers  = dict:update_counter(Entity, 1, InternalVers),
	InternalComps    = dict:fetch(CompType, Components),
	NewInternalComps = dict:store(Entity, NewComp, InternalComps),
	safe_update_component_index(Entity, CompType, NewComp, State, State#state{
		entities   = dict:update_counter(Entity, 1, Entities),
		versions   = dict:store(CompType, NewInternalVers, Versions),
		components = dict:store(CompType, NewInternalComps, Components)
	}).

%% ------------------------------------
%% supdate_component
%% ------------------------------------

-spec supdate_component(
	entity(), update_func() | comp_state(), comp_type(), state()
) -> state().

supdate_component(Entity, FuncOrComp, CompType, State) ->
	case has_component(Entity, CompType, State) of
		true ->
			update_component(Entity, FuncOrComp, CompType, State);
		false ->
			State
	end.

%% ------------------------------------
%% rupdate_component
%% ------------------------------------

-spec rupdate_component(
	entity(), rupdate_func(), comp_type(), state()
) -> {term(),state()}.

rupdate_component(Entity, Func, CompType, State) ->
	{NewComp, Ret} = Func(get_component(Entity, CompType, State)),
	{Ret, update_component(Entity, NewComp, CompType, State)}.

%% ------------------------------------
%% update_components
%% ------------------------------------

-spec update_components(
	entity(), list({comp_type(), update_func() | comp_state()}), state()
) -> state().

update_components(_Entity, [], State) ->
	State;
update_components(Entity, [{CompType,FuncOrComp}|Tail], State) ->
	NewState = update_component(Entity, FuncOrComp, CompType, State),
	update_components(Entity, Tail, NewState).

%% ------------------------------------
%% select_components
%% ------------------------------------

-spec select_components(
	select_func(), list(comp_type()), state()
) -> dict:dict(entity(), term()).

select_components(Func, [MainCompType|OtherCompTypes], State) ->
	MainComps = get_components(MainCompType, State),
	common_select_components(Func, MainComps, OtherCompTypes, State).

-spec select_components(
	select_func(), list(comp_type()), index(), state()
) -> dict:dict(entity(), term()).

select_components(Func, [MainCompType|OtherCompTypes], Index, State) ->
	MainComps = get_components(MainCompType, Index, State),
	common_select_components(Func, MainComps, OtherCompTypes, State).

%% ------------------------------------
%% has_component
%% ------------------------------------

-spec has_component(
	entity(), comp_type(), state()
) -> boolean().

has_component(
	Entity, CompType,
	#state{components = Components}
) ->
	case dict:find(CompType, Components) of
		{ok, InternalComps} ->
			case dict:find(Entity, InternalComps) of
				{ok, _Comp} -> true;
				_ -> false
			end;
		_ -> false
	end.

%% ------------------------------------
%% has_components
%% ------------------------------------

-spec has_components(
	entity(), list(comp_type()), state()
) -> boolean().

has_components(_Entity, [], _State) ->
	true;
has_components(Entity, [CompType|Tail], State) ->
	case has_component(Entity, CompType, State) of
		true  -> has_components(Entity, Tail, State);
		false -> false
	end.

%% ------------------------------------
%% get_component
%% ------------------------------------

-spec get_component(
	comp_type(), state()
) -> {entity(),comp_state()}.

get_component(
	CompType,
	#state{components = Components}
) ->
	InternalComps = dict:fetch(CompType, Components),
	[First|_Tail] = dict:to_list(InternalComps),
	First.

-spec get_component(
	entity(), comp_type(), state()
) -> comp_state().

get_component(
	Entity, CompType,
	#state{components = Components}
) ->
	InternalComps = dict:fetch(CompType, Components),
	dict:fetch(Entity, InternalComps).

%% ------------------------------------
%% find_component
%% ------------------------------------

-spec find_component(
	entity(), comp_type(), state()
) -> {ok, comp_state()} | {error, not_found}.

find_component(
	Entity, CompType,
	#state{components = Components}
) ->
	case dict:find(CompType, Components) of
		{ok, InternalComps} ->
			case dict:find(Entity, InternalComps) of
				{ok, Comp} -> {ok, Comp};
				_ -> {error, not_found}
			end;
		_ -> {error, not_found}
	end.

%% ------------------------------------
%% get_versions
%% ------------------------------------

-spec get_versions(
	comp_type(), state()
) -> dict:dict(entity(), version()).

get_versions(
	CompType,
	#state{versions = Versions}
) ->
	case dict:find(CompType, Versions) of
		{ok, InternalVers} -> InternalVers;
		_ -> dict:new()
	end.

%% ------------------------------------
%% get_components
%% ------------------------------------

-spec get_components(
	comp_type() | list(comp_type()), state()
) -> dict:dict(entity(), comp_state()).

get_components(
	CompType,
	#state{components = Components}
) when is_atom(CompType) ->
	case dict:find(CompType, Components) of
		{ok, InternalComps} -> InternalComps;
		_ -> dict:new()
	end;

get_components(
	Comps, State
) when is_list(Comps) ->
	select_components(fun(_Entity, _ListComps) ->
		true
	end, Comps, State).

-spec get_components(
	comp_type() | list(comp_type()), index(), state()
) -> dict:dict(entity(), comp_state()).

get_components(
	CompType, {IndexName,IndexValue},
	#state{indexes = Indexes}
) when is_atom(CompType) ->
	case dict:find(CompType, Indexes) of
		{ok, CompIndexes} ->
			case dict:find(IndexName, CompIndexes) of
				{ok, {_IndexFunc, InternalIndexes}} ->
					case dict:find(IndexValue, InternalIndexes) of
						{ok, Comps} -> Comps;
						_ -> dict:new()
					end;
				_ -> dict:new()
			end;
		_ -> dict:new()
	end;

get_components(
	Comps, Index, State
) when is_list(Comps) ->
	select_components(fun(_Entity, _ListComps) ->
		true
	end, Comps, Index, State).

%% ------------------------------------
%% has_entity
%% ------------------------------------

-spec has_entity(
	entity(), state()
) -> boolean().

has_entity(
	Entity,
	#state{entities = Entities}
) ->
	dict:is_key(Entity, Entities).

%% ------------------------------------
%% has_entities
%% ------------------------------------

-spec has_entities(
	list(entity()), state()
) -> boolean().

has_entities([], _State) ->
	true;
has_entities([Entity|Tail], State) ->
	case has_entity(Entity, State) of
		true  -> has_entities(Tail, State);
		false -> false
	end.

%% ------------------------------------
%% get_entities
%% ------------------------------------

-spec get_entities(
	state()
) -> dict:dict(entity(), version()).

get_entities(
	#state{entities = Entities}
) ->
	Entities.

%% ------------------------------------
%% get_entity_version
%% ------------------------------------

-spec get_entity_version(
	entity(), state()
) -> version().

get_entity_version(
	Entity,
	#state{entities = Entities}
) ->
	dict:fetch(Entity, Entities).

%% ------------------------------------
%% get_entity_components
%% ------------------------------------

-spec get_entity_components(
	entity(), state()
) -> dict:dict(comp_type(), comp_state()).

get_entity_components(
	Entity,
	#state{components = Components}
) ->
	dict:fold(fun(CompType, InternalComps, AccIn) ->
		case dict:find(Entity, InternalComps) of
			{ok, Comp} -> dict:store(CompType, Comp, AccIn);
			_ -> AccIn
		end
	end, dict:new(), Components).

-spec get_entity_components(
	entity(), list(comp_type()), state()
) -> list(comp_state()).

get_entity_components(Entity, Comps, State) ->
	RevAllComps = lists:foldl(fun(CompType, AccIn) ->
		Comp = get_component(Entity, CompType, State),
		[Comp | AccIn]
	end, [], Comps),
	lists:reverse(RevAllComps).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% remove_entity_id
%% ------------------------------------

-spec remove_entity_id(
	entity(), state()
) -> state().

remove_entity_id(
	Entity,
	State = #state{entities = Entities}
) ->
	NewEntities = dict:erase(Entity, Entities),
	State#state{entities = NewEntities}.

%% ------------------------------------
%% common_select_components
%% ------------------------------------

-spec common_select_components(
	select_func(), dict:dict(entity(), comp_state()), list(comp_type()), state()
) -> dict:dict(entity(), term()).

common_select_components(Func, MainComps, OtherCompTypes, State) ->
	dict:fold(fun(Entity, MainComp, AccIn) ->
		{RevAllComps,Check} = lists:foldl(fun(CompType, {OtherAccIn,CheckAccIn}) ->
			case CheckAccIn of
				true ->
					case find_component(Entity, CompType, State) of
						{ok, OtherComp} ->
							{[OtherComp | OtherAccIn], CheckAccIn};
						_ -> {OtherAccIn, false}
					end;
				false ->
					{OtherAccIn, CheckAccIn}
			end
		end, {[MainComp],true}, OtherCompTypes),
		AllComps = lists:reverse(RevAllComps),
		case Check andalso Func(Entity, AllComps) of
			{value, Value} -> dict:store(Entity, Value, AccIn);
			true           -> dict:store(Entity, AllComps, AccIn);
			false          -> AccIn
		end
	end, dict:new(), MainComps).

%% ------------------------------------
%% create_component_index
%% ------------------------------------

-spec create_component_index(
	entity(), comp_type(), comp_state(), state()
) -> state().

create_component_index(
	Entity, CompType, Comp,
	State = #state{indexes = Indexes}
) ->
	CompIndexes = case dict:find(CompType, Indexes) of
		{ok, FindCompIndexes} -> FindCompIndexes;
		_ -> dict:new()
	end,
	NewCompIndexes = dict:fold(fun(IndexName, {IndexFunc, InternalIndexes}, AccIn) ->
		IndexValue = IndexFunc(Comp),
		Entities = case dict:find(IndexValue, InternalIndexes) of
			{ok, FindEntities} -> FindEntities;
			_ -> dict:new()
		end,
		NewEntities = dict:store(Entity, Comp, Entities),
		NewInternalIndexes = dict:store(IndexValue, NewEntities, InternalIndexes),
		dict:store(IndexName, {IndexFunc,NewInternalIndexes}, AccIn)
	end, CompIndexes, CompIndexes),
	NewIndexes = dict:store(CompType, NewCompIndexes, Indexes),
	State#state{indexes = NewIndexes}.

-spec safe_create_component_index(
	entity(), comp_type(), comp_state(), state()
) -> state().

safe_create_component_index(Entity, CompType, Comp, State) ->
	case has_any_index(CompType, State) of
		true ->
			create_component_index(Entity, CompType, Comp, State);
		false ->
			State
	end.

%% ------------------------------------
%% remove_component_index
%% ------------------------------------

-spec remove_component_index(
	entity(), comp_type(), state(), state()
) -> state().

remove_component_index(
	Entity, CompType,
	OldState = #state{indexes = Indexes},
	NewState
) ->
	CompIndexes = case dict:find(CompType, Indexes) of
		{ok, FindCompIndexes} -> FindCompIndexes;
		_ -> dict:new()
	end,
	Comp = get_component(Entity, CompType, OldState),
	NewCompIndexes = dict:fold(fun(IndexName, {IndexFunc, InternalIndexes}, AccIn) ->
		IndexValue = IndexFunc(Comp),
		Entities = dict:fetch(IndexValue, InternalIndexes),
		NewEntities = dict:erase(Entity, Entities),
		NewInternalIndexes = case dict:size(NewEntities) of
			0 -> dict:erase(IndexValue, InternalIndexes);
			_ -> dict:store(IndexValue, NewEntities, InternalIndexes)
		end,
		dict:store(IndexName, {IndexFunc,NewInternalIndexes}, AccIn)
	end, CompIndexes, CompIndexes),
	NewIndexes = dict:store(CompType, NewCompIndexes, Indexes),
	NewState#state{indexes = NewIndexes}.

-spec safe_remove_component_index(
	entity(), comp_type(), state(), state()
) -> state().

safe_remove_component_index(Entity, CompType, OldState, NewState) ->
	case has_any_index(CompType, NewState) of
		true ->
			remove_component_index(Entity, CompType, OldState, NewState);
		false ->
			NewState
	end.

%% ------------------------------------
%% update_component_index
%% ------------------------------------

-spec update_component_index(
	entity(), comp_type(), comp_state(), state(), state()
) -> state().

update_component_index(
	Entity, CompType, NewComp,
	OldState = #state{indexes = OldIndexes},
	NewState
) ->
	CompIndexes = case dict:find(CompType, OldIndexes) of
		{ok, FindCompIndexes} -> FindCompIndexes;
		_ -> dict:new()
	end,
	OldComp = get_component(Entity, CompType, OldState),
	NewCompIndexes = dict:fold(fun(IndexName, {IndexFunc, InternalIndexes}, AccIn) ->
		OldIndexValue = IndexFunc(OldComp),
		NewIndexValue = IndexFunc(NewComp),
		InternalIndexes2 = case OldIndexValue /= NewIndexValue of
			true ->
				Entities = dict:fetch(OldIndexValue, InternalIndexes),
				NewEntities = dict:erase(Entity, Entities),
				case dict:size(NewEntities) of
					0 -> dict:erase(OldIndexValue, InternalIndexes);
					_ -> dict:store(OldIndexValue, NewEntities, InternalIndexes)
				end;
			false ->
				InternalIndexes
		end,
		Entities2 = case dict:find(NewIndexValue, InternalIndexes2) of
			{ok, FindEntities} -> FindEntities;
			_ -> dict:new()
		end,
		NewEntities2 = dict:store(Entity, NewComp, Entities2),
		NewInternalIndexes = dict:store(NewIndexValue, NewEntities2, InternalIndexes2),
		dict:store(IndexName, {IndexFunc,NewInternalIndexes}, AccIn)
	end, CompIndexes, CompIndexes),
	NewIndexes = dict:store(CompType, NewCompIndexes, OldIndexes),
	NewState#state{indexes = NewIndexes}.

safe_update_component_index(Entity, CompType, NewComp, OldState, NewState) ->
	case has_any_index(CompType, NewState) of
		true ->
			update_component_index(Entity, CompType, NewComp, OldState, NewState);
		false ->
			NewState
	end.
