-module(wor_experience).

%% public
-export([tick/1]).
-export([do_add_experience/2]).
-export([do_add_kill_experience/2]).
-export([do_recalculate/1]).
-export([level_to_experience/1]).
-export([experience_to_level/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_add_experience
%% ------------------------------------

-spec do_add_experience(
	wor_entity(), integer()
) -> ok.

do_add_experience(Entity, AddExp) ->
	case wor_entmgr:has_component(Entity, experience) of
		true ->
			ok = wor_entmgr:update_component(Entity, fun(Experience) ->
				#wor_experience_comp{experience = Exp} = Experience,
				Experience#wor_experience_comp{experience = Exp + AddExp}
			end, experience),
			ok = wor_logger:do_log_exp(Entity, AddExp),
			ok = do_recalculate(Entity);
		false -> ok
	end.

%% ------------------------------------
%% do_add_kill_experience
%% ------------------------------------

-spec do_add_kill_experience(
	wor_entity(), wor_entity()
) -> ok.

do_add_kill_experience(Who, Whom) ->
	FindExpWho  = wor_entmgr:find_component(Who,  experience),
	FindExpWhom = wor_entmgr:find_component(Whom, experience),
	case {FindExpWho, FindExpWhom} of
		{{ok, WhoExp}, {ok, WhomExp}} ->
			#wor_experience_comp{level = WhoLev } = WhoExp,
			#wor_experience_comp{level = WhomLev} = WhomExp,
			Experience = kill_experience(WhoLev, WhomLev),
			do_add_experience(Who, Experience);
		_ ->
			ok
	end.

%% ------------------------------------
%% do_recalculate
%% ------------------------------------

-spec do_recalculate(
	wor_entity()
) -> ok.

do_recalculate(Entity) ->
	case wor_entmgr:has_component(Entity, experience) of
		true ->
			ok = check_level_up(Entity),
			ok = wor_requester:send_stats_info(Entity);
		false -> ok
	end.

%% ------------------------------------
%% level_to_experience
%% ------------------------------------

-spec level_to_experience(
	integer()
) -> integer().

level_to_experience(Level) ->
	Level * Level * 100.

%% ------------------------------------
%% experience_to_level
%% ------------------------------------

-spec experience_to_level(
	integer()
) -> integer().

experience_to_level(Experience) ->
	trunc(math:sqrt(Experience/100)).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% kill_experience
%% ------------------------------------

-spec kill_experience(
	integer(), integer()
) -> integer().

kill_experience(KillerLev, EnemyLev) ->
	Exp = (EnemyLev + 1) * 10,
	AddExp = (Exp * (EnemyLev - KillerLev)) div 10,
	max(Exp + AddExp, 0).

%% ------------------------------------
%% check_level_up
%% ------------------------------------

-spec check_level_up(
	wor_entity()
) -> ok.

check_level_up(Entity) ->
	#wor_experience_comp{
		level      = Level,
		experience = Experience
	} = wor_entmgr:get_component(Entity, experience),
	case experience_to_level(Experience) > Level of
		true ->
			ok = do_level_up(Entity),
			ok = check_level_up(Entity);
		false ->
			ok
	end.

%% ------------------------------------
%% do_level_up
%% ------------------------------------

-spec do_level_up(
	wor_entity()
) -> ok.

do_level_up(Entity) ->
	CurLev = wor_entmgr:rupdate_component(Entity, fun(Exp) ->
		#wor_experience_comp{level = Lev} = Exp,
		NewLev = Lev + 1,
		NewExp = Exp#wor_experience_comp{level = NewLev},
		{NewExp, NewLev}
	end, experience),
	ok = wor_logger:do_log_level_up(Entity, CurLev),
	ok = wor_health:do_restore(Entity).
