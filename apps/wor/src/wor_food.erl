-module(wor_food).

%% public
-export([tick/1]).
-export([do_eat/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_eat
%% ------------------------------------

-spec do_eat(
	wor_entity(), wor_entity()
) -> ok.

do_eat(Who, Whom) ->
	case can_do_eat(Who, Whom) of
		true ->
			#wor_food_comp{hp = Hp} =
				wor_entmgr:get_component(Whom, food),
			ok = wor_health:do_add_hp(Who, Hp),
			ok = wor_inventory:do_drop(Who, Whom, true),
			ok = wor_dead:do_dead(Whom);
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_eat
%% ------------------------------------

-spec can_do_eat(
	wor_entity(), wor_entity()
) -> boolean().

can_do_eat(Who, Whom) ->
	wor_entmgr:has_component(Who,  inventory) andalso
	wor_entmgr:has_component(Who,  health)    andalso
	wor_entmgr:has_component(Whom, food)      andalso
	wor_inventory:has_item(Who, Whom).
