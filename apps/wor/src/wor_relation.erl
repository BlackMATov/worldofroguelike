-module(wor_relation).

%% public
-export([tick/1]).
-export([is_enemies/2]).
-export([is_friends/2]).

-include("types.hrl").

-type group_type() ::
	orc    |
	rat    |
	human  |
	hobbit |
	zombie |
	player.

-type relation_type() ::
	enemy |
	friend.

-export_type([group_type/0, relation_type/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% is_enemies
%% ------------------------------------

-spec is_enemies(
	wor_entity(), wor_entity()
) -> boolean().

is_enemies(EntityA, EntityB) ->
	check_relation(EntityA, EntityB, enemy).

%% ------------------------------------
%% is_friends
%% ------------------------------------

-spec is_friends(
	wor_entity(), wor_entity()
) -> boolean().

is_friends(EntityA, EntityB) ->
	check_relation(EntityA, EntityB, friend).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

-spec check_relation(
	wor_entity(), wor_entity(), relation_type()
) -> boolean().

check_relation(EntityA, EntityB, Relation) ->
	#wor_relation_comp{
		group     = GroupA,
		relations = RelationsA
	} = wor_entmgr:get_component(EntityA, relation),
	#wor_relation_comp{
		group     = GroupB,
		relations = RelationsB
	} = wor_entmgr:get_component(EntityB, relation),
	RA = dict:find(GroupA, RelationsB),
	RB = dict:find(GroupB, RelationsA),
	case {RA,RB} of
		{{ok, Relation},_} -> true;
		{_,{ok, Relation}} -> true;
		_ -> false
	end.
