-module(wor_pushable).

%% public
-export([tick/1]).
-export([do_push/2]).

-include("types.hrl").
-define(PUSH_COOLDOWN, 100).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_push
%% ------------------------------------

-spec do_push(
	wor_entity(), wor_vec2()
) -> ok.

do_push(Entity, Delta) ->
	#wor_placeable_comp{pos = Pos, zone = Zone} =
		wor_entmgr:get_component(Entity, placeable),
	NewPos = wor_vec2:add(Pos, Delta),
	case wor_zone:find_tile_impassable_entity(Zone, NewPos) of
		{ok, ImpassableEntity} ->
			case can_do_push(Entity, ImpassableEntity) of
				true ->
					#wor_pushable_comp{comp = Comp} =
						wor_entmgr:get_component(ImpassableEntity, pushable),
					ok = do_concrete_push(Comp, Entity, ImpassableEntity),
					ok = wor_brain:do_cooldown(Entity, ?PUSH_COOLDOWN);
				false ->
					ok
			end;
		_ -> ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_push
%% ------------------------------------

-spec can_do_push(
	wor_entity(), wor_entity()
) -> boolean().

can_do_push(Who, Whom) ->
	wor_entmgr:has_component(Who,  pusher) andalso
	wor_entmgr:has_component(Whom, pushable).

%% ------------------------------------
%% do_concrete_push
%% ------------------------------------

-spec do_concrete_push(
	wor_comp_type(), wor_entity(), wor_entity()
) -> ok.

do_concrete_push(door, _Who, Whom) ->
	wor_door:do_open(Whom);

do_concrete_push(fighter, Who, Whom) ->
	wor_fighter:do_attack(Who, Whom);

do_concrete_push(window, _Who, Whom) ->
	wor_dead:do_dead(Whom).
