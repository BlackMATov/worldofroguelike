-module(wor_dead).

%% public
-export([tick/1]).
-export([do_dead/1]).
-export([is_dead/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	wor_entmgr:fold_components(fun(Entity, _Dead, AccIn) ->
		AccIn = wor_btree:do_shutdown(Entity),
		AccIn = wor_inventory:do_drop_all(Entity),
		AccIn = case wor_entmgr:find_component(Entity, userable) of
			{ok, Userable} ->
				[
					#wor_nameable_comp{name = Name},
					#wor_placeable_comp{pos = Pos, zone = ZoneEnt}
				] = wor_entmgr:get_entity_components(Entity, [nameable, placeable]),
				#wor_userable_comp{user = User} = Userable,
				ok = wor_logger:do_log_dead(Entity),
				UserEnt = wor_entfctr:create_entity(spirit, Pos, ZoneEnt, [
					{user,  User},
					{name,  Name},
					{ticks, WorldTicks}
				]),
				wor_requester:send_self_info(UserEnt);
			_ ->
				ok
		end,
		AccIn = wor_entmgr:remove_entity(Entity)
	end, ok, dead).

%% ------------------------------------
%% do_dead
%% ------------------------------------

-spec do_dead(
	wor_entity()
) -> ok.

do_dead(Entity) ->
	case wor_entmgr:has_component(Entity, dead) of
		true ->
			ok;
		false ->
			wor_entmgr:create_component(
				Entity, dead, wor_comps:new_dead())
	end.

%% ------------------------------------
%% is_dead
%% ------------------------------------

-spec is_dead(
	wor_entity()
) -> boolean().

is_dead(Entity) ->
	wor_entmgr:has_component(Entity, dead).
