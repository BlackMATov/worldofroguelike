-module(wor_world).
-behaviour(gen_server).

%% public
-export([start_link/0]).
-export([stop/0]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").
-define(TICK_INTERVAL, 50).
-define(SYSTEMS, [
	wor_zone,
	wor_btree,
	wor_brain,
	wor_door,
	wor_dead,
	wor_food,
	wor_health,
	wor_portal,
	wor_fighter,
	wor_relation,
	wor_mobfarm,
	wor_spawner,
	wor_stats,
	wor_respawn,
	wor_userable,
	wor_logger,
	wor_usable,
	wor_pushable,
	wor_stepable,
	wor_equip_slots,
	wor_experience,
	wor_inventory,
	wor_placeable,
	wor_trasher,
	wor_requester
]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, ok}.

init([]) ->
	io:format("~p:init(Self:~p)~n", [?MODULE, self()]),
	ok = create(),
	ok = load(),
	ok = tick(),
	{ok, ok}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	io:format("~p:stop(Self:~p)~n", [?MODULE, self()]),
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(tick, State) ->
	ok = tick(),
	{noreply, State};

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% create
%% ------------------------------------

-spec create(
) -> ok.

create() ->
	wor_entmgr:create_index(placeable, tile_index, fun(Placeable) ->
		#wor_placeable_comp{pos = Pos} = Placeable,
		wor_utils:tile_index(Pos)
	end),
	wor_entmgr:create_index(placeable, zone_index, fun(Placeable) ->
		#wor_placeable_comp{zone = Zone} = Placeable,
		wor_utils:zone_index(Zone)
	end),
	wor_entmgr:create_index(farmable, farm_index, fun(Farmable) ->
		#wor_farmable_comp{farm = Farm} = Farmable,
		wor_utils:farm_index(Farm)
	end),
	wor_entmgr:create_entity([
		{world,     wor_comps:new_world()},
		{spawner,   wor_comps:new_spawner()},
		{trasher,   wor_comps:new_trasher()},
		{requester, wor_comps:new_requester()}
	]),
	ok.

%% ------------------------------------
%% load
%% ------------------------------------

-spec load(
) -> ok.

load() ->
	Filename = wor_utils:world_filename(),
	{ok, FileBin} = file:read_file(Filename),
	{[
		{<<"start">>, StartName},
		{<<"zones">>, ZoneNames}
	]} = jiffy:decode(FileBin),
	lists:foreach(fun(ZoneName) ->
		wor_entmgr:create_entity([
			{zone, wor_comps:new_zone(
				ZoneName, ZoneName == StartName)}
		])
	end, ZoneNames).

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
) -> ok.

tick() ->
	{WorldEnt, _World} = wor_entmgr:get_component(world),
	{_AllTime, _Times} = timer:tc(fun() ->
		lists:foldl(fun(Module, InternalAccIn) ->
			{Time, _} = timer:tc(fun() ->
				apply(Module, tick, [WorldEnt])
			end),
			[{Module,Time} | InternalAccIn]
		end, [], ?SYSTEMS)
	end),
	%io:format("AllTime: ~p |", [AllTime div 1000]),
	%ok = lists:foreach(fun({Module,Time}) ->
		%io:format("~p: ~p | ", [
			%erlang:atom_to_list(Module),
			%Time div 1000
		%])
	%end, lists:filter(fun({_Module,Time}) -> Time > 1000 end, Times)),
	%io:format("~n"),
	erlang:send_after(?TICK_INTERVAL, self(), tick),
	wor_entmgr:update_component(WorldEnt, fun(AfterWorld) ->
		#wor_world_comp{ticks = Ticks} = AfterWorld,
		AfterWorld#wor_world_comp{ticks = Ticks + ?TICK_INTERVAL}
	end, world).
