-module(wor_logger).

%% public
-export([tick/1]).
-export([do_log_exp/2]).
-export([do_log_news/1]).
-export([do_log_chat/2]).
-export([do_log_dead/1]).
-export([do_log_grab/2]).
-export([do_log_drop/2]).
-export([do_log_attach/1]).
-export([do_log_detach/1]).
-export([do_log_attack/3]).
-export([do_log_private/2]).
-export([do_log_level_up/2]).
-export([do_log_teleport_to/2]).
-export([do_log_teleport_from/2]).

-include("types.hrl").

-type log_type() ::
	exp           |
	news          |
	chat          |
	dead          |
	alive         |
	grab          |
	drop          |
	attach        |
	detach        |
	attack        |
	private       |
	level_up      |
	teleport_to   |
	teleport_from.
-export_type([log_type/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_log_exp
%% ------------------------------------

-spec do_log_exp(
	wor_entity(), integer()
) -> ok.

do_log_exp(UserEnt, Exp) ->
	do_user_log(UserEnt, [
		wor_utils:log_type_to_json(exp),
		Exp
	]).

%% ------------------------------------
%% do_log_news
%% ------------------------------------

-spec do_log_news(
	binary()
) -> ok.

do_log_news(Text) ->
	do_world_log([
		wor_utils:log_type_to_json(news),
		Text
	]).

%% ------------------------------------
%% do_log_chat
%% ------------------------------------

-spec do_log_chat(
	wor_entity(), binary()
) -> ok.

do_log_chat(UserEnt, Message) ->
	case wor_entmgr:find_component(UserEnt, nameable) of
		{ok, Nameable} ->
			#wor_nameable_comp{
				name = Name
			} = Nameable,
			#wor_placeable_comp{
				zone = CurZone
			} = wor_entmgr:get_component(UserEnt, placeable),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(chat),
				Name, Message
			]);
		_ -> ok
	end.

%% ------------------------------------
%% do_log_dead
%% ------------------------------------

-spec do_log_dead(
	wor_entity()
) -> ok.

do_log_dead(UserEnt) ->
	case wor_entmgr:find_component(UserEnt, nameable) of
		{ok, Nameable} ->
			#wor_nameable_comp{name = Name} = Nameable,
			do_world_log([
				wor_utils:log_type_to_json(dead),
				Name
			]);
		_ -> ok
	end.

%% ------------------------------------
%% do_log_grab
%% ------------------------------------

-spec do_log_grab(
	wor_entity(), wor_entity()
) -> ok.

do_log_grab(UserEnt, ItemEnt) ->
	case wor_entmgr:has_component(UserEnt, userable) of
		true ->
			[
				#wor_nameable_comp{name = Name},
				#wor_placeable_comp{zone = CurZone}
			] = wor_entmgr:get_entity_components(UserEnt, [nameable, placeable]),
			#wor_nameable_comp{
				name = ItemName
			} = wor_entmgr:get_component(ItemEnt, nameable),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(grab),
				Name, ItemName
			]);
		false ->
			ok
	end.

%% ------------------------------------
%% do_log_drop
%% ------------------------------------

-spec do_log_drop(
	wor_entity(), wor_entity()
) -> ok.

do_log_drop(UserEnt, ItemEnt) ->
	case wor_entmgr:has_component(UserEnt, userable) of
		true ->
			[
				#wor_nameable_comp{name = Name},
				#wor_placeable_comp{zone = CurZone}
			] = wor_entmgr:get_entity_components(UserEnt, [nameable, placeable]),
			#wor_nameable_comp{
				name = ItemName
			} = wor_entmgr:get_component(ItemEnt, nameable),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(drop),
				Name, ItemName
			]);
		false ->
			ok
	end.

%% ------------------------------------
%% do_log_attach
%% ------------------------------------

-spec do_log_attach(
	wor_entity()
) -> ok.

do_log_attach(UserEnt) ->
	case wor_entmgr:find_component(UserEnt, nameable) of
		{ok, Nameable} ->
			#wor_nameable_comp{name = Name} = Nameable,
			do_world_log([
				wor_utils:log_type_to_json(attach),
				Name
			]);
		_ -> ok
	end.

%% ------------------------------------
%% do_log_detach
%% ------------------------------------

-spec do_log_detach(
	wor_entity()
) -> ok.

do_log_detach(UserEnt) ->
	case wor_entmgr:find_component(UserEnt, nameable) of
		{ok, Nameable} ->
			#wor_nameable_comp{name = Name} = Nameable,
			do_world_log([
				wor_utils:log_type_to_json(detach),
				Name
			]);
		_ -> ok
	end.

%% ------------------------------------
%% do_log_attack
%% ------------------------------------

-spec do_log_attack(
	wor_entity(), binary(), integer()
) -> ok.

do_log_attack(UserEnt, WhomName, Damage) ->
	do_user_log(UserEnt, [
		wor_utils:log_type_to_json(attack),
		WhomName,
		Damage
	]).

%% ------------------------------------
%% do_log_private
%% ------------------------------------

-spec do_log_private(
	wor_entity(), binary()
) -> ok.

do_log_private(UserEnt, Text) ->
	do_user_log(UserEnt, [
		wor_utils:log_type_to_json(private),
		Text
	]).

%% ------------------------------------
%% do_log_level_up
%% ------------------------------------

-spec do_log_level_up(
	wor_entity(), integer()
) -> ok.

do_log_level_up(UserEnt, Lev) ->
	case wor_entmgr:has_component(UserEnt, userable) of
		true ->
			[
				#wor_nameable_comp{name = Name},
				#wor_placeable_comp{zone = CurZone}
			] = wor_entmgr:get_entity_components(UserEnt, [nameable, placeable]),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(level_up),
				Name, Lev
			]);
		false -> ok
	end.

%% ------------------------------------
%% do_log_teleport_to
%% ------------------------------------

-spec do_log_teleport_to(
	wor_entity(), wor_entity()
) -> ok.

do_log_teleport_to(UserEnt, ToZoneEnt) ->
	case wor_entmgr:has_component(UserEnt, userable) of
		true ->
			[
				#wor_nameable_comp{name = Name},
				#wor_placeable_comp{zone = CurZone}
			] = wor_entmgr:get_entity_components(UserEnt, [nameable, placeable]),
			#wor_zone_comp{
				name = ToZoneName
			} = wor_entmgr:get_component(ToZoneEnt, zone),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(teleport_to),
				Name, ToZoneName
			], fun(OtherUser) -> OtherUser /= UserEnt end);
		false -> ok
	end.

%% ------------------------------------
%% do_log_teleport_from
%% ------------------------------------

-spec do_log_teleport_from(
	wor_entity(), wor_entity()
) -> ok.

do_log_teleport_from(UserEnt, FromZoneEnt) ->
	case wor_entmgr:has_component(UserEnt, userable) of
		true ->
			[
				#wor_nameable_comp{name = Name},
				#wor_placeable_comp{zone = CurZone}
			] = wor_entmgr:get_entity_components(UserEnt, [nameable, placeable]),
			#wor_zone_comp{
			   name = FromZoneName
			} = wor_entmgr:get_component(FromZoneEnt, zone),
			do_zone_log(CurZone, [
				wor_utils:log_type_to_json(teleport_from),
				Name, FromZoneName
			], fun(OtherUser) -> OtherUser /= UserEnt end);
		false -> ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% do_user_log
%% ------------------------------------

-spec do_user_log(
	wor_entity(), wor_json()
) -> ok.

do_user_log(UserEnt, MsgValue) ->
	case wor_entmgr:find_component(UserEnt, userable) of
		{ok, #wor_userable_comp{user = User}} ->
			wor_user:send_json(User, <<"log_info">>, MsgValue);
		_ ->
			ok
	end.

%% ------------------------------------
%% do_zone_log/2
%% ------------------------------------

-spec do_zone_log(
	wor_entity(), wor_json()
) -> ok.

do_zone_log(ZoneEnt, MsgValue) ->
	do_zone_log(ZoneEnt, MsgValue, fun(_Entity) -> true end).

%% ------------------------------------
%% do_zone_log/3
%% ------------------------------------

-spec do_zone_log(
	wor_entity(), wor_json(), fun((wor_entity()) -> boolean())
) -> ok.

do_zone_log(ZoneEnt, MsgValue, FilterFunc) ->
	Users = wor_entmgr:select_components(
		fun(UserEnt, [Placeable, Userable]) ->
			#wor_placeable_comp{
				zone = UserZone
			} = Placeable,
			#wor_userable_comp{
				user = User
			} = Userable,
			case UserZone == ZoneEnt andalso FilterFunc(UserEnt) of
				true  -> {value, User};
				false -> false
			end
		end, [placeable, userable], {zone_index, wor_utils:zone_index(ZoneEnt)}),
	dict:fold(fun(_UserEnt, User, AccIn) ->
		AccIn = wor_user:send_json(User, <<"log_info">>, MsgValue)
	end, ok, Users).

%% ------------------------------------
%% do_world_log
%% ------------------------------------

-spec do_world_log(
	wor_json()
) -> ok.

do_world_log(MsgValue) ->
	wor_entmgr:fold_components(fun(_UserEnt, Userable, AccIn) ->
		#wor_userable_comp{user = User} = Userable,
		AccIn = wor_user:send_json(User, <<"log_info">>, MsgValue)
	end, ok, userable).
