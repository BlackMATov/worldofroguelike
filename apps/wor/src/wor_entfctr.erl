-module(wor_entfctr).

-export([create_entity/4]).
-export([create_from_ascii_map/3]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% create_entity
%% ------------------------------------

-spec create_entity(
	atom(), wor_vec2(), wor_entity(), list()
) -> wor_entity().

create_entity(EntityType, Pos, Zone, Opts) ->
	Entity = wor_entmgr:create_entity(
		comps_for_entity(EntityType, Pos, Zone, Opts)),
	Equipment = equipment_for_entity(EntityType, Pos, Zone),
	ok = wor_inventory:do_grab(Entity, Equipment),
	ok = wor_equip_slots:do_equip(Entity, Equipment),
	ok = wor_fighter:do_recalculate(Entity),
	ok = wor_health:do_recalculate(Entity),
	ok = wor_health:do_restore(Entity),
	Entity.

%% ------------------------------------
%% create_from_ascii_map
%% ------------------------------------

-spec create_from_ascii_map(
	list(), list(), wor_entity()
) -> wor_vec2().

create_from_ascii_map(AsciiMap, MapLegend, Zone) ->
	lists:foldl(fun(AsciiMapLine, {AccW,Y}) ->
		TW = create_from_ascii_map_line(AsciiMapLine, MapLegend, Zone, Y),
		{max(AccW,TW), Y + 1}
	end, {0,0}, AsciiMap).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% create_from_ascii_map_line
%% ------------------------------------

-spec create_from_ascii_map_line(
	binary(), list(), wor_entity(), integer()
) -> integer().

create_from_ascii_map_line(AsciiMapLine, MapLegend, Zone, Y) ->
	AsciiLine = binary:bin_to_list(AsciiMapLine),
	lists:foldl(fun(AsciiEntity, X) ->
		create_from_legend({X,Y}, Zone, AsciiEntity, MapLegend),
		X + 1
	end, 0, AsciiLine).

%% ------------------------------------
%% create_from_legend
%% ------------------------------------

-spec create_from_legend(
	wor_vec2(), wor_entity(), integer(), list()
) -> wor_entity() | undefined.

create_from_legend(Pos, Zone, AsciiEntity, MapLegend) ->
	case ascii_entity_to_type(AsciiEntity, MapLegend) of
		undefined ->
			undefined;
		{EntityType, LegendOpts} ->
			create_entity(EntityType, Pos, Zone, LegendOpts)
	end.

%% ------------------------------------
%% ascii_entity_to_type
%% ------------------------------------

-spec ascii_entity_to_type(
	integer(), list()
) -> {atom(), list()} | undefined.

ascii_entity_to_type(AsciiEntity, MapLegend) ->
	lists:foldl(fun(LegendLine, AccIn) ->
		{[{BinChar, BinEntityType}, {<<"opts">>, LegendOpts}]} = LegendLine,
		[AsciiChar|_] = binary:bin_to_list(BinChar),
		case AsciiChar == AsciiEntity of
			true ->
				EntityType = binary_to_atom(BinEntityType, latin1),
				EntityOpts = lists:foldl(fun(JsonOpt, OptsAccIn) ->
					{[{PropBinName, PropValue}]} = JsonOpt,
					PropName = binary_to_atom(PropBinName, latin1),
					[{PropName, PropValue} | OptsAccIn]
				end, [], LegendOpts),
				{EntityType, EntityOpts};
			_ ->
				AccIn
		end
	end, undefined, MapLegend).

%% ------------------------------------
%% equipment_for_entity
%% ------------------------------------

-spec equipment_for_entity(
	atom(), wor_vec2(), wor_entity()
) -> list().

equipment_for_entity(orc, Pos, Zone) ->
	[
		create_entity(hard_sword,  Pos, Zone, []),
		create_entity(hard_helmet, Pos, Zone, [])
	];

equipment_for_entity(hobbit, Pos, Zone) ->
	[
		create_entity(sword, Pos, Zone, []),
		create_entity(light_helmet, Pos, Zone, [])
	];

equipment_for_entity(zombie, Pos, Zone) ->
	[
		create_entity(light_sword,  Pos, Zone, []),
		create_entity(light_helmet, Pos, Zone, [])
	];

equipment_for_entity(_EntityType, _Pos, _Zone) ->
	[].

%% ------------------------------------
%% simple_slots
%% ------------------------------------

simple_slots() ->
	dict:from_list([
		{head,      undefined},
		{body,      undefined},
		{left_arm,  undefined},
		{right_arm, undefined}
	]).

%% ------------------------------------
%% simple_enemy_btree
%% ------------------------------------

simple_enemy_btree() ->
	CheckTargetNode = wor_btree:bt_or([
		wor_btree:bt_and([
			wor_btree_nodes:if_has_target(),
			wor_btree_nodes:if_target_los(8)
		]),
		wor_btree_nodes:find_target(8)
	]),
	BattleNode = wor_btree:bt_or([
		wor_btree:bt_and([
			wor_btree_nodes:if_target_los(2),
			wor_btree_nodes:attack_target(),
			wor_btree_nodes:wait(500,750)
		]),
		wor_btree:bt_and([
			wor_btree_nodes:step_to_target(),
			wor_btree_nodes:wait(500,750)
		])
	]),
	BattleLoop = wor_btree:bt_loop_and([
		CheckTargetNode,
		BattleNode
	]),
	WalkerNode = wor_btree:bt_and([
		wor_btree_nodes:random_step(),
		wor_btree_nodes:wait(1000,2000)
	]),
	WaitNode = wor_btree_nodes:wait(1000,2000),
	wor_btree:bt_loop_or([
		BattleLoop,
		WalkerNode,
		WaitNode
	]).

%% ------------------------------------
%% simple_enemy_comps
%% ------------------------------------

simple_enemy_comps(Pos, Zone, View, Group, {Str, Dex, Int}, Name, Friendly) ->
	Relations = case Friendly of
		true  -> dict:new();
		false -> dict:from_list([{player, enemy}])
	end,
	[
		{placeable,   wor_comps:new_placeable(Pos, Zone, View, true, false, true)},
		{relation,    wor_comps:new_relation(Group, Relations)},
		{brain,       wor_comps:new_brain()},
		{btree,       wor_comps:new_btree(simple_enemy_btree())},
		{health,      wor_comps:new_health()},
		{stats,       wor_comps:new_stats(Str, Dex, Int)},
		{fighter,     wor_comps:new_fighter()},
		{experience,  wor_comps:new_experience()},
		{pusher,      wor_comps:new_pusher()},
		{pushable,    wor_comps:new_pushable(fighter)},
		{nameable,    wor_comps:new_nameable(Name)},
		{inventory,   wor_comps:new_inventory()},
		{equip_slots, wor_comps:new_equip_slots(simple_slots())}
	].

%% ------------------------------------
%% simple_armor_comps
%% ------------------------------------

simple_armor_comps(Pos, Zone, View, Name, Slot, Def) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, View, true, true, true)},
		{nameable,  wor_comps:new_nameable(Name)},
		{inv_item,  wor_comps:new_inv_item()},
		{equipment, wor_comps:new_equipment(Slot)},
		{usable,    wor_comps:new_usable(equipment)},
		{add_stat,  wor_comps:new_add_stat([{def, Def}])}
	].

%% ------------------------------------
%% simple_weapon_comps
%% ------------------------------------

simple_weapon_comps(Pos, Zone, View, Name, MinDmg, MaxDmg) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, View, true, true, true)},
		{nameable,  wor_comps:new_nameable(Name)},
		{inv_item,  wor_comps:new_inv_item()},
		{equipment, wor_comps:new_equipment(right_arm)},
		{usable,    wor_comps:new_usable(equipment)},
		{add_stat,  wor_comps:new_add_stat([{min_dmg, MinDmg}, {max_dmg, MaxDmg}])}
	].

%% ------------------------------------
%% comps_for_entity
%% ------------------------------------

-spec comps_for_entity(
	atom(), wor_vec2(), wor_entity(), list()
) -> list().

comps_for_entity(wall, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, wall, true, false, false)}
	];

comps_for_entity(tree, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, tree, true, false, false)}
	];

comps_for_entity(rock, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, rock, true, false, false)}
	];

comps_for_entity(water, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, water, true, false, true)}
	];

comps_for_entity(ver_window, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, ver_window, true, false, true)},
		{window,    wor_comps:new_window()},
		{pushable,  wor_comps:new_pushable(window)}
	];

comps_for_entity(hor_window, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, hor_window, true, false, true)},
		{window,    wor_comps:new_window()},
		{pushable,  wor_comps:new_pushable(window)}
	];

comps_for_entity(door, Pos, Zone, Opts) ->
	Opened = proplists:get_value(opened, Opts),
	{View, Passable, Transparent} = case Opened of
		true  -> {opened_door, true,  true};
		false -> {closed_door, false, false}
	end,
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, View, true, Passable, Transparent)},
		{door,      wor_comps:new_door(Opened)},
		{usable,    wor_comps:new_usable(door)},
		{pushable,  wor_comps:new_pushable(door)}
	];

comps_for_entity(orc, Pos, Zone, _Opts) ->
	simple_enemy_comps(
		Pos, Zone, orc, orc, {2, 1, 1}, <<"orc">>, false);

comps_for_entity(rat, Pos, Zone, _Opts) ->
	simple_enemy_comps(
		Pos, Zone, rat, rat, {2, 1, 1}, <<"rat">>, false);

comps_for_entity(human, Pos, Zone, _Opts) ->
	simple_enemy_comps(
		Pos, Zone, human, human, {2, 1, 1}, <<"human">>, true);

comps_for_entity(hobbit, Pos, Zone, _Opts) ->
	simple_enemy_comps(
		Pos, Zone, hobbit, hobbit, {2, 1, 1}, <<"hobbit">>, false);

comps_for_entity(zombie, Pos, Zone, _Opts) ->
	simple_enemy_comps(
		Pos, Zone, zombie, zombie, {2, 1, 1}, <<"zombie">>, false);

comps_for_entity(player, Pos, Zone, Opts) ->
	User = proplists:get_value(user, Opts),
	Name = proplists:get_value(name, Opts, <<"player">>),
	[
		{placeable,   wor_comps:new_placeable(Pos, Zone, player, true, false, true)},
		{relation,    wor_comps:new_relation(player)},
		{brain,       wor_comps:new_brain()},
		{userable,    wor_comps:new_userable(User)},
		{health,      wor_comps:new_health()},
		{stats,       wor_comps:new_stats(2, 1, 1)},
		{fighter,     wor_comps:new_fighter()},
		{experience,  wor_comps:new_experience()},
		{pusher,      wor_comps:new_pusher()},
		{usabler,     wor_comps:new_usabler()},
		{pushable,    wor_comps:new_pushable(fighter)},
		{nameable,    wor_comps:new_nameable(Name)},
		{inventory,   wor_comps:new_inventory()},
		{equip_slots, wor_comps:new_equip_slots(simple_slots())}
	];

comps_for_entity(spirit, Pos, Zone, Opts) ->
	User = proplists:get_value(user, Opts),
	Name = proplists:get_value(name, Opts, <<"player">>),
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, spirit, true, true, true)},
		{brain,     wor_comps:new_brain()},
		{spirit,    wor_comps:new_spirit()},
		{userable,  wor_comps:new_userable(User)},
		{nameable,  wor_comps:new_nameable(Name)}
	];

comps_for_entity(respawn, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, respawn, true, true, true)},
		{respawn,   wor_comps:new_respawn()},
		{stepable,  wor_comps:new_stepable(respawn)}
	];

comps_for_entity(portal, Pos, Zone, Opts) ->
	Name     = proplists:get_value(name,     Opts),
	ToZone   = proplists:get_value(tozone,   Opts),
	ToTarget = proplists:get_value(totarget, Opts),
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, portal, true, true, true)},
		{portal,    wor_comps:new_portal(Name, ToZone, ToTarget)},
		{stepable,  wor_comps:new_stepable(portal)}
	];

comps_for_entity(mobfarm, Pos, Zone, Opts) ->
	Type   = proplists:get_value(type,   Opts),
	Max    = proplists:get_value(max,    Opts),
	Period = proplists:get_value(period, Opts),
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, none, false, true, true)},
		{mobfarm,   wor_comps:new_mobfarm(Type, Max, Period)}
	];

comps_for_entity(light_sword, Pos, Zone, _Opts) ->
	simple_weapon_comps(Pos, Zone, sword, <<"light sword">>, 4, 5);
comps_for_entity(sword, Pos, Zone, _Opts) ->
	simple_weapon_comps(Pos, Zone, sword, <<"sword">>, 5, 7);
comps_for_entity(hard_sword, Pos, Zone, _Opts) ->
	simple_weapon_comps(Pos, Zone, sword, <<"hard sword">>, 8, 10);

comps_for_entity(light_helmet, Pos, Zone, _Opts) ->
	simple_armor_comps(Pos, Zone, helmet, <<"light helmet">>, head, 3);
comps_for_entity(helmet, Pos, Zone, _Opts) ->
	simple_armor_comps(Pos, Zone, helmet, <<"helmet">>, head, 4);
comps_for_entity(hard_helmet, Pos, Zone, _Opts) ->
	simple_armor_comps(Pos, Zone, helmet, <<"hard helmet">>, head, 6);

comps_for_entity(potion, Pos, Zone, _Opts) ->
	[
		{placeable, wor_comps:new_placeable(Pos, Zone, potion, true, true, true)},
		{nameable,  wor_comps:new_nameable(<<"potion">>)},
		{inv_item,  wor_comps:new_inv_item()},
		{food,      wor_comps:new_food(5)},
		{usable,    wor_comps:new_usable(food)}
	].
