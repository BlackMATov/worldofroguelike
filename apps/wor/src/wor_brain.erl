-module(wor_brain).

%% public
-export([tick/1]).
-export([do_cooldown/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	wor_entmgr:fold_components(fun(BrainEnt, Brain, AccIn) ->
		AccIn = do_brain_action(Brain, BrainEnt),
		AccIn = do_after_action(WorldTicks, BrainEnt)
	end, ok, brain).

%% ------------------------------------
%% do_cooldown
%% ------------------------------------

-spec do_cooldown(
	wor_entity(), integer()
) -> ok.

do_cooldown(Entity, Cooldown) ->
	wor_entmgr:supdate_component(Entity, fun(Brain) ->
		#wor_brain_comp{ticks = Ticks} = Brain,
		Brain#wor_brain_comp{ticks = Ticks + Cooldown}
	end, brain).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% do_brain_action
%% ------------------------------------

-spec do_brain_action(
	wor_brain_comp(), wor_entity()
) -> ok.

do_brain_action(
	#wor_brain_comp{action = undefined},
	_Entity
)->
	ok;

do_brain_action(
	#wor_brain_comp{action = Action},
	Entity
) ->
	ActionType = wor_action:get_type(Action),
	do_concrete_action(ActionType, Action, Entity).

%% ------------------------------------
%% do_concrete_action
%% ------------------------------------

-spec do_concrete_action(
	wor_action:type(), wor_action:state(), wor_entity()
) -> ok.

do_concrete_action(wait, Action, Entity) ->
	Cooldown = wor_action:get_opt(Action, cooldown),
	wor_brain:do_cooldown(Entity, Cooldown);

do_concrete_action(use, Action, Entity) ->
	Delta = wor_action:get_opt(Action, delta),
	wor_usable:do_use(Entity, Delta);

do_concrete_action(chat, Action, Entity) ->
	Message = wor_action:get_opt(Action, message),
	wor_logger:do_log_chat(Entity, Message);

do_concrete_action(random_step, _Action, Entity) ->
	wor_placeable:do_random_step(Entity);

do_concrete_action(step, Action, Entity) ->
	Delta = wor_action:get_opt(Action, delta),
	wor_placeable:do_step(Entity, Delta);

do_concrete_action(grab_item, Action, Entity) ->
	ItemEnt = wor_action:get_opt(Action, item),
	wor_inventory:do_grab(Entity, ItemEnt);

do_concrete_action(drop_item, Action, Entity) ->
	ItemEnt = wor_action:get_opt(Action, item),
	wor_inventory:do_drop(Entity, ItemEnt);

do_concrete_action(use_item, Action, Entity) ->
	ItemEnt = wor_action:get_opt(Action, item),
	wor_usable:do_use_item(Entity, ItemEnt).

%% ------------------------------------
%% do_after_action
%% ------------------------------------

-spec do_after_action(
	integer(), wor_entity()
) -> ok.

do_after_action(WorldTicks, Entity) ->
	wor_entmgr:supdate_component(Entity, fun(Brain) ->
		#wor_brain_comp{ticks = Ticks} = Brain,
		Brain#wor_brain_comp{
			ticks  = max(WorldTicks, Ticks),
			action = undefined
		}
	end, brain).
