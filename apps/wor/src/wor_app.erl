-module(wor_app).
-behaviour(application).

%% application
-export([start/2]).
-export([stop/1]).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR application
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start
%% ------------------------------------

start(_Type, _Args) ->
	{ok, Port}      = application:get_env(port),
	{ok, Acceptors} = application:get_env(acceptors),
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/websocket", wor_socket,    []},
			{"/",          cowboy_static, {priv_file, wor, "client/index.html"}},
			{"/log",       cowboy_static, {priv_file, wor, "client/log.html"}},
			{"/[...]",     cowboy_static, {priv_dir,  wor, "client"}}
		]}
	]),
	{ok, _} = cowboy:start_http(http_listener, Acceptors,
		[{port, Port}],
		[{env, [{dispatch, Dispatch}]}]
	),
	wor_sup:start_link().

%% ------------------------------------
%% stop
%% ------------------------------------

stop(_State) ->
	ok.
