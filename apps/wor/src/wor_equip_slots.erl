-module(wor_equip_slots).

%% public
-export([tick/1]).
-export([do_equip/2]).
-export([do_try_equip/2]).
-export([do_unequip/2]).
-export([do_unequip_all/1]).
-export([is_equipped/2]).
-export([is_slot_free/2]).
-export([is_slot_exist/2]).
-export([slot_equipment/2]).

-include("types.hrl").

-type slot_type() ::
	head      |
	body      |
	left_arm  |
	right_arm.
-export_type([slot_type/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_equip
%% ------------------------------------

-spec do_equip(
	wor_entity(), wor_entity() | list(wor_entity())
) -> ok.

do_equip(Who, Whom) when is_integer(Whom) ->
	case can_do_equip(Who, Whom) of
		true ->
			#wor_equipment_comp{slot = Slot} =
				wor_entmgr:get_component(Whom, equipment),
			wor_entmgr:update_component(Who, fun(EquipSlots) ->
				#wor_equip_slots_comp{slots = Slots} = EquipSlots,
				NewSlots = dict:store(Slot, Whom, Slots),
				EquipSlots#wor_equip_slots_comp{slots = NewSlots}
			end, equip_slots),
			wor_fighter:do_recalculate(Who);
		false ->
			ok
	end;

do_equip(_Who, Items = []) when is_list(Items) ->
	ok;

do_equip(Who, Items = [ItemEnt|Tail]) when is_list(Items) ->
	ok = do_equip(Who, ItemEnt),
	do_equip(Who, Tail).

%% ------------------------------------
%% do_try_equip
%% ------------------------------------

-spec do_try_equip(
	wor_entity(), wor_entity()
) -> ok.

do_try_equip(Who, Whom) ->
	case can_do_equip(Who, Whom) of
		true ->
			case is_slot_free(Who, Whom) of
				true  -> do_equip(Who, Whom);
				false -> ok
			end;
		false ->
			ok
	end.

%% ------------------------------------
%% do_unequip
%% ------------------------------------

-spec do_unequip(
	wor_entity(), wor_entity()
) -> ok.

do_unequip(Who, Whom) ->
	case can_do_unequip(Who, Whom) of
		true ->
			#wor_equipment_comp{slot = Slot} =
				wor_entmgr:get_component(Whom, equipment),
			wor_entmgr:update_component(Who, fun(EquipSlots) ->
				#wor_equip_slots_comp{slots = Slots} = EquipSlots,
				NewSlots = dict:store(Slot, undefined, Slots),
				EquipSlots#wor_equip_slots_comp{slots = NewSlots}
			end, equip_slots),
			wor_fighter:do_recalculate(Who);
		false ->
			ok
	end.

%% ------------------------------------
%% do_unequip_all
%% ------------------------------------

-spec do_unequip_all(
	wor_entity()
) -> ok.

do_unequip_all(Who) ->
	case can_do_unequip_all(Who) of
		true ->
			#wor_equip_slots_comp{slots = Slots} =
				wor_entmgr:get_component(Who, equip_slots),
			dict:fold(fun(Slot, Equipment, AccIn) ->
				case is_slot_free(Who, Slot) of
					true  -> AccIn;
					false -> AccIn = do_unequip(Who, Equipment)
				end
			end, ok, Slots);
		false ->
			ok
	end.

%% ------------------------------------
%% is_equipped
%% ------------------------------------

-spec is_equipped(
	wor_entity(), wor_entity()
) -> boolean().

is_equipped(Who, Whom) ->
	#wor_equipment_comp{slot = Slot} =
		wor_entmgr:get_component(Whom, equipment),
	slot_equipment(Who, Slot) == Whom.

%% ------------------------------------
%% is_slot_free
%% ------------------------------------

-spec is_slot_free(
	wor_entity(), slot_type() | wor_entity()
) -> boolean().

is_slot_free(Who, Slot) when is_atom(Slot) ->
	is_slot_exist(Who, Slot) andalso
	slot_equipment(Who, Slot) == undefined;

is_slot_free(Who, Whom) when is_integer(Whom) ->
	#wor_equipment_comp{slot = Slot} =
		wor_entmgr:get_component(Whom, equipment),
	is_slot_free(Who, Slot).

%% ------------------------------------
%% is_slot_exist
%% ------------------------------------

-spec is_slot_exist(
	wor_entity(), slot_type() | wor_entity()
) -> boolean().

is_slot_exist(Who, Slot) when is_atom(Slot) ->
	#wor_equip_slots_comp{slots = Slots} =
		wor_entmgr:get_component(Who, equip_slots),
	dict:is_key(Slot, Slots);

is_slot_exist(Who, Whom) when is_integer(Whom) ->
	#wor_equipment_comp{slot = Slot} =
		wor_entmgr:get_component(Whom, equipment),
	is_slot_exist(Who, Slot).

%% ------------------------------------
%% slot_equipment
%% ------------------------------------

-spec slot_equipment(
	wor_entity(), slot_type()
) -> wor_entity() | undefined.

slot_equipment(Who, Slot) ->
	#wor_equip_slots_comp{slots = Slots} =
		wor_entmgr:get_component(Who, equip_slots),
	case dict:find(Slot, Slots) of
		{ok, Equipment} -> Equipment;
		_ -> undefined
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_equip
%% ------------------------------------

-spec can_do_equip(
	wor_entity(), wor_entity()
) -> boolean().

can_do_equip(Who, Whom) ->
	wor_entmgr:has_component(Who,  inventory)   andalso
	wor_entmgr:has_component(Who,  equip_slots) andalso
	wor_entmgr:has_component(Whom, equipment)   andalso
	wor_inventory:has_item(Who, Whom)           andalso
	is_slot_exist(Who, Whom).

%% ------------------------------------
%% can_do_unequip
%% ------------------------------------

-spec can_do_unequip(
	wor_entity(), wor_entity()
) -> boolean().

can_do_unequip(Who, Whom) ->
	wor_entmgr:has_component(Who,  inventory)   andalso
	wor_entmgr:has_component(Who,  equip_slots) andalso
	wor_entmgr:has_component(Whom, equipment)   andalso
	wor_inventory:has_item(Who, Whom)           andalso
	is_equipped(Who, Whom).

%% ------------------------------------
%% can_do_unequip_all
%% ------------------------------------

-spec can_do_unequip_all(
	wor_entity()
) -> boolean().

can_do_unequip_all(Who) ->
	wor_entmgr:has_component(Who, inventory) andalso
	wor_entmgr:has_component(Who, equip_slots).
