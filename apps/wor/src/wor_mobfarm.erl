-module(wor_mobfarm).

%% public
-export([tick/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	wor_entmgr:fold_components(fun(Entity, Mobfarm, AccIn) ->
		#wor_mobfarm_comp{
			type   = Type,
			max    = Max,
			period = Period,
			ticks  = Ticks
		} = Mobfarm,
		case WorldTicks >= Ticks of
			true ->
				ok = spawn_mob(Entity, Type, Max, WorldTicks),
				ok = wor_entmgr:update_component(
					Entity,
					Mobfarm#wor_mobfarm_comp{ticks = WorldTicks + Period},
					mobfarm);
			false ->
				AccIn
		end
	end, ok, mobfarm).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% spawn_mob
%% ------------------------------------

-spec spawn_mob(
	wor_entity(), binary(), integer(), integer()
) -> ok.

spawn_mob(FarmEnt, Type, Max, WorldTicks) ->
	#wor_placeable_comp{
		pos  = Pos,
		zone = ZoneEnt
	} = wor_entmgr:get_component(FarmEnt, placeable),
	Farmables = wor_entmgr:get_components(
		farmable, {farm_index, wor_utils:farm_index(FarmEnt)}),
	case Max > dict:size(Farmables) of
		true ->
			Places = [
				wor_vec2:add(Pos, { 0,  0}),
				wor_vec2:add(Pos, {-1, -1}),
				wor_vec2:add(Pos, {-1,  0}),
				wor_vec2:add(Pos, {-1,  1}),
				wor_vec2:add(Pos, { 0, -1}),
				wor_vec2:add(Pos, { 0,  1}),
				wor_vec2:add(Pos, { 1, -1}),
				wor_vec2:add(Pos, { 1,  0}),
				wor_vec2:add(Pos, { 1,  1})
			],
			try_spawn_mob(FarmEnt, ZoneEnt, Type, WorldTicks, Places);
		false ->
			ok
	end.

%% ------------------------------------
%% try_spawn_mob
%% ------------------------------------

-spec try_spawn_mob(
	wor_entity(), wor_entity(), binary(), integer(), list(wor_vec2())
) -> ok.

try_spawn_mob(_Entity, _ZoneEnt, _Type, _Ticks, []) ->
	ok;

try_spawn_mob(FarmEnt, ZoneEnt, Type, WorldTicks, [Pos | Tail]) ->
	case wor_zone:is_tile_empty(ZoneEnt, Pos) of
		true ->
			EntityType = binary_to_atom(Type, latin1),
			Entity = wor_entfctr:create_entity(EntityType, Pos, ZoneEnt, [
				{ticks, WorldTicks}
			]),
			ok = wor_entmgr:create_component(
				Entity, farmable, wor_comps:new_farmable(FarmEnt)),
			ok = wor_health:do_recalculate(Entity),
			ok = wor_fighter:do_recalculate(Entity);
		false ->
			try_spawn_mob(FarmEnt, ZoneEnt, Type, WorldTicks, Tail)
	end.
