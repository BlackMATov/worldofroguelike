-module(wor_comps).

-export([new_zone/2]).
-export([new_dead/0]).
-export([new_world/0]).
-export([new_btree/1]).
-export([new_brain/0]).
-export([new_door/1]).
-export([new_food/1]).
-export([new_window/0]).
-export([new_usable/1]).
-export([new_spirit/0]).
-export([new_usabler/0]).
-export([new_pusher/0]).
-export([new_pushable/1]).
-export([new_stepable/1]).
-export([new_portal/3]).
-export([new_mobfarm/3]).
-export([new_farmable/1]).
-export([new_nameable/1]).
-export([new_equipment/1]).
-export([new_equip_slots/1]).
-export([new_inv_item/0]).
-export([new_add_stat/1]).
-export([new_inventory/0]).
-export([new_health/0]).
-export([new_spawner/0]).
-export([new_respawn/0]).
-export([new_stats/3]).
-export([new_fighter/0]).
-export([new_relation/1]).
-export([new_relation/2]).
-export([new_experience/0]).
-export([new_userable/1]).
-export([new_placeable/6]).
-export([new_trash/1]).
-export([new_trasher/0]).
-export([new_requester/0]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% new_zone
%% ------------------------------------

-spec new_zone(
	binary(), boolean()
) -> wor_zone_comp().

new_zone(Name, Started) ->
	#wor_zone_comp{
		name    = Name,
		started = Started
	}.

%% ------------------------------------
%% new_dead
%% ------------------------------------

-spec new_dead(
) -> wor_dead_comp().

new_dead() ->
	#wor_dead_comp{}.

%% ------------------------------------
%% new_world
%% ------------------------------------

-spec new_world(
) -> wor_world_comp().

new_world() ->
	#wor_world_comp{}.

%% ------------------------------------
%% new_btree
%% ------------------------------------

-spec new_btree(
	wor_btree:bt_node()
) -> wor_btree_comp().

new_btree(Root) ->
	#wor_btree_comp{
		root = Root
	}.

%% ------------------------------------
%% new_brain
%% ------------------------------------

-spec new_brain(
) -> wor_brain_comp().

new_brain() ->
	#wor_brain_comp{}.

%% ------------------------------------
%% new_door
%% ------------------------------------

-spec new_door(
	boolean()
) -> wor_door_comp().

new_door(Opened) ->
	#wor_door_comp{
		opened = Opened
	}.

%% ------------------------------------
%% new_food
%% ------------------------------------

-spec new_food(
	integer()
) -> wor_food_comp().

new_food(Hp) ->
	#wor_food_comp{
		hp = Hp
	}.

%% ------------------------------------
%% new_window
%% ------------------------------------

-spec new_window(
) -> wor_window_comp().

new_window() ->
	#wor_window_comp{}.

%% ------------------------------------
%% new_usable
%% ------------------------------------

-spec new_usable(
	wor_comp_type()
) -> wor_usable_comp().

new_usable(Comp) ->
	#wor_usable_comp{
		comp = Comp
	}.

%% ------------------------------------
%% new_spirit
%% ------------------------------------

-spec new_spirit(
) -> wor_spirit_comp().

new_spirit() ->
	#wor_spirit_comp{}.

%% ------------------------------------
%% new_usabler
%% ------------------------------------

-spec new_usabler(
) -> wor_usabler_comp().

new_usabler() ->
	#wor_usabler_comp{}.

%% ------------------------------------
%% new_pusher
%% ------------------------------------

-spec new_pusher(
) -> wor_pusher_comp().

new_pusher() ->
	#wor_pusher_comp{}.

%% ------------------------------------
%% new_pushable
%% ------------------------------------

-spec new_pushable(
	wor_comp_type()
) -> wor_pushable_comp().

new_pushable(Comp) ->
	#wor_pushable_comp{
		comp = Comp
	}.

%% ------------------------------------
%% new_stepable
%% ------------------------------------

-spec new_stepable(
	wor_comp_type()
) -> wor_stepable_comp().

new_stepable(Comp) ->
	#wor_stepable_comp{
		comp = Comp
	}.

%% ------------------------------------
%% new_portal
%% ------------------------------------

-spec new_portal(
	binary(), binary(), binary()
) -> wor_portal_comp().

new_portal(Name, ToZone, ToTarget) ->
	#wor_portal_comp{
		name     = Name,
		tozone   = ToZone,
		totarget = ToTarget
	}.

%% ------------------------------------
%% new_mobfarm
%% ------------------------------------

-spec new_mobfarm(
	binary(), integer(), integer()
) -> wor_mobfarm_comp().

new_mobfarm(Type, Max, Period) ->
	#wor_mobfarm_comp{
		type   = Type,
		max    = Max,
		period = Period
	}.

%% ------------------------------------
%% new_farmable
%% ------------------------------------

-spec new_farmable(
	wor_entity()
) -> wor_farmable_comp().

new_farmable(Farm) ->
	#wor_farmable_comp{
		farm = Farm
	}.

%% ------------------------------------
%% new_nameable
%% ------------------------------------

-spec new_nameable(
	binary()
) -> wor_nameable_comp().

new_nameable(Name) ->
	#wor_nameable_comp{
		name = Name
	}.

%% ------------------------------------
%% new_equipment
%% ------------------------------------

-spec new_equipment(
	wor_equip_slots:slot_type()
) -> wor_equipment_comp().

new_equipment(Slot) ->
	#wor_equipment_comp{
		slot = Slot
	}.

%% ------------------------------------
%% new_equip_slots
%% ------------------------------------

-spec new_equip_slots(
	dict:dict(wor_equip_slots:slot_type(), wor_entity())
) -> wor_equip_slots_comp().

new_equip_slots(Slots) ->
	#wor_equip_slots_comp{
		slots = Slots
	}.

%% ------------------------------------
%% new_inv_item
%% ------------------------------------

-spec new_inv_item(
) -> wor_inv_item_comp().

new_inv_item() ->
	#wor_inv_item_comp{}.

%% ------------------------------------
%% new_add_stat
%% ------------------------------------

-spec new_add_stat(
	list({wor_stats:stat_type(), integer()})
) -> wor_add_stat_comp().

new_add_stat(Stats) ->
	#wor_add_stat_comp{
		stats = Stats
	}.

%% ------------------------------------
%% new_inventory
%% ------------------------------------

-spec new_inventory(
) -> wor_inventory_comp().

new_inventory() ->
	#wor_inventory_comp{}.

%% ------------------------------------
%% new_health
%% ------------------------------------

-spec new_health(
) -> wor_health_comp().

new_health() ->
	#wor_health_comp{}.

%% ------------------------------------
%% new_spawner
%% ------------------------------------

-spec new_spawner(
) -> wor_spawner_comp().

new_spawner() ->
	#wor_spawner_comp{}.

%% ------------------------------------
%% new_respawn
%% ------------------------------------

-spec new_respawn(
) -> wor_respawn_comp().

new_respawn() ->
	#wor_respawn_comp{}.

%% ------------------------------------
%% new_stats
%% ------------------------------------

-spec new_stats(
	integer(), integer(), integer()
) -> wor_stats_comp().

new_stats(Str, Dex, Int)  ->
	#wor_stats_comp{
		str = Str,
		dex = Dex,
		int = Int
	}.

%% ------------------------------------
%% new_fighter
%% ------------------------------------

-spec new_fighter(
) -> wor_fighter_comp().

new_fighter()  ->
	#wor_fighter_comp{}.

%% ------------------------------------
%% new_relation
%% ------------------------------------

-spec new_relation(
	wor_relation:group_type()
) -> wor_relation_comp().

new_relation(Group)  ->
	new_relation(Group, dict:new()).

-spec new_relation(
	wor_relation:group_type(),
	dict:dict(wor_relation:group_type(),wor_relation:relation_type())
) -> wor_relation_comp().

new_relation(Group, Relations)  ->
	#wor_relation_comp{
		group     = Group,
		relations = Relations
	}.

%% ------------------------------------
%% new_experience
%% ------------------------------------

-spec new_experience(
) -> wor_experience_comp().

new_experience()  ->
	#wor_experience_comp{}.

%% ------------------------------------
%% new_userable
%% ------------------------------------

-spec new_userable(
	pid()
) -> wor_userable_comp().

new_userable(User) ->
	#wor_userable_comp{
		user = User
	}.

%% ------------------------------------
%% new_placeable
%% ------------------------------------

-spec new_placeable(
	wor_vec2(), wor_entity(), wor_view_type(), boolean(), boolean(), boolean()
) -> wor_placeable_comp().

new_placeable(Pos, ZoneEnt, View, Visible, Passable, Transparent) ->
	#wor_placeable_comp{
		pos         = Pos,
		zone        = ZoneEnt,
		view        = View,
		visible     = Visible,
		passable    = Passable,
		transparent = Transparent
	}.

%% ------------------------------------
%% new_trash
%% ------------------------------------

-spec new_trash(
	integer()
) -> wor_trash_comp().

new_trash(Ticks) ->
	#wor_trash_comp{
		ticks = Ticks
	}.

%% ------------------------------------
%% new_trasher
%% ------------------------------------

-spec new_trasher(
) -> wor_trasher_comp().

new_trasher() ->
	#wor_trasher_comp{}.

%% ------------------------------------
%% new_requester
%% ------------------------------------

-spec new_requester(
) -> wor_requester_comp().

new_requester() ->
	#wor_requester_comp{}.
