-module(wor_placeable).

%% public
-export([tick/1]).
-export([do_step/2]).
-export([do_random_step/1]).

-include("types.hrl").
-define(STEP_COOLDOWN, 100).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_step
%% ------------------------------------

-spec do_step(
	wor_entity(), wor_vec2()
) -> ok.

do_step(Entity, Delta) ->
	case can_do_step(Entity) of
		true ->
			#wor_placeable_comp{
				pos      = Pos,
				zone     = Zone,
				passable = Passable
			} = wor_entmgr:get_component(Entity, placeable),
			NewPos = wor_vec2:add(Pos, Delta),
			FindResult = wor_zone:find_tile_impassable_entity(Zone, NewPos),
			case {Passable, FindResult} of
				{Passable, {error, out_zone}} ->
					ok;
				{Passable, {error, not_found}} ->
					do_concrete_step(Entity, NewPos);
				{true, {ok, _ImpassableEntity}} ->
					do_concrete_step(Entity, NewPos);
				{false, {ok, _ImpassableEntity}} ->
					wor_pushable:do_push(Entity, Delta)
			end;
		false ->
			ok
	end.

%% ------------------------------------
%% do_random_step
%% ------------------------------------

-spec do_random_step(
	wor_entity()
) -> ok.

do_random_step(Entity) ->
	case can_do_step(Entity) of
		true ->
			#wor_placeable_comp{
				pos  = Pos,
				zone = Zone
			} = wor_entmgr:get_component(Entity, placeable),
			Delta = wor_utils:random_direction(),
			NewPos = wor_vec2:add(Pos, Delta),
			FindResult = wor_zone:find_tile_impassable_entity(Zone, NewPos),
			case FindResult of
				{error, not_found} ->
					do_step(Entity, Delta);
				FindResult ->
					ok
			end;
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

-spec can_do_step(
	wor_entity()
) -> boolean().

can_do_step(Entity) ->
	wor_entmgr:has_component(Entity, placeable).

%% ------------------------------------
%% do_concrete_step
%% ------------------------------------

-spec do_concrete_step(
	wor_entity(), wor_vec2()
) -> ok.

do_concrete_step(Entity, NewPos) ->
	wor_entmgr:update_component(Entity, fun(Placeable) ->
		Placeable#wor_placeable_comp{pos = NewPos}
	end, placeable),
	wor_stepable:do_step(Entity),
	wor_brain:do_cooldown(Entity, ?STEP_COOLDOWN).
