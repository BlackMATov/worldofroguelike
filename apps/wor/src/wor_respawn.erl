-module(wor_respawn).

%% public
-export([tick/1]).
-export([spawn_user/4]).
-export([do_resurrect/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% spawn_user
%% ------------------------------------

-spec spawn_user(
	wor_entity(), pid(), binary(), integer()
) -> boolean().

spawn_user(RespawnEnt, User, Name, Ticks) ->
	#wor_placeable_comp{
		pos  = Pos,
		zone = ZoneEnt
	} = wor_entmgr:get_component(RespawnEnt, placeable),
	Places = [
		wor_vec2:add(Pos, { 0,  0}),
		wor_vec2:add(Pos, {-1, -1}),
		wor_vec2:add(Pos, {-1,  0}),
		wor_vec2:add(Pos, {-1,  1}),
		wor_vec2:add(Pos, { 0, -1}),
		wor_vec2:add(Pos, { 0,  1}),
		wor_vec2:add(Pos, { 1, -1}),
		wor_vec2:add(Pos, { 1,  0}),
		wor_vec2:add(Pos, { 1,  1})
	],
	try_spawn_user(User, Name, ZoneEnt, Ticks, Places).

%% ------------------------------------
%% do_resurrect
%% ------------------------------------

-spec do_resurrect(
	wor_entity(), wor_entity()
) -> ok.

do_resurrect(Who, Whom) ->
	case can_do_resurrect(Who, Whom) of
		true ->
			#wor_brain_comp{ticks = Ticks} =
				wor_entmgr:get_component(Whom, brain),
			#wor_userable_comp{user = User} =
				wor_entmgr:get_component(Whom, userable),
			#wor_nameable_comp{name = Name} =
				wor_entmgr:get_component(Whom, nameable),
			#wor_placeable_comp{pos = Pos, zone = ZoneEnt} =
				wor_entmgr:get_component(Whom, placeable),
			ok = wor_entmgr:remove_entity(Whom),
			true = try_spawn_user(User, Name, ZoneEnt, Ticks, [Pos]),
			ok;
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_resurrect
%% ------------------------------------

-spec can_do_resurrect(
	wor_entity(), wor_entity()
) -> boolean().

can_do_resurrect(Who, Whom) ->
	wor_entmgr:has_component(Who,  respawn) andalso
	wor_entmgr:has_component(Whom, spirit).

%% ------------------------------------
%% try_spawn_user
%% ------------------------------------

-spec try_spawn_user(
	pid(), binary(), wor_entity(), integer(), list(wor_vec2())
) -> boolean().

try_spawn_user(_User, _Name, _ZoneEnt, _Ticks, []) ->
	false;

try_spawn_user(User, Name, ZoneEnt, Ticks, [Pos | Places]) ->
	case wor_zone:is_tile_passable(ZoneEnt, Pos) of
		true ->
			UserEnt = wor_entfctr:create_entity(player, Pos, ZoneEnt, [
				{user,  User},
				{name,  Name},
				{ticks, Ticks}
			]),
			ok = wor_logger:do_log_attach(UserEnt),
			ok = wor_health:do_recalculate(UserEnt),
			ok = wor_fighter:do_recalculate(UserEnt),
			ok = wor_requester:send_self_info(UserEnt),
			true;
		false ->
			try_spawn_user(User, Name, ZoneEnt, Ticks, Places)
	end.
