-module(wor_zone).

%% public
-export([tick/1]).

%% public
-export([find_zone_by_name/1]).
-export([is_tile_empty/2]).
-export([is_tile_passable/2]).
-export([is_line_passable/2]).
-export([is_tile_transparent/2]).
-export([is_line_transparent/2]).
-export([find_tile_usable_entity/2]).
-export([find_tile_impassable_entity/2]).
-export([find_tile_intransparent_entity/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok = check_load().

%% ------------------------------------
%% find_zone_by_name
%% ------------------------------------

-spec find_zone_by_name(
	binary()
) -> {ok, wor_entity()} | {error, not_found}.

find_zone_by_name(Name) ->
	wor_entmgr:fold_components(fun(ZoneEnt, Zone, AccIn) ->
		case AccIn of
			{error, not_found} ->
				#wor_zone_comp{name = ZoneName} = Zone,
				case ZoneName of
					Name -> {ok, ZoneEnt};
					_    -> AccIn
				end;
			_ -> AccIn
		end
	end, {error, not_found}, zone).

%% ------------------------------------
%% is_tile_empty
%% ------------------------------------

-spec is_tile_empty(
	wor_entity(), wor_vec2()
) -> boolean().

is_tile_empty(ZoneEnt, Pos = {X,Y}) ->
	#wor_zone_comp{size = {W,H}} = wor_entmgr:get_component(ZoneEnt, zone),
	case (X >= 0) and (Y >= 0) and (X < W) and (Y < H) of
		true ->
			Entities = wor_entmgr:select_components(
				fun(_PlaceableEnt, [Placeable]) ->
					#wor_placeable_comp{
						zone = OtherZoneEnt
					} = Placeable,
					ZoneEnt == OtherZoneEnt
				end, [placeable], {tile_index, wor_utils:tile_index(Pos)}),
			dict:is_empty(Entities);
		false -> false
	end.

%% ------------------------------------
%% is_tile_passable
%% ------------------------------------

-spec is_tile_passable(
	wor_entity(), wor_vec2()
) -> boolean().

is_tile_passable(ZoneEnt, Pos) ->
	case find_tile_impassable_entity(ZoneEnt, Pos) of
		{ok, _ImpassableEntity} -> false;
		{error, out_zone}       -> false;
		{error, not_found}      -> true
	end.

%% ------------------------------------
%% is_line_passable
%% ------------------------------------

-spec is_line_passable(
	wor_entity(), list(wor_vec2())
) -> boolean().

is_line_passable(_ZoneEnt, []) ->
	true;

is_line_passable(ZoneEnt, [Pos|Tail]) ->
	case is_tile_passable(ZoneEnt, Pos) of
		true  -> is_line_passable(ZoneEnt, Tail);
		false -> false
	end.

%% ------------------------------------
%% is_tile_transparent
%% ------------------------------------

-spec is_tile_transparent(
	wor_entity(), wor_vec2()
) -> boolean().

is_tile_transparent(ZoneEnt, Pos) ->
	case find_tile_intransparent_entity(ZoneEnt, Pos) of
		{ok, _IntransparentEntity} -> false;
		{error, out_zone}          -> false;
		{error, not_found}         -> true
	end.

%% ------------------------------------
%% is_line_transparent
%% ------------------------------------

-spec is_line_transparent(
	wor_entity(), list(wor_vec2())
) -> boolean().

is_line_transparent(_ZoneEnt, []) ->
	true;

is_line_transparent(ZoneEnt, [Pos|Tail]) ->
	case is_tile_transparent(ZoneEnt, Pos) of
		true  -> is_line_transparent(ZoneEnt, Tail);
		false -> false
	end.

%% ------------------------------------
%% find_tile_usable_entity
%% ------------------------------------

-spec find_tile_usable_entity(
	wor_entity(), wor_vec2()
) -> {ok, wor_entity()} | {error, not_found} | {error, out_zone}.

find_tile_usable_entity(ZoneEnt, Pos = {X,Y}) ->
	#wor_zone_comp{size = {W,H}} = wor_entmgr:get_component(ZoneEnt, zone),
	case (X >= 0) and (Y >= 0) and (X < W) and (Y < H) of
		true ->
			Entities = wor_entmgr:select_components(
				fun(_UsableEnt, [Placeable, _Usable]) ->
					#wor_placeable_comp{zone = OtherZoneEnt} = Placeable,
					ZoneEnt == OtherZoneEnt
				end, [placeable, usable], {tile_index, wor_utils:tile_index(Pos)}),
			case dict:is_empty(Entities) of
				true  -> {error, not_found};
				false ->
					[Entity|_] = dict:fetch_keys(Entities),
					{ok, Entity}
			end;
		false -> {error, out_zone}
	end.

%% ------------------------------------
%% find_tile_impassable_entity
%% ------------------------------------

-spec find_tile_impassable_entity(
	wor_entity(), wor_vec2()
) -> {ok, wor_entity()} | {error, not_found} | {error, out_zone}.

find_tile_impassable_entity(ZoneEnt, Pos = {X,Y}) ->
	#wor_zone_comp{size = {W,H}} = wor_entmgr:get_component(ZoneEnt, zone),
	case (X >= 0) and (Y >= 0) and (X < W) and (Y < H) of
		true ->
			Entities = wor_entmgr:select_components(
				fun(_PlaceableEnt, [Placeable]) ->
					#wor_placeable_comp{
						zone     = OtherZoneEnt,
						passable = OtherPassable
					} = Placeable,
					ZoneEnt == OtherZoneEnt andalso not OtherPassable
				end, [placeable], {tile_index, wor_utils:tile_index(Pos)}),
			case dict:is_empty(Entities) of
				true  -> {error, not_found};
				false ->
					[Entity|_] = dict:fetch_keys(Entities),
					{ok, Entity}
			end;
		false -> {error, out_zone}
	end.

%% ------------------------------------
%% find_tile_intransparent_entity
%% ------------------------------------

-spec find_tile_intransparent_entity(
	wor_entity(), wor_vec2()
) -> {ok, wor_entity()} | {error, not_found} | {error, out_zone}.

find_tile_intransparent_entity(ZoneEnt, Pos = {X,Y}) ->
	#wor_zone_comp{size = {W,H}} = wor_entmgr:get_component(ZoneEnt, zone),
	case (X >= 0) and (Y >= 0) and (X < W) and (Y < H) of
		true ->
			Entities = wor_entmgr:select_components(
				fun(_PlaceableEnt, [Placeable]) ->
					#wor_placeable_comp{
						zone        = OtherZoneEnt,
						transparent = OtherTransparent
					} = Placeable,
					ZoneEnt == OtherZoneEnt andalso not OtherTransparent
				end, [placeable], {tile_index, wor_utils:tile_index(Pos)}),
			case dict:is_empty(Entities) of
				true  -> {error, not_found};
				false ->
					[Entity|_] = dict:fetch_keys(Entities),
					{ok, Entity}
			end;
		false -> {error, out_zone}
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% check_load
%% ------------------------------------

-spec check_load(
) -> ok.

check_load() ->
	NotLoadedZones = wor_entmgr:filter_components(fun(_Entity, Zone) ->
		not Zone#wor_zone_comp.loaded
	end, zone),
	NewZoneList = dict:fold(fun(Entity, Zone, AccIn) ->
		[{Entity, load_zone(Entity, Zone)} | AccIn]
	end, [], NotLoadedZones),
	lists:foreach(fun({Entity, NewZone}) ->
		wor_entmgr:update_component(Entity, NewZone, zone)
	end, NewZoneList).

%% ------------------------------------
%% load_zone
%% ------------------------------------

-spec load_zone(
	wor_entity(), wor_zone_comp()
) -> wor_zone_comp().

load_zone(
	ZoneEnt,
	Zone = #wor_zone_comp{name = Name}
) ->
	{ok, FileBin} = file:read_file(wor_utils:zone_filename(Name)),
	{[
		{<<"ascii">>,  AsciiMap},
		{<<"legend">>, MapLegend}
	]} = jiffy:decode(FileBin),
	Size = {W,H} = wor_entfctr:create_from_ascii_map(AsciiMap, MapLegend, ZoneEnt),
	true = ?WOR_MAX_ZONE_WIDTH  >= W,
	true = ?WOR_MAX_ZONE_HEIGHT >= H,
	Zone#wor_zone_comp{
		size   = Size,
		loaded = true
	}.
