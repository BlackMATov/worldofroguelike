-module(wor_door).

%% public
-export([tick/1]).
-export([do_open/1]).
-export([do_close/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_open
%% ------------------------------------

-spec do_open(
	wor_entity()
) -> ok.

do_open(Entity) ->
	case can_do_open(Entity) of
		true ->
			wor_entmgr:update_components(Entity, [
				{placeable, fun(Placeable) ->
					Placeable#wor_placeable_comp{
						view        = opened_door,
						passable    = true,
						transparent = true
					}
				end},
				{door, fun(Door) ->
					Door#wor_door_comp{opened = true}
				end}
			]);
		false ->
			ok
	end.

%% ------------------------------------
%% do_close
%% ------------------------------------

-spec do_close(
	wor_entity()
) -> ok.

do_close(Entity) ->
	case can_do_close(Entity) of
		true ->
			#wor_placeable_comp{
				pos  = Pos,
				zone = ZoneEnt
			} = wor_entmgr:get_component(Entity, placeable),
			case wor_zone:is_tile_passable(ZoneEnt, Pos) of
				true ->
					wor_entmgr:update_components(Entity, [
						{placeable, fun(Placeable) ->
							Placeable#wor_placeable_comp{
								view        = closed_door,
								passable    = false,
								transparent = false
							}
						end},
						{door, fun(Door) ->
							Door#wor_door_comp{opened = false}
						end}
					]);
				false ->
					ok
			end;
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_open
%% ------------------------------------

-spec can_do_open(
	wor_entity()
) -> boolean().

can_do_open(Entity) ->
	wor_entmgr:has_components(Entity, [placeable, door]).

%% ------------------------------------
%% can_do_close
%% ------------------------------------

-spec can_do_close(
	wor_entity()
) -> boolean().

can_do_close(Entity) ->
	wor_entmgr:has_components(Entity, [placeable, door]).
