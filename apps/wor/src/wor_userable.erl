-module(wor_userable).

%% public
-export([tick/1]).
-export([user_action/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	UserActions = dict:fold(fun(UserEnt, _Userable, AccIn) ->
		Action = wor_entmgr:rupdate_component(UserEnt, fun(OtherUserable) ->
			#wor_userable_comp{newaction = NewAction} = OtherUserable,
			{OtherUserable#wor_userable_comp{newaction = undefined}, NewAction}
		end, userable),
		case Action of
			undefined -> AccIn;
			_ -> [{UserEnt, Action} | AccIn]
		end
	end, [], wor_entmgr:filter_components(fun(UserEnt, _Userable) ->
		#wor_brain_comp{
			ticks = Ticks
		} = wor_entmgr:get_component(UserEnt, brain),
		Ticks =< WorldTicks
	end, userable)),
	lists:foreach(fun({UserEnt,Action}) ->
		wor_entmgr:update_component(UserEnt, fun(Brain) ->
			Brain#wor_brain_comp{action = Action}
		end, brain)
	end, UserActions).

%% ------------------------------------
%% user_action
%% ------------------------------------

-spec user_action(
	pid(), wor_action:state()
) -> ok.

user_action(User, Action) ->
	dict:fold(fun(UserEnt, _Userable, AccIn) ->
		AccIn = wor_entmgr:supdate_component(UserEnt, fun(OtherUserable) ->
			OtherUserable#wor_userable_comp{newaction = Action}
		end, userable)
	end, ok, wor_entmgr:filter_components(fun(_UserEnt, Userable) ->
		#wor_userable_comp{user = OtherUser} = Userable,
		User == OtherUser
	end, userable)).
