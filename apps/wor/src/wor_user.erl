-module(wor_user).
-behaviour(gen_server).

%% public
-export([start_link/1]).
-export([stop/1]).

%% public
-export([send_msg/2]).
-export([send_json/2]).
-export([send_json/3]).
-export([send_text/2]).
-export([send_binary/2]).

%% public
-export([json_msg_handle/2]).
-export([binary_msg_handle/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").

-record(state, {
	socket :: pid()
}).
-type state() :: #state{}.

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
	pid()
) -> {ok, pid()}.

start_link(Socket) ->
	gen_server:start_link(?MODULE, [Socket], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
	pid()
) -> ok.

stop(Pid) ->
	gen_server:call(Pid, stop).

%% ------------------------------------
%% send_msg
%% ------------------------------------

-spec send_msg(
	pid(), binary()
) -> ok.

send_msg(Pid, MsgId) ->
	gen_server:cast(Pid, {send_msg, MsgId}).

%% ------------------------------------
%% send_json
%% ------------------------------------

-spec send_json(
	pid(), wor_json()
) -> ok.

send_json(Pid, JsonMsg) ->
	gen_server:cast(Pid, {send_json, JsonMsg}).

-spec send_json(
	pid(), binary(), wor_json()
) -> ok.

send_json(Pid, MsgId, MsgValue) ->
	gen_server:cast(Pid, {send_json, MsgId, MsgValue}).

%% ------------------------------------
%% send_text
%% ------------------------------------

-spec send_text(
	pid(), binary()
) -> ok.

send_text(Pid, Msg) ->
	gen_server:cast(Pid, {send_text, Msg}).

%% ------------------------------------
%% send_binary
%% ------------------------------------

-spec send_binary(
	pid(), binary()
) -> ok.

send_binary(Pid, Binary) ->
	gen_server:cast(Pid, {send_binary, Binary}).

%% ------------------------------------
%% json_msg_handle
%% ------------------------------------

-spec json_msg_handle(
	pid(), wor_json()
) -> ok.

json_msg_handle(Pid, Json) ->
	gen_server:cast(Pid, {json_msg_handle, Json}).

%% ------------------------------------
%% binary_msg_handle
%% ------------------------------------

-spec binary_msg_handle(
	pid(), binary()
) -> ok.

binary_msg_handle(Pid, Binary) ->
	gen_server:cast(Pid, {binary_msg_handle, Binary}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, state()}.

init([Socket]) ->
	io:format("~p:init(Self:~p, Socket:~p)~n", [?MODULE, self(), Socket]),
	{ok, #state{socket = Socket}}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	io:format("~p:stop(Self:~p)~n", [?MODULE, self()]),
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({send_msg, MsgId},
	State = #state{socket = Socket}
) ->
	JsonMsg = [MsgId,[]],
	wor_socket:send_json(Socket, JsonMsg),
	{noreply, State};

handle_cast({send_json, JsonMsg},
	State = #state{socket = Socket}
) ->
	wor_socket:send_json(Socket, JsonMsg),
	{noreply, State};

handle_cast({send_json, MsgId, MsgValue},
	State = #state{socket = Socket}
) ->
	JsonMsg = [MsgId, MsgValue],
	wor_socket:send_json(Socket, JsonMsg),
	{noreply, State};

handle_cast({send_text, Msg},
	State = #state{socket = Socket}
) ->
	wor_socket:send_text(Socket, Msg),
	{noreply, State};

handle_cast({send_binary, Binary},
	State = #state{socket = Socket}
) ->
	wor_socket:send_binary(Socket, Binary),
	{noreply, State};

handle_cast({json_msg_handle, Json}, State) ->
	ok = json_msg_handle_impl(Json, State),
	{noreply, State};

handle_cast({binary_msg_handle, Binary}, State) ->
	ok = binary_msg_handle_impl(Binary, State),
	{noreply, State};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok = wor_spawner:detach_user(self()).

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% json_msg_handle_impl
%% ------------------------------------

-spec json_msg_handle_impl(
	wor_json(), state()
) -> ok.

json_msg_handle_impl([<<"request_ping">>, MsgValue], State) ->
	request_receive_impl(ping, MsgValue, State);

json_msg_handle_impl([<<"request_grab">>, MsgValue], State) ->
	request_receive_impl(grab, MsgValue, State);

json_msg_handle_impl([<<"request_inventory">>, MsgValue], State) ->
	request_receive_impl(inventory, MsgValue, State);

json_msg_handle_impl([<<"action_attach">>, MsgValue], State) ->
	action_receive_impl(attach, MsgValue, State);

json_msg_handle_impl([<<"action_use">>, MsgValue], State) ->
	action_receive_impl(use, MsgValue, State);

json_msg_handle_impl([<<"action_chat">>, MsgValue], State) ->
	action_receive_impl(chat, MsgValue, State);

json_msg_handle_impl([<<"action_step">>, MsgValue], State) ->
	action_receive_impl(step, MsgValue, State);

json_msg_handle_impl([<<"action_grab_item">>, MsgValue], State) ->
	action_receive_impl(grab_item, MsgValue, State);

json_msg_handle_impl([<<"action_drop_item">>, MsgValue], State) ->
	action_receive_impl(drop_item, MsgValue, State);

json_msg_handle_impl([<<"action_use_item">>, MsgValue], State) ->
	action_receive_impl(use_item, MsgValue, State);

json_msg_handle_impl(_Json, _State) ->
	ok.

%% ------------------------------------
%% binary_msg_handle_impl
%% ------------------------------------

-spec binary_msg_handle_impl(
	binary(), state()
) -> ok.

binary_msg_handle_impl(_Binary, _State) ->
	ok.

%% ------------------------------------
%% request_receive_impl
%% ------------------------------------

-spec request_receive_impl(
	atom(), wor_json(), state()
) -> ok.

request_receive_impl(ping, MsgValue, _State) ->
	ok = send_json(self(), <<"ping_info">>, MsgValue);

request_receive_impl(grab, _MsgValue, _State) ->
	ok = wor_requester:request_grab_info(self());

request_receive_impl(inventory, _MsgValue, _State) ->
	ok = wor_requester:request_inventory_info(self()).

%% ------------------------------------
%% action_receive_impl
%% ------------------------------------

-spec action_receive_impl(
	wor_action:type(), wor_json(), state()
) -> ok.

action_receive_impl(
	attach, [Name], _State
) when is_binary(Name), byte_size(Name) > 0 ->
	ok = wor_spawner:attach_user(self(), Name);

action_receive_impl(
	use, [Dx, Dy], _State
) when is_integer(Dx), is_integer(Dy),
	abs(Dx) < 2, abs(Dy) < 2, (Dx /= 0 orelse Dy /= 0)
->
	Action = wor_action:new(use, [{delta, {Dx, Dy}}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(
	chat, [Message], _State
) when is_binary(Message), byte_size(Message) > 0 ->
	Action = wor_action:new(chat, [{message, Message}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(
	step, [Dx, Dy], _State
) when is_integer(Dx), is_integer(Dy),
	abs(Dx) < 2, abs(Dy) < 2, (Dx /= 0 orelse Dy /= 0)
->
	Action = wor_action:new(step, [{delta, {Dx, Dy}}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(
	grab_item, [Item], _State
) when is_integer(Item) ->
	Action = wor_action:new(grab_item, [{item, Item}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(
	drop_item, [Item], _State
) when is_integer(Item) ->
	Action = wor_action:new(drop_item, [{item, Item}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(
	use_item, [Item], _State
) when is_integer(Item) ->
	Action = wor_action:new(use_item, [{item, Item}]),
	ok = wor_userable:user_action(self(), Action);

action_receive_impl(_Action, _MsgValue, _State) ->
	ok.
