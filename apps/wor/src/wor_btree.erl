-module(wor_btree).

%% public
-export([tick/1]).
-export([do_shutdown/1]).

-export([bt_or/1]).
-export([bt_and/1]).
-export([bt_act/1]).
-export([bt_loop/1]).
-export([bt_loop/2]).
-export([bt_loop_or/1]).
-export([bt_loop_or/2]).
-export([bt_loop_and/1]).
-export([bt_loop_and/2]).
-export([bt_cond/1]).
-export([bt_const/1]).

-include("types.hrl").

-type bt_result()     :: success | failure.
-type bt_node()       :: {bt_func(), term()}.
-type bt_blackboard() :: dict:dict().
-type bt_func()       :: fun((wor_entity(), bt_blackboard(), term()) -> bt_result()).
-type bt_cond_func()  :: fun((wor_entity(), bt_blackboard()) -> boolean()).
-type bt_act_func()   ::
	fun((wor_entity(), bt_blackboard()) ->
	{bt_result(), wor_action:state(), bt_blackboard()} | {bt_result(), bt_blackboard()}).

-export_type([bt_result/0, bt_node/0, bt_blackboard/0]).
-export_type([bt_func/0, bt_cond_func/0, bt_act_func/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	#wor_world_comp{ticks = WorldTicks} =
		wor_entmgr:get_component(World, world),
	Entities = wor_entmgr:select_components(fun(_BTreeEnt, [BTree, Brain]) ->
		#wor_brain_comp{ticks = Ticks} = Brain,
		case Ticks =< WorldTicks of
			true  ->
				#wor_btree_comp{
					coro       = Coro,
					root       = Root,
					blackboard = Blackboard
				} = BTree,
				{value, {Coro, Root, Blackboard}};
			false ->
				false
		end
	end, [btree, brain]),
	btree_process(Entities).

%% ------------------------------------
%% do_shutdown
%% ------------------------------------

-spec do_shutdown(
	wor_entity()
) -> ok.

do_shutdown(Entity) ->
	case wor_entmgr:find_component(Entity, btree) of
		{ok, BTree} ->
			#wor_btree_comp{
				coro = Coro
			} = BTree,
			case ecoro:is_dead(Coro) of
				true  -> ok;
				false -> ecoro:shutdown(Coro)
			end;
		_ ->
			ok
	end.

%% ------------------------------------
%% bt_xxx
%% ------------------------------------

-spec bt_or(list(bt_node())) -> bt_node().
bt_or(Childs) ->
	{fun bt_or_impl/3, Childs}.

-spec bt_and(list(bt_node())) -> bt_node().
bt_and(Childs) ->
	{fun bt_and_impl/3, Childs}.

-spec bt_act(bt_act_func()) -> bt_node().
bt_act(Func) ->
	{fun bt_act_impl/3, Func}.

-spec bt_loop(bt_node()) -> bt_node().
bt_loop(Child) ->
	{fun bt_loop_impl/3, {infinity, Child}}.

-spec bt_loop(integer(), bt_node()) -> bt_node().
bt_loop(N, Child) ->
	{fun bt_loop_impl/3, {N, Child}}.

-spec bt_loop_or(list(bt_node())) -> bt_node().
bt_loop_or(Childs) ->
	bt_loop(bt_or(Childs)).

-spec bt_loop_or(integer(), list(bt_node())) -> bt_node().
bt_loop_or(N, Childs) ->
	bt_loop(N, bt_or(Childs)).

-spec bt_loop_and(list(bt_node())) -> bt_node().
bt_loop_and(Childs) ->
	bt_loop(bt_and(Childs)).

-spec bt_loop_and(integer(), list(bt_node())) -> bt_node().
bt_loop_and(N, Childs) ->
	bt_loop(N, bt_and(Childs)).

-spec bt_cond(bt_cond_func()) -> bt_node().
bt_cond(Func) ->
	{fun bt_cond_impl/3, Func}.

-spec bt_const(bt_result()) -> bt_node().
bt_const(Result) ->
	{fun bt_const_impl/3, Result}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% btree_check_start
%% ------------------------------------

-spec btree_check_start(
	wor_entity(), ecoro:ecoro() | undefined, bt_node(), bt_blackboard()
) -> ecoro:ecoro().

btree_check_start(Entity, undefined, {Func,Param}, Blackboard) ->
	ecoro:start(fun(_Args) ->
		Func(Entity, Blackboard, Param)
	end);

btree_check_start(Entity, Coro, Root, Blackboard) ->
	case ecoro:is_dead(Coro) of
		true ->
			btree_check_start(Entity, undefined, Root, Blackboard);
		false ->
			Coro
	end.

%% ------------------------------------
%% btree_process
%% ------------------------------------

-spec btree_process(
	dict:dict()
) -> ok.

btree_process(Entities) ->
	dict:fold(fun(Entity, {Coro, Root, Blackboard}, AccIn) ->
		NewCoro = btree_check_start(Entity, Coro, Root, Blackboard),
		NewBlackboard = case ecoro:resume(NewCoro, {success, Blackboard}) of
			{true, {ResAction, ResBlackboard}} ->
				wor_entmgr:update_component(Entity, fun(Brain) ->
					Brain#wor_brain_comp{action = ResAction}
				end, brain),
				ResBlackboard;
			{false, {success, ResBlackboard}} ->
				ResBlackboard;
			{false, {failure, ResBlackboard}} ->
				ResBlackboard
		end,
		AccIn = wor_entmgr:update_component(Entity, fun(BTree) ->
			BTree#wor_btree_comp{
				coro       = NewCoro,
				blackboard = NewBlackboard
			}
		end, btree)
	end, ok, Entities).

%% ------------------------------------
%% bt_or_impl
%% ------------------------------------

-spec bt_or_impl(
	wor_entity(), bt_blackboard(), list(bt_node())
) -> {bt_result(), bt_blackboard()}.

bt_or_impl(_Entity, Blackboard, []) ->
	{failure, Blackboard};

bt_or_impl(Entity, Blackboard, [{Func,Param}|Tail]) ->
	case Func(Entity, Blackboard, Param) of
		{failure, NewBlackboard} ->
			bt_or_impl(Entity, NewBlackboard, Tail);
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_and_impl
%% ------------------------------------

-spec bt_and_impl(
	wor_entity(), bt_blackboard(), list(bt_node())
) -> {bt_result(), bt_blackboard()}.

bt_and_impl(_Entity, Blackboard, []) ->
	{success, Blackboard};

bt_and_impl(Entity, Blackboard, [{Func,Param}|Tail]) ->
	case Func(Entity, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_and_impl(Entity, NewBlackboard, Tail);
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_act_impl
%% ------------------------------------

-spec bt_act_impl(
	wor_entity(), bt_blackboard(), bt_act_func()
) -> {bt_result(), bt_blackboard()}.

bt_act_impl(Entity, Blackboard, Func) ->
	case Func(Entity, Blackboard) of
		{running, Action, Blackboard2} ->
			case ecoro:yield({Action, Blackboard2}) of
				{success, Blackboard3} ->
					bt_act_impl(Entity, Blackboard3, Func);
				{failure, Blackboard3} ->
					{failure, Blackboard3}
			end;
		{success, Action, Blackboard2} ->
			case ecoro:yield({Action, Blackboard2}) of
				{success, Blackboard3} ->
					{success, Blackboard3};
				{failure, Blackboard3} ->
					{failure, Blackboard3}
			end;
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_loop_impl
%% ------------------------------------

-spec bt_loop_impl(
	wor_entity(), bt_blackboard(), {infinity | integer(), bt_node()}
) -> {bt_result(), bt_blackboard()}.

bt_loop_impl(Entity, Blackboard, {infinity, Node = {Func,Param}}) ->
	case Func(Entity, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_loop_impl(Entity, NewBlackboard, {infinity, Node});
		Result ->
			Result
	end;

bt_loop_impl(_Entity, Blackboard, {0, _Node}) ->
	{success, Blackboard};

bt_loop_impl(Entity, Blackboard, {N, Node = {Func,Param}}) ->
	case Func(Entity, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_loop_impl(Entity, NewBlackboard, {N - 1, Node});
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_cond_impl
%% ------------------------------------

-spec bt_cond_impl(
	wor_entity(), bt_blackboard(), bt_cond_func()
) -> {bt_result(), bt_blackboard()}.

bt_cond_impl(Entity, Blackboard, Func) ->
	case Func(Entity, Blackboard) of
		true  -> {success, Blackboard};
		false -> {failure, Blackboard}
	end.

%% ------------------------------------
%% bt_const_impl
%% ------------------------------------

-spec bt_const_impl(
	wor_entity(), bt_blackboard(), bt_result()
) -> {bt_result(), bt_blackboard()}.

bt_const_impl(_Entity, Blackboard, Result) ->
	{Result, Blackboard}.
