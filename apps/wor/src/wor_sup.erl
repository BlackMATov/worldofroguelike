-module(wor_sup).
-behaviour(supervisor).

% public
-export([start_link/0]).

% supervisor
-export([init/1]).

-define(CHILD(I, Type),
	{I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR supervisor
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([]) ->
	Children = [
		?CHILD(wor_entmgr,   worker),
		?CHILD(wor_broadmgr, worker),
		?CHILD(wor_world,    worker)
	],
	{ok, {{one_for_all, 10, 60}, Children}}.
