-module(wor_portal).

%% public
-export([tick/1]).
-export([do_teleport/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(_World) ->
	ok.

%% ------------------------------------
%% do_teleport
%% ------------------------------------

-spec do_teleport(
	wor_entity(), wor_entity()
) -> ok.

do_teleport(PortalEnt, Entity) ->
	case can_do_teleport(PortalEnt, Entity) of
		true ->
			#wor_portal_comp{
				tozone   = ToZone,
				totarget = ToTarget
			} = wor_entmgr:get_component(PortalEnt, portal),
			Places = lists:foldl(fun({PortalPos, PortalZoneEnt}, AccIn) ->
				AccIn ++ [
					{wor_vec2:add(PortalPos, { 0,  0}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, {-1, -1}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, {-1,  0}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, {-1,  1}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, { 0, -1}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, { 0,  1}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, { 1, -1}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, { 1,  0}), PortalZoneEnt},
					{wor_vec2:add(PortalPos, { 1,  1}), PortalZoneEnt}
				]
			end, [], find_portal_places(ToZone, ToTarget)),
			try_teleport_entity(Entity, Places);
		false ->
			ok
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_do_teleport
%% ------------------------------------

-spec can_do_teleport(
	wor_entity(), wor_entity()
) -> boolean().

can_do_teleport(Who, Whom) ->
	wor_entmgr:has_component(Who,  portal) andalso
	wor_entmgr:has_component(Whom, userable).

%% ------------------------------------
%% find_portal_places
%% ------------------------------------

-spec find_portal_places(
	binary(), binary()
) -> list({wor_vec2(), wor_entity()}).

find_portal_places(ToZone, ToTarget) ->
	case wor_zone:find_zone_by_name(ToZone) of
		{ok, ZoneEnt} ->
			Portals = wor_entmgr:select_components(
				fun(_PortalEnt, [Portal, Placeable]) ->
					#wor_portal_comp{
						name = PortalName
					} = Portal,
					#wor_placeable_comp{
						pos  = PortalPos,
						zone = PortalZoneEnt
					} = Placeable,
					case {PortalName, PortalZoneEnt} of
						{ToTarget, ZoneEnt} ->
							{value, {PortalPos, PortalZoneEnt}};
						_ -> false
					end
				end, [portal, placeable]),
			dict:fold(fun(_PortalEnt, Value, AccIn) ->
				[Value | AccIn]
			end, [], Portals);
		_ -> []
	end.

%% ------------------------------------
%% try_teleport_entity
%% ------------------------------------

-spec try_teleport_entity(
	wor_entity(), list({wor_vec2(),wor_entity()})
) -> ok.

try_teleport_entity(_Entity, []) ->
	ok;

try_teleport_entity(Entity, [{Pos,ZoneEnt} | Tail]) ->
	case wor_zone:is_tile_passable(ZoneEnt, Pos) of
		true ->
			#wor_placeable_comp{
				zone = OldZone
			} = wor_entmgr:get_component(Entity, placeable),
			wor_logger:do_log_teleport_to(Entity, ZoneEnt),
			wor_entmgr:update_component(Entity, fun(Placeable) ->
				Placeable#wor_placeable_comp{
					pos  = Pos,
					zone = ZoneEnt
				}
			end, placeable),
			wor_logger:do_log_teleport_from(Entity, OldZone);
		false ->
			try_teleport_entity(Entity, Tail)
	end.
