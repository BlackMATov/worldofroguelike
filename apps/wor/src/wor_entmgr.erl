-module(wor_entmgr).
-behaviour(gen_server).

%% public
-export([start_link/0]).
-export([stop/0]).

%% public
-export([dump/0]).
-export([state/0]).
-export([transaction/1]).

%% public
-export([create_entity/0]).
-export([create_entity/1]).
-export([remove_entity/1]).

%% public
-export([create_index/3]).
-export([has_any_index/1]).

%% public
-export([create_component/3]).
-export([create_components/2]).
-export([remove_component/2]).
-export([remove_components/1]).
-export([remove_components/2]).

%% public
-export([update_component/2]).
-export([update_component/3]).
-export([supdate_component/3]).
-export([rupdate_component/3]).
-export([update_components/2]).
-export([select_components/2]).
-export([select_components/3]).

%% public
-export([has_component/2]).
-export([has_components/2]).
-export([get_component/1]).
-export([get_component/2]).
-export([find_component/2]).

%% public
-export([get_versions/1]).
-export([get_components/1]).
-export([get_components/2]).

%% public
-export([map_components/2]).
-export([fold_components/3]).
-export([fold_components/4]).
-export([filter_components/2]).
-export([filter_components/3]).

%% public
-export([has_entity/1]).
-export([has_entities/1]).
-export([get_entities/0]).
-export([get_entity_version/1]).
-export([get_entity_components/1]).
-export([get_entity_components/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").

-type index()        :: wor_es:index().
-type version()      :: wor_es:version().
-type index_name()   :: wor_es:index_name().
-type index_func()   :: wor_es:index_func().
-type select_func()  :: wor_es:select_func().
-type update_func()  :: wor_es:update_func().
-type rupdate_func() :: wor_es:rupdate_func().

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ------------------------------------
%% dump
%% ------------------------------------

-spec dump(
) -> ok.

dump() ->
	gen_server:call(?MODULE, dump).

%% ------------------------------------
%% state
%% ------------------------------------

-spec state(
) -> wor_es:state().

state() ->
	gen_server:call(?MODULE, state).

%% ------------------------------------
%% transaction
%% ------------------------------------

-spec transaction(
	fun((wor_es:state()) -> wor_es:state())
) -> ok.

transaction(Func) ->
	gen_server:cast(?MODULE, {transaction, Func}).

%% ------------------------------------
%% create_entity
%% ------------------------------------

-spec create_entity(
) -> wor_entity().

create_entity() ->
	gen_server:call(?MODULE, create_entity).

-spec create_entity(
	list({wor_comp_type(), wor_comp_state()})
) -> wor_entity().

create_entity(Comps) ->
	gen_server:call(?MODULE, {create_entity, Comps}).

%% ------------------------------------
%% remove_entity
%% ------------------------------------

-spec remove_entity(
	wor_entity()
) -> ok.

remove_entity(Entity) ->
	gen_server:cast(?MODULE, {remove_entity, Entity}).

%% ------------------------------------
%% create_index
%% ------------------------------------

-spec create_index(
	wor_comp_type(), index_name(), index_func()
) -> ok.

create_index(CompType, IndexName, Func) ->
	gen_server:cast(?MODULE, {create_index, CompType, IndexName, Func}).

%% ------------------------------------
%% has_any_index
%% ------------------------------------

-spec has_any_index(
	wor_comp_type()
) -> boolean().

has_any_index(CompType) ->
	gen_server:call(?MODULE, {has_any_index, CompType}).

%% ------------------------------------
%% create_component
%% ------------------------------------

-spec create_component(
	wor_entity(), wor_comp_type(), wor_comp_state()
) -> ok.

create_component(Entity, CompType, Comp) ->
	gen_server:cast(?MODULE, {create_component, Entity, CompType, Comp}).

%% ------------------------------------
%% create_components
%% ------------------------------------

-spec create_components(
	wor_entity(), list({wor_comp_type(), wor_comp_state()})
) -> ok.

create_components(Entity, Comps) ->
	gen_server:cast(?MODULE, {create_components, Entity, Comps}).

%% ------------------------------------
%% remove_component
%% ------------------------------------

-spec remove_component(
	wor_entity(), wor_comp_type()
) -> ok.

remove_component(Entity, CompType) ->
	gen_server:cast(?MODULE, {remove_component, Entity, CompType}).

%% ------------------------------------
%% remove_components
%% ------------------------------------

-spec remove_components(
	wor_entity()
) -> ok.

remove_components(Entity) ->
	gen_server:cast(?MODULE, {remove_components, Entity}).

-spec remove_components(
	wor_entity(), list(wor_comp_type())
) -> ok.

remove_components(Entity, Comps) ->
	gen_server:cast(?MODULE, {remove_components, Entity, Comps}).

%% ------------------------------------
%% update_component
%% ------------------------------------

-spec update_component(
	update_func() | wor_comp_state(), wor_comp_type()
) -> ok.

update_component(FuncOrComp, CompType) ->
	gen_server:cast(?MODULE, {update_component, FuncOrComp, CompType}).

-spec update_component(
	wor_entity(), update_func() | wor_comp_state(), wor_comp_type()
) -> ok.

update_component(Entity, FuncOrComp, CompType) ->
	gen_server:cast(?MODULE, {update_component, Entity, FuncOrComp, CompType}).

%% ------------------------------------
%% supdate_component
%% ------------------------------------

-spec supdate_component(
	wor_entity(), update_func() | wor_comp_state(), wor_comp_type()
) -> ok.

supdate_component(Entity, FuncOrComp, CompType) ->
	gen_server:cast(?MODULE, {supdate_component, Entity, FuncOrComp, CompType}).

%% ------------------------------------
%% rupdate_component
%% ------------------------------------

-spec rupdate_component(
	wor_entity(), rupdate_func(), wor_comp_type()
) -> term().

rupdate_component(Entity, Func, CompType) ->
	gen_server:call(?MODULE, {rupdate_component, Entity, Func, CompType}).

%% ------------------------------------
%% update_components
%% ------------------------------------

-spec update_components(
	wor_entity(), list({wor_comp_type(), update_func() | wor_comp_state()})
) -> ok.

update_components(Entity, FuncsOrComps) ->
	gen_server:cast(?MODULE, {update_components, Entity, FuncsOrComps}).

%% ------------------------------------
%% select_components
%% ------------------------------------

-spec select_components(
	select_func(), list(wor_comp_type())
) -> dict:dict(wor_entity(), term()).

select_components(Func, Comps) ->
	gen_server:call(?MODULE, {select_components, Func, Comps}).

-spec select_components(
	select_func(), list(wor_comp_type()), index()
) -> dict:dict(wor_entity(), term()).

select_components(Func, Comps, Index) ->
	gen_server:call(?MODULE, {select_components, Func, Comps, Index}).

%% ------------------------------------
%% has_component
%% ------------------------------------

-spec has_component(
	wor_entity(), wor_comp_type()
) -> boolean().

has_component(Entity, CompType) ->
	gen_server:call(?MODULE, {has_component, Entity, CompType}).

%% ------------------------------------
%% has_components
%% ------------------------------------

-spec has_components(
	wor_entity(), list(wor_comp_type())
) -> boolean().

has_components(Entity, CompTypes) ->
	gen_server:call(?MODULE, {has_components, Entity, CompTypes}).

%% ------------------------------------
%% get_component
%% ------------------------------------

-spec get_component(
	wor_comp_type()
) -> {wor_entity(),wor_comp_state()}.

get_component(CompType) ->
	gen_server:call(?MODULE, {get_component, CompType}).

-spec get_component(
	wor_entity(), wor_comp_type()
) -> wor_comp_state().

get_component(Entity, CompType) ->
	gen_server:call(?MODULE, {get_component, Entity, CompType}).

%% ------------------------------------
%% find_component
%% ------------------------------------

-spec find_component(
	wor_entity(), wor_comp_type()
) -> {ok, wor_comp_state()} | {error, not_found}.

find_component(Entity, CompType) ->
	gen_server:call(?MODULE, {find_component, Entity, CompType}).

%% ------------------------------------
%% get_versions
%% ------------------------------------

-spec get_versions(
	wor_comp_type()
) -> dict:dict(wor_entity(), version()).

get_versions(CompType) ->
	gen_server:call(?MODULE, {get_versions, CompType}).

%% ------------------------------------
%% get_components
%% ------------------------------------

-spec get_components(
	wor_comp_type() | list(wor_comp_type())
) -> dict:dict(wor_entity(), wor_comp_state()).

get_components(CompTypeOrComps) ->
	gen_server:call(?MODULE, {get_components, CompTypeOrComps}).

-spec get_components(
	wor_comp_type() | list(wor_comp_type()), index()
) -> dict:dict(wor_entity(), wor_comp_state()).

get_components(CompTypeOrComps, Index) ->
	gen_server:call(?MODULE, {get_components, CompTypeOrComps, Index}).

%% ------------------------------------
%% map_components
%% ------------------------------------

-spec map_components(
	fun((wor_entity(),wor_comp_state()) -> term()),
	wor_comp_type() | list(wor_comp_type())
) -> dict:dict(wor_entity(), term()).

map_components(Func, CompTypeOrComps) ->
	dict:map(Func, get_components(CompTypeOrComps)).

%% ------------------------------------
%% fold_components
%% ------------------------------------

-spec fold_components(
	fun((wor_entity(),wor_comp_state(),term()) -> term()),
	term(), wor_comp_type() | list(wor_comp_type())
) -> term().

fold_components(Func, AccIn, CompTypeOrComps) ->
	dict:fold(Func, AccIn, get_components(CompTypeOrComps)).

-spec fold_components(
	fun((wor_entity(),wor_comp_state(),term()) -> term()),
	term(), wor_comp_type() | list(wor_comp_type()),
	index()
) -> term().

fold_components(Func, AccIn, CompTypeOrComps, Index) ->
	dict:fold(Func, AccIn, get_components(CompTypeOrComps, Index)).

%% ------------------------------------
%% filter_components
%% ------------------------------------

-spec filter_components(
	fun((wor_entity(),wor_comp_state()) -> boolean()),
	wor_comp_type() | list(wor_comp_type())
) -> dict:dict(wor_entity(), wor_comp_state()).

filter_components(Func, CompTypeOrComps) ->
	dict:filter(Func, get_components(CompTypeOrComps)).

-spec filter_components(
	fun((wor_entity(),wor_comp_state()) -> boolean()),
	wor_comp_type() | list(wor_comp_type()),
	index()
) -> dict:dict(wor_entity(), wor_comp_state()).

filter_components(Func, CompTypeOrComps, Index) ->
	dict:filter(Func, get_components(CompTypeOrComps, Index)).

%% ------------------------------------
%% has_entity
%% ------------------------------------

-spec has_entity(
	wor_entity()
) -> boolean().

has_entity(Entity) ->
	gen_server:call(?MODULE, {has_entity, Entity}).

%% ------------------------------------
%% has_entities
%% ------------------------------------

-spec has_entities(
	list(wor_entity())
) -> boolean().

has_entities(Entities) ->
	gen_server:call(?MODULE, {has_entities, Entities}).

%% ------------------------------------
%% get_entities
%% ------------------------------------

-spec get_entities(
) -> dict:dict(wor_entity(), version()).

get_entities() ->
	gen_server:call(?MODULE, get_entities).

%% ------------------------------------
%% get_entity_version
%% ------------------------------------

-spec get_entity_version(
	wor_entity()
) -> version().

get_entity_version(Entity) ->
	gen_server:call(?MODULE, {get_entity_version, Entity}).

%% ------------------------------------
%% get_entity_components
%% ------------------------------------

-spec get_entity_components(
	wor_entity()
) -> dict:dict(wor_comp_type(), wor_comp_state()).

get_entity_components(Entity) ->
	gen_server:call(?MODULE, {get_entity_components, Entity}).

-spec get_entity_components(
	wor_entity(), list(wor_comp_type())
) -> list(wor_comp_state()).

get_entity_components(Entity, Comps) ->
	gen_server:call(?MODULE, {get_entity_components, Entity, Comps}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, wor_es:state()}.

init([]) ->
	io:format("~p:init(Self:~p)~n", [?MODULE, self()]),
	{ok, wor_es:new()}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	io:format("~p:stop(Self:~p)~n", [?MODULE, self()]),
	{stop, normal, ok, State};

handle_call(dump, _From, State) ->
	Result = wor_es:dump(State),
	{reply, Result, State};

handle_call(state, _From, State) ->
	{reply, State, State};

handle_call(create_entity, _From, State) ->
	{Result, NewState} = wor_es:create_entity(State),
	{reply, Result, NewState};

handle_call({create_entity, Comps}, _From, State) ->
	{Result, NewState} = wor_es:create_entity(Comps, State),
	{reply, Result, NewState};

handle_call({has_any_index, CompType}, _From, State) ->
	Result = wor_es:has_any_index(CompType, State),
	{reply, Result, State};

handle_call({rupdate_component, Entity, Func, CompType}, _From, State) ->
	{Ret, NewState} = wor_es:rupdate_component(Entity, Func, CompType, State),
	{reply, Ret, NewState};

handle_call({select_components, Func, Comps}, _From, State) ->
	Result = wor_es:select_components(Func, Comps, State),
	{reply, Result, State};

handle_call({select_components, Func, Comps, Index}, _From, State) ->
	Result = wor_es:select_components(Func, Comps, Index, State),
	{reply, Result, State};

handle_call({has_component, Entity, CompType}, _From, State) ->
	Result = wor_es:has_component(Entity, CompType, State),
	{reply, Result, State};

handle_call({has_components, Entity, CompTypes}, _From, State) ->
	Result = wor_es:has_components(Entity, CompTypes, State),
	{reply, Result, State};

handle_call({get_component, CompType}, _From, State) ->
	Result = wor_es:get_component(CompType, State),
	{reply, Result, State};

handle_call({get_component, Entity, CompType}, _From, State) ->
	Result = wor_es:get_component(Entity, CompType, State),
	{reply, Result, State};

handle_call({find_component, Entity, CompType}, _From, State) ->
	Result = wor_es:find_component(Entity, CompType, State),
	{reply, Result, State};

handle_call({get_versions, CompType}, _From, State) ->
	Result = wor_es:get_versions(CompType, State),
	{reply, Result, State};

handle_call({get_components, CompTypeOrComps}, _From, State) ->
	Result = wor_es:get_components(CompTypeOrComps, State),
	{reply, Result, State};

handle_call({get_components, CompTypeOrComps, Index}, _From, State) ->
	Result = wor_es:get_components(CompTypeOrComps, Index, State),
	{reply, Result, State};

handle_call({has_entity, Entity}, _From, State) ->
	Result = wor_es:has_entity(Entity, State),
	{reply, Result, State};

handle_call({has_entities, Entities}, _From, State) ->
	Result = wor_es:has_entities(Entities, State),
	{reply, Result, State};

handle_call(get_entities, _From, State) ->
	Result = wor_es:get_entities(State),
	{reply, Result, State};

handle_call({get_entity_version, Entity}, _From, State) ->
	Result = wor_es:get_entity_version(Entity, State),
	{reply, Result, State};

handle_call({get_entity_components, Entity}, _From, State) ->
	Result = wor_es:get_entity_components(Entity, State),
	{reply, Result, State};

handle_call({get_entity_components, Entity, Comps}, _From, State) ->
	Result = wor_es:get_entity_components(Entity, Comps, State),
	{reply, Result, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({transaction, Func}, State) ->
	NewState = Func(State),
	{noreply, NewState};

handle_cast({remove_entity, Entity}, State) ->
	NewState = wor_es:remove_entity(Entity, State),
	{noreply, NewState};

handle_cast({create_index, CompType, IndexName, Func}, State) ->
	NewState = wor_es:create_index(CompType, IndexName, Func, State),
	{noreply, NewState};

handle_cast({create_component, Entity, CompType, Comp}, State) ->
	NewState = wor_es:create_component(Entity, CompType, Comp, State),
	{noreply, NewState};

handle_cast({create_components, Entity, Comps}, State) ->
	NewState = wor_es:create_components(Entity, Comps, State),
	{noreply, NewState};

handle_cast({remove_component, Entity, CompType}, State) ->
	NewState = wor_es:remove_component(Entity, CompType, State),
	{noreply, NewState};

handle_cast({remove_components, Entity}, State) ->
	NewState = wor_es:remove_components(Entity, State),
	{noreply, NewState};

handle_cast({remove_components, Entity, Comps}, State) ->
	NewState = wor_es:remove_components(Entity, Comps, State),
	{noreply, NewState};

handle_cast({update_component, FuncOrComp, CompType}, State) ->
	NewState = wor_es:update_component(FuncOrComp, CompType, State),
	{noreply, NewState};

handle_cast({update_component, Entity, FuncOrComp, CompType}, State) ->
	NewState = wor_es:update_component(Entity, FuncOrComp, CompType, State),
	{noreply, NewState};

handle_cast({supdate_component, Entity, FuncOrComp, CompType}, State) ->
	NewState = wor_es:supdate_component(Entity, FuncOrComp, CompType, State),
	{noreply, NewState};

handle_cast({update_components, Entity, FuncsOrComps}, State) ->
	NewState = wor_es:update_components(Entity, FuncsOrComps, State),
	{noreply, NewState};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
