-module(wor_broadmgr).
-behaviour(gen_server).

%% public
-export([start_link/0]).
-export([stop/0]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").
-define(TICK_INTERVAL, 50).

-record(cache, {
	cache_users    = sets:new() :: sets:set(pid()),
	cache_entities = dict:new() :: dict:dict(wor_entity(), integer())
}).
-type cache() :: #cache{}.

-record(state, {
	caches = dict:new() :: dict:dict(wor_entity(), cache())
}).
-type state() :: #state{}.

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, state()}.

init([]) ->
	io:format("~p:init(Self:~p)~n", [?MODULE, self()]),
	{ok, broadcast(#state{})}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	io:format("~p:stop(Self:~p)~n", [?MODULE, self()]),
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(tick, State) ->
	NewState = broadcast(State),
	{noreply, NewState};

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

-spec broadcast(
	state()
) -> state().

broadcast(State) ->
	ES = wor_entmgr:state(),
	NewState = dict:fold(fun(ZoneEnt, _Zone, AccIn) ->
		zone_broadcast(ZoneEnt, ES, AccIn)
	end, State, wor_es:get_components(zone, ES)),
	erlang:send_after(?TICK_INTERVAL, self(), tick),
	NewState.

%% ------------------------------------
%% zone_broadcast
%% ------------------------------------

-spec zone_broadcast(
	wor_entity(), wor_es:state(), state()
) -> state().

zone_broadcast(
	ZoneEnt, ES,
	State = #state{caches = Caches}
) ->
	Users    = zone_users(ZoneEnt, ES),
	Cache    = zone_cache(ZoneEnt, State),
	NewCache = broadcast_zone(ZoneEnt, ES, Users, Cache),
	State#state{caches = dict:store(ZoneEnt, NewCache, Caches)}.

%% ------------------------------------
%% zone_cache
%% ------------------------------------

-spec zone_cache(
	wor_entity(), state()
) -> cache().

zone_cache(
	ZoneEnt,
	#state{caches = Caches}
) ->
	case dict:find(ZoneEnt, Caches) of
		{ok, Cache} -> Cache;
		_ -> #cache{}
	end.

%% ------------------------------------
%% zone_users
%% ------------------------------------

-spec zone_users(
	wor_entity(), wor_es:state()
) -> list(pid()).

zone_users(ZoneEnt, ES) ->
	dict:fold(fun(_UserEnt, [
		#wor_userable_comp{user = User},
		#wor_placeable_comp{zone = OtherZoneEnt}], AccIn) ->
		case OtherZoneEnt == ZoneEnt of
			true  -> [User | AccIn];
			false -> AccIn
		end
	end, [], wor_es:get_components([userable, placeable], ES)).

%% ------------------------------------
%% broadcast_zone
%% ------------------------------------

-spec broadcast_zone(
	wor_entity(), wor_es:state(), list(pid()), cache()
) -> cache().

broadcast_zone(_ZoneEnt, _ES, [], _Cache) ->
	#cache{};

broadcast_zone(
	ZoneEnt, ES, Users,
	Cache = #cache{cache_users = CacheUsers}
) ->
	NewCacheUsers = sets:from_list(Users),
	UserIntersect = sets:intersection(CacheUsers, NewCacheUsers),
	Resync        = sets:size(UserIntersect) < sets:size(NewCacheUsers),
	{EntitiesInfo, KillsInfo, InfoCache} = case Resync of
		true  -> broadcast_info_sync(ZoneEnt, ES, Cache);
		false -> broadcast_info_diff(ZoneEnt, ES, Cache)
	end,
	ok = broadcast_info(ZoneEnt, ES, Users, Resync, EntitiesInfo, KillsInfo),
	InfoCache#cache{cache_users = NewCacheUsers}.

%% ------------------------------------
%% broadcast_info
%% ------------------------------------

-spec broadcast_info(
	wor_entity(), wor_es:state(), list(pid()), boolean(), wor_json(), wor_json()
) -> ok.

broadcast_info(ZoneEnt, ES, Users, Resync, EntitiesInfo, KillsInfo) ->
	#wor_zone_comp{
		size = Size
	} = wor_es:get_component(ZoneEnt, zone, ES),
	Json = [<<"zone_info">>, [
		length(Users),
		wor_utils:vec2_to_json(Size),
		wor_utils:bool_to_json(Resync),
		EntitiesInfo,
		KillsInfo
	]],
	Text = jiffy:encode(Json),
	lists:foreach(fun(User) ->
		wor_user:send_text(User, Text)
	end, Users).

%% ------------------------------------
%% broadcast_info_sync
%% ------------------------------------

-spec broadcast_info_sync(
	wor_entity(), wor_es:state(), cache()
) -> {wor_json(), wor_json(), cache()}.

broadcast_info_sync(ZoneEnt, ES, Cache) ->
	AllPlaceables = wor_es:get_components(placeable, ES),
	PlaceableVersions = wor_es:get_versions(placeable, ES),
	{NewCacheEntities, EntitiesInfo} =
		dict:fold(fun(
			Ent,
			Placeable = #wor_placeable_comp{zone = OtherZoneEnt},
			AccIn = {CacheAccIn, InfoAccIn}) ->
			case OtherZoneEnt == ZoneEnt of
				true ->
					Ver = dict:fetch(Ent, PlaceableVersions),
					{
						dict:store(Ent, Ver, CacheAccIn),
						placeable_to_info(Ent, Placeable, InfoAccIn)
					};
				false ->
					AccIn
			end
		end, {dict:new(),[]}, AllPlaceables),
	{EntitiesInfo, [], Cache#cache{
		cache_entities = NewCacheEntities
	}}.

%% ------------------------------------
%% broadcast_info_diff
%% ------------------------------------

-spec broadcast_info_diff(
	wor_entity(), wor_es:state(), cache()
) -> {wor_json(), wor_json(), cache()}.

broadcast_info_diff(
	ZoneEnt, ES,
	Cache = #cache{cache_entities = CacheEntities}
) ->
	AllPlaceables = wor_es:get_components(placeable, ES),
	PlaceableVersions = wor_es:get_versions(placeable, ES),
	{CacheEntities2, KillsInfo} =
		dict:fold(fun(
			Ent,
			_Ver,
			AccIn = {CacheAccIn, InfoAccIn}) ->
			case dict:find(Ent, AllPlaceables) of
				{ok, #wor_placeable_comp{zone = ZoneEnt}} ->
					AccIn;
				_ ->
					{
						dict:erase(Ent, CacheAccIn),
						[Ent | InfoAccIn]
					}
			end
		end, {CacheEntities,[]}, CacheEntities),
	{CacheEntities3, EntitiesInfo} =
		dict:fold(fun(
			Ent,
			Placeable = #wor_placeable_comp{zone = OtherZoneEnt},
			AccIn = {CacheAccIn, InfoAccIn}) ->
			case OtherZoneEnt == ZoneEnt of
				true ->
					Ver = dict:fetch(Ent, PlaceableVersions),
					case dict:find(Ent, CacheAccIn) of
						{ok, OldEntVer} when OldEntVer == Ver ->
							AccIn;
						_ ->
							{
								dict:store(Ent, Ver, CacheAccIn),
								placeable_to_info(Ent, Placeable, InfoAccIn)
							}
					end;
				false ->
					AccIn
			end
		end, {CacheEntities2,[]}, AllPlaceables),
	{EntitiesInfo, KillsInfo, Cache#cache{
		cache_entities = CacheEntities3
	}}.

%% ------------------------------------
%% placeable_to_info
%% ------------------------------------

-spec placeable_to_info(
	wor_entity(), wor_placeable_comp(), wor_json()
) -> wor_json().

placeable_to_info(Ent, Placeable, InfoAccIn) ->
	#wor_placeable_comp{
		pos     = Pos,
		view    = View,
		visible = Visible
	} = Placeable,
	Info = [
		Ent,
		wor_utils:vec2_to_json(Pos),
		wor_utils:view_to_json(View),
		wor_utils:bool_to_json(Visible)
	],
	[Info | InfoAccIn].
