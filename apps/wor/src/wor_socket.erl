-module(wor_socket).
-behaviour(cowboy_websocket_handler).

%% public
-export([send_json/2]).
-export([send_text/2]).
-export([send_binary/2]).

%% cowboy_websocket_handler
-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% send_json
%% ------------------------------------

-spec send_json(
	pid(), wor_json()
) -> ok.

send_json(Pid, JsonMsg) ->
	Pid ! {send_json, JsonMsg},
	ok.

%% ------------------------------------
%% send_text
%% ------------------------------------

-spec send_text(
	pid(), binary()
) -> ok.

send_text(Pid, Msg) ->
	Pid ! {send_text, Msg},
	ok.

%% ------------------------------------
%% send_binary
%% ------------------------------------

-spec send_binary(
	pid(), binary()
) -> ok.

send_binary(Pid, Binary) ->
	Pid ! {send_binary, Binary},
	ok.

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR cowboy_websocket_handler
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init({tcp, http}, _Req, _Opts) ->
	{upgrade, protocol, cowboy_websocket}.

%% ------------------------------------
%% websocket_init
%% ------------------------------------

websocket_init(_TransportName, Req, _Opts) ->
	{ok, State} = wor_user:start_link(self()),
	{ok, Req, State}.

%% ------------------------------------
%% websocket_handle
%% ------------------------------------

websocket_handle({text, Msg}, Req, State) ->
	wor_user:json_msg_handle(State, jiffy:decode(Msg)),
	{ok, Req, State};

websocket_handle({binary, Binary}, Req, State) ->
	wor_user:binary_msg_handle(State, Binary),
	{ok, Req, State};

websocket_handle(_Data, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_info
%% ------------------------------------

websocket_info({send_json, Json}, Req, State) ->
	Info = {text, jiffy:encode(Json)},
	{reply, Info, Req, State};

websocket_info({send_text, Msg}, Req, State) ->
	Info = {text, Msg},
	{reply, Info, Req, State};

websocket_info({send_binary, Msg}, Req, State) ->
	Info = {binary, Msg},
	{reply, Info, Req, State};

websocket_info(_Info, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_terminate
%% ------------------------------------

websocket_terminate(_Reason, _Req, State) ->
	ok = wor_user:stop(State).
