-module(wor_requester).

%% public
-export([tick/1]).
-export([send_self_info/1]).
-export([send_grab_info/1]).
-export([send_stats_info/1]).
-export([send_inventory_info/1]).
-export([request_grab_info/1]).
-export([request_inventory_info/1]).

-include("types.hrl").

-type info() ::
	grab_info      |
	inventory_info.

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	wor_entity()
) -> ok.

tick(World) ->
	resolve_process(World).

%% ------------------------------------
%% send_self_info
%% ------------------------------------

-spec send_self_info(
	wor_entity()
) -> ok.

send_self_info(Entity) ->
	case can_send_self_info(Entity) of
		true ->
			#wor_userable_comp{user = User} =
				wor_entmgr:get_component(Entity, userable),
			IsDead = wor_entmgr:has_component(Entity, spirit),
			wor_user:send_json(User, <<"self_info">>, [
				Entity,
				wor_utils:bool_to_json(IsDead)
			]);
		false ->
			ok
	end.

%% ------------------------------------
%% send_grab_info
%% ------------------------------------

-spec send_grab_info(
	wor_entity()
) -> ok.

send_grab_info(Entity) ->
	case can_send_grab_info(Entity) of
		true ->
			[
				#wor_userable_comp{user = User},
				#wor_placeable_comp{pos = Pos, zone = ZoneEnt}
			] = wor_entmgr:get_entity_components(Entity, [userable, placeable]),
			Entities = wor_entmgr:select_components(
				fun(_InvItemEnt, [Placeable, _InvItem, Nameable]) ->
					#wor_placeable_comp{zone = OtherZoneEnt} = Placeable,
					case ZoneEnt == OtherZoneEnt of
						true ->
							#wor_nameable_comp{name = OtherName} = Nameable,
							{value, OtherName};
						false -> false
					end
				end, [placeable, inv_item, nameable],
				{tile_index, wor_utils:tile_index(Pos)}),
			JsonItems = dict:fold(fun(InvItemEnt, Name, AccIn) ->
				[[InvItemEnt, Name] | AccIn]
			end, [], Entities),
			Json = [<<"grab_info">>, [
				JsonItems
			]],
			wor_user:send_json(User, Json);
		false ->
			ok
	end.

%% ------------------------------------
%% send_stats_info
%% ------------------------------------

-spec send_stats_info(
	wor_entity()
) -> ok.

send_stats_info(Entity) ->
	case can_send_stats_info(Entity) of
		true ->
			[
				#wor_userable_comp{user = User},
				#wor_fighter_comp{min_dmg = MinDmg, max_dmg = MaxDmg, def = Def},
				#wor_health_comp{hp = Hp, max = MaxHp},
				#wor_stats_comp{str = Str, dex = Dex, int = Int},
				#wor_experience_comp{level = Level, experience = Experience}
			] = wor_entmgr:get_entity_components(
					Entity, [userable, fighter, health, stats, experience]),
			Json = [<<"stats_info">>, [
				MinDmg, MaxDmg, Def,
				Hp, MaxHp,
				Str, Dex, Int,
				Level, Experience,
				wor_experience:level_to_experience(Level + 1)
			]],
			wor_user:send_json(User, Json);
		false ->
			ok
	end.

%% ------------------------------------
%% send_inventory_info
%% ------------------------------------

-spec send_inventory_info(
	wor_entity()
) -> ok.

send_inventory_info(Entity) ->
	case can_send_inventory_info(Entity) of
		true ->
			[
				#wor_userable_comp{user = User},
				#wor_inventory_comp{items = Items}
			] = wor_entmgr:get_entity_components(Entity, [userable, inventory]),
			JsonItems = sets:fold(fun(InvItemEnt, AccIn) ->
				Info = inv_item_to_info(Entity, InvItemEnt),
				[Info | AccIn]
			end, [], Items),
			Json = [<<"inventory_info">>, [
				JsonItems
			]],
			wor_user:send_json(User, Json);
		false ->
			ok
	end.

%% ------------------------------------
%% request_grab_info
%% ------------------------------------

request_grab_info(User) ->
	request_info(User, grab_info).

%% ------------------------------------
%% request_inventory_info
%% ------------------------------------

request_inventory_info(User) ->
	request_info(User, inventory_info).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% can_send_self_info
%% ------------------------------------

-spec can_send_self_info(
	wor_entity()
) -> boolean().

can_send_self_info(Entity) ->
	wor_entmgr:has_component(Entity, userable).

%% ------------------------------------
%% can_send_grab_info
%% ------------------------------------

-spec can_send_grab_info(
	wor_entity()
) -> boolean().

can_send_grab_info(Entity) ->
	wor_entmgr:has_components(Entity, [
		userable, placeable]).

%% ------------------------------------
%% can_send_stats_info
%% ------------------------------------

-spec can_send_stats_info(
	wor_entity()
) -> boolean().

can_send_stats_info(Entity) ->
	wor_entmgr:has_components(Entity, [
		userable, fighter, health, stats, experience]).

%% ------------------------------------
%% can_send_inventory_info
%% ------------------------------------

-spec can_send_inventory_info(
	wor_entity()
) -> boolean().

can_send_inventory_info(Entity) ->
	wor_entmgr:has_components(Entity, [
		userable, inventory]).

%% ------------------------------------
%% inv_item_to_info
%% ------------------------------------

-spec inv_item_to_info(
	wor_entity(), wor_entity()
) -> wor_json().

inv_item_to_info(Owner, InvItemEnt) ->
	#wor_nameable_comp{name = Name} =
		wor_entmgr:get_component(InvItemEnt, nameable),
	Equipped = case wor_entmgr:has_component(InvItemEnt, equipment) of
		true  -> wor_equip_slots:is_equipped(Owner, InvItemEnt);
		false -> false
	end,
	[
		InvItemEnt,
		Name,
		wor_utils:bool_to_json(Equipped)
	].

%% ------------------------------------
%% request_info
%% ------------------------------------

-spec request_info(
	pid(), info()
) -> ok.

request_info(User, Info) ->
	wor_entmgr:update_component(fun(Requester) ->
		Requests = Requester#wor_requester_comp.requests,
		NewRequests = queue:in({User, Info}, Requests),
		Requester#wor_requester_comp{requests = NewRequests}
	end, requester).

%% ------------------------------------
%% resolve_process
%% ------------------------------------

-spec resolve_process(
	wor_entity()
) -> ok.

resolve_process(RequesterEnt) ->
	{User, Info} = wor_entmgr:rupdate_component(RequesterEnt, fun(Requester) ->
		#wor_requester_comp{requests = Requests} = Requester,
		case queue:out(Requests) of
			{{value, {ReqUser, ReqInfo}}, NewRequests} ->
				{
					Requester#wor_requester_comp{requests = NewRequests},
					{ReqUser, ReqInfo}
				};
			_ ->
				{
					Requester,
					{undefined, undefined}
				}
		end
	end, requester),
	case {User, Info} of
		{undefined, _} -> ok;
		{_, undefined} -> ok;
		{User, Info} ->
			ok = send_info(User, Info),
			ok = resolve_process(RequesterEnt)
	end.

%% ------------------------------------
%% send_info
%% ------------------------------------

-spec send_info(
	pid(), info()
) -> ok.

send_info(User, Info) ->
	dict:fold(fun(UserableEnt, _Userable, AccIn) ->
		AccIn = case Info of
			grab_info      -> send_grab_info(UserableEnt);
			inventory_info -> send_inventory_info(UserableEnt)
		end
	end, ok, wor_entmgr:filter_components(fun(_UserEnt, Userable) ->
		#wor_userable_comp{user = OtherUser} = Userable,
		User == OtherUser
	end, userable)).
